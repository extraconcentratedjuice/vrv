/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "csr.h"
#include "emu.h"
#include "memloader.h"
#include "reg.h"
#include "symbol.h"
#include "utils.h"

regword_t R[R_LENGTH];
memaddr_t PC;
fregword_t FR[FR_LENGTH];

char *int_reg_names[] = {
    "zero", "ra", "sp", "gp", "tp", "t0", "t1", "t2", "fp", "s1", "a0", "a1",
    "a2", "a3", "a4", "a5", "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7",
    "s8", "s9", "s10", "s11", "t3", "t4", "t5", "t6"
};

void initialize_registers(void)
{
    // Clear all registers
    memclr(R, sizeof(R));
    memclr(FR, sizeof(FR));
    memclr(C, CSR_ARRAY_SIZE * sizeof(uregword_t));

    // Set `sp`, `gp`,  `pc`
    R[REG_SP] = STACK_TOP;
    R[REG_GP] = SMALL_DATA_MID;
    // - Function returns 0 if symbol is undefined
    PC = find_symbol_address(DEFAULT_RUN_LOCATION);
}
