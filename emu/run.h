/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef RUN_H
#define RUN_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "csr.h"
#include "reg.h"
#include "memseg.h"


/*
 * Exception codes
 */

#define INTERRUPT_BIT   0x80000000
typedef enum excode {

    EXC_INST_MISALIGN   = 0,
    EXC_INST_ACCESS,
    EXC_INST_ILLEGAL,
    EXC_BREAKPOINT,
    EXC_LOAD_MISALIGN,
    EXC_LOAD_ACCESS,
    EXC_STORE_MISALIGN,
    EXC_STORE_ACCESS,
    EXC_U_ECALL,

    EXC_M_ECALL         = 11,

    INT_M_SOFTWARE      = ( 3 | INTERRUPT_BIT),
    INT_M_TIMER         = ( 7 | INTERRUPT_BIT),
    INT_M_EXTERNAL      = (11 | INTERRUPT_BIT),
    // IntCode >= 16 reserved for platform

} excode_e;


/*
 * Interrupt bits
 */

typedef enum intbits {

    INT_M_SOFTWARE_BIT  = 1 << (INT_M_SOFTWARE  & ~INTERRUPT_BIT),
    INT_M_TIMER_BIT     = 1 << (INT_M_TIMER     & ~INTERRUPT_BIT),
    INT_M_EXTERNAL_BIT  = 1 << (INT_M_EXTERNAL  & ~INTERRUPT_BIT),

} intbits_e;


/*
 * Trap vector mode
 */

typedef enum trapvecmode {

    TRAP_DIRECT = 0,
    TRAP_VECTORED,
    TRAP_RESERVED2,
    TRAP_RESERVED3,

} trapvecmode_e;


/*
 * Interrupt inlines
 */

static inline void raise_interrupt(intbits_e ibit)
{
    set_csr_field(CSR_MIP, 0, ibit, ibit);
}

static inline void lower_interrupt(intbits_e ibit)
{
    set_csr_field(CSR_MIP, 0, ibit, 0);
}


/*
 * Run results
 */

typedef enum runres {
    RUN_STOPPED = 0,
    RUN_CONTINUABLE,
    RUN_BREAKPOINT,
    RUN_WAIT_FOR_INPUT,
} runres_e;


/*
 * Exported variables
 */

extern bool force_break;    // Used by GUI frontends to pause execution


/*
 * Exported functions
 */

/* Exception raising and handling */
void raise_exception(excode_e ex, uregword_t mtval);
void print_exception(excode_e ex, memaddr_t addr);

/* Assembly execution */
runres_e run_asm(int steps);

#ifdef __cplusplus
}
#endif

#endif  // RUN_H
