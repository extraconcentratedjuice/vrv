/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef SSTREAM_H
#define SSTREAM_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sstream {
    char* buf;       /* Buffer containing output */
    int max_length;  /* Length of buffer */
    int empty_pos;   /* Index  of empty char in stream*/
    int initialized; /* Stream initialized? */
} sstream_s;


void ss_init(sstream_s *ss);
void ss_destroy(sstream_s *ss);
void ss_clear(sstream_s *ss);
void ss_erase(sstream_s *ss, int n);
int ss_length(sstream_s *ss);
char *ss_to_string(sstream_s *ss);
void ss_printf(sstream_s *ss, char* fmt, ...);
void ss_print_byte(sstream_s *ss, char byte);

#ifdef __cplusplus
}
#endif

#endif  // SSTREAM_H
