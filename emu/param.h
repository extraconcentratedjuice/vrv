/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef GLBL_H
#define GLBL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>

#include "memaccess.h"

/*
 * Configuration
 */

struct configuration {
    uint32_t timer_tick_ms;         // Timer ticks per ms
    uint32_t stack_max_size;        // Max size of stack segment
    uint16_t small_data_max_size;   // Max size for `.comm` to store in small data seg

    /* Interval (in instructions) at which memory-mapped IO registers are checked
     * and updated. (This is to reduce overhead from making system calls to check
     * for IO. It can be set as low as 1.) */
    int io_interval;
    bool mapped_tty;                 // True => tty is mapped to memory
};

extern struct configuration conf;


/*
 * VRV control options
 */

typedef enum memop {
    // Increase by Pow2 to create bitmask
    MEMOP_LOAD      = 0x1,
    MEMOP_STORE     = 0x2,
} memop_e;

struct control {
    int verbose_lvl;                // Level of verbosity of logs
    int misaligned_exceptions;      // Which memory access ops should cause misaligned excs
    bool break_on_exceptions;       // True => excs will immediately return to prompt
};

extern struct control ctl;


/*
 * Emulator context and state
 */

typedef enum emustate {
    EMU_STATE=0,
    PARSE_STATE,
    EXEC_STATE
} emustate_e;

struct context {
    emustate_e state;               // Current state of emulator
    memaddr_t program_addr;         // Address of first user instruction (after bootloader)
    access_level_e current_access_level;
    int vrv_exit_code;              // When not using prompt, will be the exit code returned by main
    bool exception_occurred;        // True => exception has been raised
    bool parse_error_occurred;      // True => error during parsing
};

extern struct context ctx;

#ifdef __cplusplus
}
#endif

#endif  // GLBL_H
