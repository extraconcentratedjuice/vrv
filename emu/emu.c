/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <stdio.h>

#include "csr.h"
#include "emu.h"
#include "frontend.h"
#include "inst.h"
#include "map.h"
#include "memaccess.h"
#include "memloader.h"
#include "param.h"
#include "parser.h"
#include "parser.tab.h"
#include "reg.h"
#include "run.h"
#include "scanner.h"
#include "symbol.h"
#include "tty.h"
#include "utils.h"

#include "system_default.h"

#ifdef _MSC_VER
#define environ _environ
#endif


fe_api_s *fe = NULL;

/*
 * Structs
 */

/* Record of where a breakpoint was placed and the instruction previously in
 * memory. */
typedef struct bkpt {
    memaddr_t addr;
    inst_s *inst;
    struct bkpt *next;
    unsigned int id;
} bkpt_s;


/*
 * Local variables
 */

static bkpt_s *breakpoints;

static unsigned int next_bkpt_id = 1;


/*
 * Local functions
 */

static memaddr_t copy_int_to_stack(int val);
static memaddr_t copy_str_to_stack(char *str);

static inline void end_of_assembly_file(void);

static bkpt_s *delete_bkpt_node(bkpt_s *curr, bkpt_s *prev);


/* ========================================================================== *
 * Initialization + Destruction
 * ========================================================================== */

bool initialize_world(char *exception_filename)
{
    // Initialize the various emulator systems
    initialize_maps();
    initialize_memory();
    initialize_inst_memory();

    // Conditionally initialize devices
    /* @TEMP: Hard-coded for `tty`. In the future, it will likely be up to
     *        the frontend to determine which devices to initialize. */
    if (conf.mapped_tty)
        initialize_tty();

    // System starts in machine-mode
    ctx.current_access_level = M_MODE_ACCESS;

    // Read exception file
    if (!read_exception_file(exception_filename))
        return false;

    // Initialize registers after start location is defined
    initialize_registers();

    return true;
}

void destroy_world(void)
{
    // Destroy/clear the various emulator systems
    destroy_memory();
    destroy_inst_memory();
    destroy_symbol_table();

    // Delete debugger breakpoints
    delete_all_breakpoints();
}

void initialize_run_stack(int argc, char **argv)
{
    extern char **environ;
    // Count number of environment variables
    int envc = 0;
    for (; environ[envc] != NULL; ++envc) ;

    memaddr_t *env_addrs = (memaddr_t *)xmalloc(envc * sizeof(memaddr_t));
    memaddr_t *arg_addrs = (memaddr_t *)xmalloc(argc * sizeof(memaddr_t));
    unsigned env_addr_i = 0;
    unsigned arg_addr_i = 0;

    R[REG_SP] = STACK_TOP;      // Initialize `sp`

    // Put strings on stack
    // - env
    for (int i = 0; i < envc; ++i)
        env_addrs[env_addr_i++] = copy_str_to_stack(environ[i]);

    // - argv
    for (int i = 0; i < argc; ++i)
        arg_addrs[arg_addr_i++] = copy_str_to_stack(argv[i]);

    // Align stack pointer for word-size data
    R[REG_SP] = R[REG_SP] & ~(BP_WORD - 1); // Round down to nearest word
    R[REG_SP] = R[REG_SP] & ~0x10;          // Align stack to 16-bytes

    // Build pointer vectors on stack
    // - env
    copy_int_to_stack(0);       // Null-terminate vector
    for (int i = env_addr_i - 1; i >= 0; --i)
        R[REG_A2] = copy_int_to_stack(env_addrs[i]);

    // - argv
    copy_int_to_stack(0);       // Null-terminate vector
    for (int i = arg_addr_i - 1; i >= 0; --i)
        R[REG_A1] = copy_int_to_stack(arg_addrs[i]);

    // argc
    R[REG_A0] = argc;
    write_word(R[REG_SP] -= BP_WORD, argc, EMU_ACCESS);
}


/* ========================================================================== *
 * Stack Loading
 * ========================================================================== */

static memaddr_t copy_int_to_stack(int32_t val)
{
    write_word(R[REG_SP] -= BP_WORD, val, EMU_ACCESS);
    return R[REG_SP];
}

static memaddr_t copy_str_to_stack(char *str)
{
    int i = (int)strlen(str);
    while (i >= 0) {
        write_byte(--R[REG_SP], str[i], EMU_ACCESS);
        --i;
    }
    return R[REG_SP];
}


/* ========================================================================== *
 * Assembling
 * ========================================================================== */

static inline void end_of_assembly_file(void)
{
    flush_local_labels();
    set_segment(TEXT_SEG);
}

bool read_program(FILE* file, char *filename)
{
    set_segment(TEXT_SEG);
    initialize_scanner(file);
    initialize_parser(filename);

    ctx.state = PARSE_STATE;
    while (!yyparse() && !ctx.parse_error_occurred);
    ctx.state = EMU_STATE;

    pop_scanner();

    if (ctx.parse_error_occurred)
    {
        fe->error("Error loading file: %s\n", filename);
        return false;
    }

    fe->write_log(1, "Loaded: %s\n", filename);

    fclose(file);
    end_of_assembly_file();
    return true;
}

/* Reads a program from a buffer and returns true if parsed without error.
 * `buffer_opened` will equal true if the buffer was opened succesfully.
 * - Certain frontends can fail gracefully/recoverably if the file did not
 *   begin parsing, so this flag  helps to signal this situation. */
bool read_program_buffer(void *buffer, size_t buffer_len, char *buffer_name, bool *buffer_opened)
{
    FILE* file;

    file = fmemopen(buffer, buffer_len, "r");

    if (file == NULL) {
        *buffer_opened = false;
        fe->error("Cannot open memory stream: %s\n", buffer_name);
        return false;
    }

    *buffer_opened = true;

    return read_program(file, buffer_name);
}

/* Reads the program file and returns true if parsed without error.
 * `file_opened` will equal true if the file was opened succesfully.
 * - Certain frontends can fail gracefully/recoverably if the file did not
 *   begin parsing, so this flag  helps to signal this situation. */
bool read_program_file(char *filename, bool *file_opened)
{
    FILE *file;

    file = fopen(filename, "r");

    if (file == NULL) {
        *file_opened = false;
        fe->error("Cannot open file: %s\n", filename);
        return false;
    }
    
    *file_opened = true;

    return read_program(file, filename);
}

/* Parse the exception file, checking that it contains the required
 *   startup symbol.
 * `char *filename`: If NULL is passed, DEFAULT_SYSTEM_FILE will be read.
 * Returns true when the file is parsed without error and it defines
 *   the startup symbol. */
bool read_exception_file(char *filename)
{
    // If no exception file, use default
    bool file_opened;
    bool read_success;

    if (filename == NULL) {
        filename = DEFAULT_SYSTEM_FILE;
        read_success = read_program_buffer(system_default_s, system_default_s_len, filename, &file_opened);
    } else {
        read_success = read_program_file(filename, &file_opened);
    }

    if (!read_success) {
        fe->error("Failed to parse exception file: %s\n", filename);
        return false;
    }

    // Require program start symbol from exception handler
    label_s *l;
    lookup_or_make_label(DEFAULT_RUN_LOCATION, &l);
    if (!symbol_is_defined(l)) {
        fe->error("Exception file must define symbol: \"%s\"\n",
                  DEFAULT_RUN_LOCATION);
        return false;
    }

    // Set program address that the current text segment location counter
    ctx.program_addr = segloc(TEXT_SEG);

    return true;
}


/* ========================================================================== *
 * Breakpoint Management
 * ========================================================================== */

/* Delete the breakpoint at the given address, returning its id.
 * Returns -1 if no breakpoint found. */
unsigned int delete_breakpoint(memaddr_t addr)
{
    // Loop through breakpoints to find one with the given address
    for (bkpt_s *prev = NULL, *curr = breakpoints;
            curr != NULL;
            prev = curr, curr = curr->next) {
        if (curr->addr == addr) {
            // Found the breakpoint to delete
            unsigned int ret = curr->id;
            delete_bkpt_node(curr, prev);
            return ret;
        }
    }

    fe->error("No breakpoint to delete at 0x%08x\n", addr);
    return -1;
}


/* Delete the breakpoint with the given id, returning its address.
 * Returns 0 if no breakpoint found. */
memaddr_t delete_breakpoint_id(unsigned int id)
{
    // Loop through breakpoints to find one with the given id
    for (bkpt_s *prev = NULL, *curr = breakpoints;
            curr != NULL;
            prev = curr, curr = curr->next) {
        if (curr->id == id) {
            // Found the breakpoint to delete
            unsigned int ret = curr->addr;
            delete_bkpt_node(curr, prev);
            return ret;
        }
    }
    fe->error("No breakpoint to delete with id: %d\n", id);
    return 0;
}

int delete_all_breakpoints(void)
{
    // Loop through breakpoints, deleting all unconditionally
    int count = 0;
    while (breakpoints != NULL) {
        delete_bkpt_node(breakpoints, NULL);
        ++count;
    }
    return count;
}

static bkpt_s *delete_bkpt_node(bkpt_s *curr, bkpt_s *prev)
{
    // Write instruction back into IM
    write_inst(curr->addr, curr->inst, EMU_ACCESS);

    // Instruction copy from `set_breakpoint` needs to be freed
    free(curr->inst);

    // Remove from list
    if (!prev)
        breakpoints = curr->next;
    else
        prev->next = curr->next;

    // Return next node
    bkpt_s *next = curr->next;
    free(curr);
    return next;
}

unsigned int add_breakpoint_id(memaddr_t addr, unsigned int id)
{
    inst_s *inst;

    if ((inst = set_breakpoint(addr)) == NULL)
        return 0;

    bkpt_s *bp = (bkpt_s *)xmalloc(sizeof(bkpt_s));
    bp->addr = addr;
    bp->inst = inst;
    bp->id = id != 0 ? id : next_bkpt_id++;

    bp->next = breakpoints;
    breakpoints = bp;

    return bp->id;
}

/* Returns a pointer to the  breakpointed instruction at the given address.
 * Returns NULL if no breakpoint.
 * NOTE: This instruction pointer's lifetime is tied to the breakpoint node,
 *       so this should not be held past a exectution step. */
inst_s *inst_at_breakpoint_addr(memaddr_t addr)
{
    // Loop through breakpoints to find one with the given address
    for (bkpt_s *curr = breakpoints; curr != NULL; curr = curr->next) {
        if (curr->addr == addr) {
            // Found the breakpoint
            return curr->inst;
        }
    }

    return NULL;
}

void print_breakpoints(void)
{
    sstream_s ss;
    ss_init(&ss);

    if (breakpoints) {
        ss_printf(&ss, "--- Breakpoints ---\n");
        for (bkpt_s *b = breakpoints; b != NULL; b = b->next) {
            if (b->inst) {
                char *inst_str = inst_to_string(b->inst);
                ss_printf(&ss, "- %3d: %s\n", b->id, inst_str);
                free(inst_str);
            } else {
                ss_printf(&ss, "- %3d:[0x%08x]\n", b->id, b->addr);
            }
        }
    } else {
        ss_printf(&ss, "No breakpoints set\n");
    }

    char *str = ss_to_string(&ss);
    fe->error("%s", str);
    free(str);
}


/* ========================================================================== *
 * Program Execution
 * ========================================================================== */

/* Run the program for @steps instructions. If @steps is 0, run for a default
 * amount of steps. Return execution status. */
runres_e exec_program(int steps)
{
    runres_e res = RUN_CONTINUABLE;

    // If steps is zero, use default run steps
    if (!steps)
        steps = DEFAULT_RUN_STEPS;

    // Skip any starting breakpoints and execute the swapped instruction
    // Read first instruction to determine if it's a breakpoint
    ctx.exception_occurred = false;
    inst_s *first_inst = read_inst(PC, EMU_ACCESS);
    if (ctx.exception_occurred || !first_inst) {
        fe->run_error("Attempt to execute non-instruction at 0x%08x\n", PC);
        return RUN_STOPPED;
    }

    if (inst_is_db_breakpoint(first_inst)) {
        // Debugger breakpoint, swap with saved instruction for 1 cycle
        memaddr_t addr = PC;
        unsigned int bkpt_id = delete_breakpoint(addr);

        ctx.state = EXEC_STATE;
        res = run_asm(1);
        ctx.state = EMU_STATE;
        --steps;

        add_breakpoint_id(addr, bkpt_id);

        // Continue execution if there's a db breakpoint on an explicit `ebreak`
        // - NOTE: This shouldn't be possible, but just in case
        if (res == RUN_BREAKPOINT)
            res = RUN_CONTINUABLE;

    } else if (first_inst->opcode == Y_EBREAK_OP) {
        // Explicit breakpoint, just skip over it
        PC += BP_WORD;
        --steps;
    }

    if (steps && res == RUN_CONTINUABLE) {
        ctx.state = EXEC_STATE;
        res = run_asm(steps);
        ctx.state = EMU_STATE;
    }

    // Bubble up the result of `run_asm` to the frontend
    return res;
}
