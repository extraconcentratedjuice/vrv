/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <fenv.h>
#include <math.h>
#include <setjmp.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef _WIN32
#define VC_EXTRALEAN
#include <Windows.h>
#else
#include <errno.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/time.h>
#endif

#include "csr.h"
#include "frontend.h"
#include "inst.h"
#include "map.h"
#include "memaccess.h"
#include "param.h"
#include "parser.tab.h"
#include "reg.h"
#include "run.h"
#include "symbol.h"
#include "syscall.h"
#include "tty.h"
#include "utils.h"


/* Disable MS VS warning about constant predicate in conditional. */
#ifdef _MSC_BUILD
#pragma warning(disable: 4127)
#endif


/*
 * Macros
 */

/* If bit set, interpret factor as signed (MODE[1] => rs1, MODE[0] => rs2) */
#define MULH_MODE       3 /* signed x signed */
#define MULHSU_MODE     2 /* signed x unsigned */
#define MULHU_MODE      0 /* unsigned x unsigned */


/*
 * Exported variables
 */

bool force_break = false;   // For the execution env. to force an execution break


/*
 * Local variables
 */

/* Union for easy access to floating-point bit representation */
typedef union floatbits {
    regword_t b;
    fregword_t f;
} floatbits_u;

/* Floating-point canonical NaN is defined as this value in the spec. */
static floatbits_u rv32f_qnan = { .b = 0x7fc00000 };

/* Define `issignaling` for BSD (only defined in GNU's math.h) */
#ifndef issignaling
#define issignaling(v) \
    (!(((floatbits_u)(v)).b & 0x00400000) && (((floatbits_u)(v)).b & 0x003fffff))
#endif


/*
 * Local functions
 */

/* Timer functions */
static void bump_timer(void);
static void start_timer(void);
#ifdef _WIN32
void CALLBACK timer_completion_routine(LPVOID lpArgToCompletionRoutine,
                                       DWORD dwTimerLowValue,
                                       DWORD dwTimerHighValue);
#endif

/* Instruction helpers */
static regword_t multiply_hi(regword_t v1, regword_t v2, int mul_mode);
static void set_fflags(void);

/* Execution helpers */
static void check_for_interrupt(void);

/* Mapped IO */
static void check_memory_mapped_io(void);


/* ========================================================================== *
 * Timer Functions
 * ========================================================================== */

#ifdef _WIN32
/* Windows callback function */
void CALLBACK timer_completion_routine(LPVOID lpArgToCompletionRoutine,
                                       DWORD dwTimerLowValue,
                                       DWORD dwTimerHighValue)
{
    lpArgToCompletionRoutine = lpArgToCompletionRoutine;
    dwTimerLowValue = dwTimerLowValue;
    dwTimerHighValue = dwTimerHighValue;
    bump_timer();
}
#endif

/* Increment `mtime` register and test if it matches the `mtimecmp` register.
 * If so, cause an interrupt. */
static void bump_timer(void)
{
    // Increment `mtime`
    ++(C[CSR_MTIME]);
    if (!C[CSR_MTIME])
        ++(C[CSR_MTIMEH]);

    // Compare with `mtimecmp` and raise interrupt if equal
    if (C[CSR_MTIMEH] > C[CSR_MTIMECMPH]
            || (C[CSR_MTIMEH] == C[CSR_MTIMECMPH] && C[CSR_MTIME] >= C[CSR_MTIMECMP])) {
        raise_interrupt(INT_M_TIMER_BIT);
    }
}

static void start_timer()
{
#ifdef _WIN32
    HANDLE timer = CreateWaitableTimer(NULL, TRUE, TEXT("VRVTimer"));
    if (NULL == timer) {
        fe->error("CreateWaitableTimer failed");
    } else {
        LARGE_INTEGER interval;
        // Interval has 100 nsec units, negative value signals relative timer
        interval.u.LowPart = conf.timer_tick_ms * -10000;
        interval.u.HighPart = -1;

        if (!SetWaitableTimer(timer, &interval, 1, timer_completion_routine, 0, FALSE)) {
            fe->error("SetWaitableTimer failed");
        }
    }
#else
    /* Should use `ITIMER_VIRTUAL` delivering `SIGVTALRM`, but that does not seem
     * to work under Cygwin, so we'll adopt the lowest common denominator and
     * use real time.
     *
     * We ignore the resulting signal, however, and read the timer with getitimer,
     * since signals interrupt I/O calls, such as read, and make user
     * interaction with VRV work very poorly. Since speed isn't an important
     * aspect of VRV, polling isn't a big deal. */
    if (SIG_ERR == signal(SIGALRM, SIG_IGN)) {
        FE_PERROR("signal failed");
    } else {
        struct itimerval time;
        if (-1 == getitimer(ITIMER_REAL, &time)) {
            FE_PERROR("getitmer failed");
        }
        if (time.it_value.tv_usec == 0 && time.it_value.tv_sec == 0) {
            /* Timer is expired or has not been started.
             * Start a non-periodic timer for `timer_tick_ms` milliseconds. */
            time.it_interval.tv_sec = 0;
            time.it_interval.tv_usec = 0;
            time.it_value.tv_sec = 0;
            time.it_value.tv_usec = conf.timer_tick_ms * 1000;
            if (-1 == setitimer(ITIMER_REAL, &time, NULL)) {
                FE_PERROR("setitmer failed");
            }
        }
    }
#endif
}


/* ========================================================================== *
 * Instruction Helpers
 * ========================================================================== */

/* Generic instruction for performing all CSR actions */
static uregword_t csr_action(imm_t csr_encoding, uregword_t value, uregword_t mask)
{
    // Lookup CSR index and ensure it exists within VRV
    csr_encoding_s *csr = SEARCH_CSR_ENCODING(csr_encoding);
    if (!csr) {
        fe->error("Unrecognized CSR encountered during execution: 0x%03x\n", csr_encoding);
        raise_exception(EXC_INST_ILLEGAL, 0);
        return 0;
    }

    // Ensure CSR is readable by the current access level
    if (!csr_can_read(csr_encoding, ctx.current_access_level)) {
        raise_exception(EXC_INST_ILLEGAL, 0);
        return 0;
    }

    // If mask is non-zero, ensure that the CSR can be written to
    if (mask && !csr_can_write(csr_encoding, ctx.current_access_level)) {
        raise_exception(EXC_INST_ILLEGAL, 0);
        return 0;
    }

    // Read then write csr value
    uregword_t old_value = get_csr_field(csr->index, csr->shift, csr->mask);
    set_csr_field(csr->index, csr->shift, csr->mask & mask, value);

    /* Reset mtime interrupt when mtimecmp gets updated */
    if (csr->index == CSR_MTIMECMP || csr->index == CSR_MTIMECMPH)
        lower_interrupt(INT_M_TIMER_BIT);

    return old_value;
}

/* Multiply two 32-bit numbers, `rs1` and `rs2`
 * to produce high 32-bits of the full 64-bit result.
 *
 *            A B
 *          x C D
 * --------------
 *       AD || BD
 * AC || CB ||  0
 *
 * where A and B are the high and low short words of `rs1`,
 * C and D are the short words of `rs2`, AD is the product of A and D,
 * and X || Y is (X << 16) + Y. Since the algorithm is programmed in C,
 * we need to be careful not to overflow.
 *
 * The low 32-bits of this product is produced by `mul` instruction and can be
 * found simply through multiplication in C, ignoring overflow.
 * The low part is unaffected by the desired signedness.
 */
static regword_t multiply_hi(regword_t rs1, regword_t rs2, int mul_mode)
{
    uregword_t a, b, c, d;
    uregword_t bd, ad, cb, ac;
    uregword_t mid, bd_hi, mid2, carry_mid = 0;
    uregword_t hi, lo;
    bool negate = false;

    if ((mul_mode & 2) && rs1 < 0) {
        negate = true;
        rs1 = -rs1;
    }
    if ((mul_mode & 1) && rs2 < 0) {
        negate = !negate;
        rs2 = -rs2;
    }

    a = (rs1 >> 16) & 0xffff;
    b = rs1 & 0xffff;
    c = (rs2 >> 16) & 0xffff;
    d = rs2 & 0xffff;
    bd = b * d;
    ad = a * d;
    cb = c * b;
    ac = a * c;

    /* Need to add `ad + cb` and then the hi part of `bd`, so do this in
     * two stages to account for overflow after each addition. */
    mid = ad + cb;
    if (mid < ad || mid < cb)
        carry_mid = 1;
    bd_hi = ((bd >> 16) & 0xffff);
    mid2 = mid + bd_hi;
    if (mid2 < mid || mid2 < bd_hi)
        ++carry_mid;

    hi = ac + (carry_mid << 16) + ((mid2 >> 16) & 0xffff);
    if (negate) {
        hi = ~hi;
        // Account for signed-carry from low part
        lo = (bd & 0xffff) | ((mid2 & 0xffff) << 16);
        if (!lo)
            ++hi;
    }

    return hi;
}

/* Accrue floating point exception flags.
 * It is up to software to reset VRV fflags, so we clear the hardware flags
 *   after each call to `set_fflags` to ensure we do not carry over
 *   any flags previously cleared within VRV program. */
static void set_fflags(void)
{
    // Inform compiler we will be manually accessing fp exception flags
    // #pragma STDC FENV_ACCESS on
    /* ^ This pragma is unsupported by all but the most niche compilers.
     *   It is "possible" to implicitly  enable the above through compiler flags.
     *   However, many targets don't support those flags.
     *   We hope the target implicity gives access to `fenv.h` interface,
     *     and that's the most that we can do. */

    int fflags = GET_FFLAGS;    // Accrue the flags cumulatively

    if (fetestexcept(FE_INVALID))
        fflags |= 0x10;
    if (fetestexcept(FE_DIVBYZERO))
        fflags |= 0x08;
    if (fetestexcept(FE_OVERFLOW))
        fflags |= 0x04;
    if (fetestexcept(FE_UNDERFLOW))
        fflags |= 0x02;
    if (fetestexcept(FE_INEXACT))
        fflags |= 0x01;

    // Clear hardware fp exception flags
    feclearexcept(FE_ALL_EXCEPT);

    // We are done accessing fp exception flags
    // #pragma STDC FENV_ACCESS off

    SET_FFLAGS(fflags);
}


/* ========================================================================== *
 * Instruction Macros
 * ========================================================================== */

/*
 * FP epilogues
 */

#define FP_CLEAR        feclearexcept(FE_ALL_EXCEPT)

/* For non-arithmetic float operations that need to clear hardware fe flags */
#define FP_OP(freg_flag)               \
do {                                   \
    /* #pragma STDC FENV_ACCESS on */  \
    feclearexcept(FE_ALL_EXCEPT);      \
    /* #pragma STDC FENV_ACCESS off */ \
    freg_flag = true;                  \
} while (0)

/* For arithmetic float operations that set `fflags` and canonicalize nans */
#define FP_OP_ARITH(v, freg_flag)    \
do {                                 \
    if (isnan(v))                    \
        v = rv32f_qnan.f;            \
    set_fflags();                    \
    freg_flag = true;                \
} while (0)


/* ========================================================================== *
 * Mapped IO
 * ========================================================================== */

static void check_memory_mapped_io(void)
{
    // @NOTE: Hard-coded for TTY device
    check_tty_device();
}


/* ========================================================================== *
 * Execution Helpers
 * ========================================================================== */

static void check_for_interrupt(void)
{
    // Ignore if no pending bits or if globally disabled
    if (!C[CSR_MIP] || !GET_MSTATUS_MIE)
        return;

    uregword_t enabled_mip = C[CSR_MIP] & C[CSR_MIE];

    // Check each instruction in order of precedence:
    // - Highest ID first, but timer last
    for (uint32_t intcode = 31, intbit = (1 << intcode);
            intbit > 0;
            --intcode, intbit >>= 1) {
        if (enabled_mip & intbit) {
            raise_exception(INTERRUPT_BIT | intcode, 0);
            break;
        }
    }
}


/* ========================================================================== *
 * Exception Raising and Handling
 * ========================================================================== */

/* Excpetion occurred during program execution, send exception to kernel. */
void raise_exception(excode_e ex, uregword_t mtval)
{
    ctx.exception_occurred = true;

    // Exception raised from machine-mode, send to VRV to prevent infinite loop
    if (ctx.current_access_level == M_MODE_ACCESS) {
        print_exception(ex, mtval);
        fe->run_error("  Exception occurred in machine mode, panic!\n");
        return;
    } else if (ctx.current_access_level == EMU_ACCESS) {
        print_exception(ex, mtval);
        fe->run_error("  Exception occurred outside emulated program\n");
        return;
    }

    // Set `mcause` to the exception code
    C[CSR_MCAUSE] = ex;

    // Set `mtval` register with the given value
    C[CSR_MTVAL] = mtval;

    // Switch to machine-mode
    ctx.current_access_level = M_MODE_ACCESS;

    // Disable interrupts while saving previos MIE
    SET_MSTATUS_MPIE(GET_MSTATUS_MIE);
    SET_MSTATUS_MIE(0);

    // Set mepc with current instruction address, then jump to `mtvec`
    C[CSR_MEPC] = PC;
    PC = C[CSR_MTVEC] & ~0x3;   /* Lower 2 bits are trap vector mode */

    /* If cause is interrupt and in "vectored" trap mode, offset PC by cause */
    if ((ex & INTERRUPT_BIT) && (C[CSR_MTVEC] & 0x3) == TRAP_VECTORED)
        PC += (ex ^ INTERRUPT_BIT) * BP_WORD;
}

/* Used to display exceptions that occur outside of program execution.
 * E.g. erroneous storage when parsing or invalid instruction read
 *   from within the kernel. */
void print_exception(excode_e ex, memaddr_t mtval)
{
    switch (ex) {
        case EXC_INST_MISALIGN:
            fe->write_log(1, "Misaligned instruction address: 0x%08x\n", mtval);
            break;
        case EXC_INST_ACCESS:
            fe->write_log(1, "Instruction access fault: 0x%08x\n", mtval);
            break;
        case EXC_INST_ILLEGAL:
            fe->write_log(1, "Illegal instruction: 0x%08x\n", mtval);
            break;
        case EXC_LOAD_MISALIGN:
            fe->write_log(1, "Misaligned load address: 0x%08x\n", mtval);
            break;
        case EXC_LOAD_ACCESS:
            fe->write_log(1, "Load access fault: 0x%08x\n", mtval);
            break;
        case EXC_STORE_MISALIGN:
            fe->write_log(1, "Misaligned store address: 0x%08x\n", mtval);
            break;
        case EXC_STORE_ACCESS:
            fe->write_log(1, "Store access fault: 0x%08x\n", mtval);
            break;
        default:
            fe->write_log(1,
                "Unknown runtime exception encountered outside program execution: 0x%08x, 0x%08x\n",
                ex, mtval);
    }
}


/* ========================================================================== *
 * Assembly Execution
 * ========================================================================== */

/* Run the program stored in memory, starting at address `initial_PC`
 *   for `steps_to_run` instruction executions.
 * If flag `display` is true, print each instruction before it executes.
 * Return an enum specifying why the run loop has exited. */
runres_e run_asm(int steps_to_run)
{
    /* When using mapped IO, run in discrete intervals, checking and updating
     *   IO registers at the start of each interval.
     * Otherwise, run all steps in one single interval. */
    int next_step = conf.mapped_tty ? conf.io_interval : steps_to_run;

    // Start a timer running
    start_timer();

    // Maintain invariant value
    R[REG_ZERO] = 0;

    for (int step_size = MIN(next_step, steps_to_run);
             steps_to_run > 0;
             steps_to_run -= step_size, step_size = MIN(next_step, steps_to_run)) {

        // Check and update IO registers at the start of each IO interval
        if (conf.mapped_tty)
            check_memory_mapped_io();

        force_break = false;
        for (int step = 0; step < step_size; ++step) {
            if (force_break)
                return RUN_CONTINUABLE;

#ifdef _WIN32
            // Put thread in awaitable state for WaitableTimer
            SleepEx(0, TRUE);
#else
            {
                // Poll for timer expiration
                struct itimerval time;
                if (-1 == getitimer(ITIMER_REAL, &time)) {
                    FE_PERROR("getitmer failed");
                }
                if (time.it_value.tv_usec == 0 && time.it_value.tv_sec == 0) {
                    // Timer expired
                    bump_timer();

                    // Restart timer for next interval
                    start_timer();
                }
            }
#endif

            /* @NOTE: This was perviously done every `io_interval` steps
             *        and only when `mapped_tty` was on. Timer inetrupts were
             *        directly sent as exceptions. However, it seemed best to
             *        consolidate the interrupt handling system and so, for now,
             *        interrupts are checked every cycle. Though this can be
             *        changed to have an unconditional interrupt interval. */
            // Check for any pending and enabled interrupts
            check_for_interrupt();

            // If enabled, break to calling enviornment on exception
            if (ctl.break_on_exceptions && ctx.exception_occurred) {
                fe->write_log(1, "Exception encountered, breaking to frontend...\n");
                return RUN_BREAKPOINT;
            }

            // Read instruction at PC and check for error
            ctx.exception_occurred = false;
            inst_s *inst = read_inst(PC, ctx.current_access_level);
            if (ctx.exception_occurred) {
                // Exception occurred during memory read
                continue;
            } else if (!inst) {
                raise_exception(EXC_INST_ILLEGAL, PC);
                continue;
            } else if (inst->expr && inst->expr->symbol && !symbol_is_defined(inst->expr->symbol)) {
                fe->run_error("Instruction references undefined symbol at 0x%08x\n--> %s\n",
                        PC, inst_to_string(inst));
                return RUN_STOPPED;
            }

            if (ctl.verbose_lvl >= 2) {
                // Print the instruction
                char *inst_str = inst_to_string(inst);
                fe->write_log(2, "%s\n", inst_str);
                free(inst_str);
                // Test assembly instruction to ensure encoding and decoding match
                test_assembly(inst);
            }

            // Initialize result values
            regword_t res = 0;
            fregword_t fres = 0;

            // Set cycle-wise flags
            bool inc_pc = true;
            bool store_freg = false;

            // Execute instruction
            switch (inst->opcode) {

                /* M-Mode */
                case Y_MRET_OP:
                    // Ensure this is called with the proper access level
                    if (ctx.current_access_level < M_MODE_ACCESS) {
                        fe->run_error(
                            "Attempt to execute privelaged instruction from user-mode at 0x%08x\n",
                            PC);
                        return RUN_STOPPED;
                    }
                    // Lower access level to user-mode
                    ctx.current_access_level = U_MODE_ACCESS;
                    // Restore `mstatus.mie` global interrupt flag
                    SET_MSTATUS_MIE(GET_MSTATUS_MPIE);
                    // Restore `PC` to continue user-mode program execution
                    PC = C[CSR_MEPC];
                    // It is up to the kernel to increment `mepc` when necessary
                    inc_pc = false;
                    break;

                /* Loads */
                case Y_LB_OP:
                    res = sign_ex(read_byte(inst->imm + R[inst->rs1], ctx.current_access_level), 8 * BP_BYTE);
                    break;
                case Y_LH_OP:
                    res = sign_ex(read_half(inst->imm + R[inst->rs1], ctx.current_access_level), 8 * BP_HALF);
                    break;
                case Y_LW_OP:
                    res = read_word(inst->imm + R[inst->rs1], ctx.current_access_level);
                    break;
                case Y_LBU_OP:
                    res = read_byte(inst->imm + R[inst->rs1], ctx.current_access_level);
                    break;
                case Y_LHU_OP:
                    res = read_half(inst->imm + R[inst->rs1], ctx.current_access_level);
                    break;

                /* Stores */
                case Y_SB_OP:
                    write_byte(inst->imm + R[inst->rs1], R[inst->rs2], ctx.current_access_level);
                    break;
                case Y_SH_OP:
                    write_half(inst->imm + R[inst->rs1], R[inst->rs2], ctx.current_access_level);
                    break;
                case Y_SW_OP:
                    write_word(inst->imm + R[inst->rs1], R[inst->rs2], ctx.current_access_level);
                    break;

                /* Arithmetic operations */
                case Y_ADD_OP:
                    res = R[inst->rs1] + R[inst->rs2];
                    break;
                case Y_SUB_OP:
                    res = R[inst->rs1] - R[inst->rs2];
                    break;
                case Y_SLL_OP:
                    res = R[inst->rs1] << R[inst->rs2];
                    break;
                case Y_SLT_OP:
                    res = (bool)(R[inst->rs1] < R[inst->rs2]);
                    break;
                case Y_SLTU_OP:
                    res = (bool)(UR[inst->rs1] < UR[inst->rs2]);
                    break;
                case Y_XOR_OP:
                    res = R[inst->rs1] ^ R[inst->rs2];
                    break;
                case Y_SRL_OP:
                    res = UR[inst->rs1] >> R[inst->rs2];
                    break;
                case Y_SRA_OP:
                    res = R[inst->rs1] >> R[inst->rs2];
                    break;
                case Y_OR_OP:
                    res = R[inst->rs1] | R[inst->rs2];
                    break;
                case Y_AND_OP:
                    res = R[inst->rs1] & R[inst->rs2];
                    break;

                /* Immediate arithmetic operations */
                case Y_ADDI_OP:
                    res = R[inst->rs1] + inst->imm;
                    break;
                case Y_SLTI_OP:
                    res = (bool)(R[inst->rs1] < inst->imm);
                    break;
                case Y_SLTIU_OP:
                    res = (bool)(UR[inst->rs1] < (uimm_t)inst->imm);
                    break;
                case Y_XORI_OP:
                    res = R[inst->rs1] ^ inst->imm;
                    break;
                case Y_ORI_OP:
                    res = R[inst->rs1] | inst->imm;
                    break;
                case Y_ANDI_OP:
                    res = R[inst->rs1] & inst->imm;
                    break;
                case Y_SLLI_OP:
                    res = R[inst->rs1] << inst->imm;
                    break;
                case Y_SRLI_OP:
                    res = (uregword_t)R[inst->rs1] >> inst->imm;
                    break;
                case Y_SRAI_OP:
                    res = R[inst->rs1] >> inst->imm;
                    break;

                /* Branches */
                case Y_BEQ_OP:
                    if (R[inst->rs1] == R[inst->rs2]) {
                        PC += inst->imm;
                        inc_pc = false;
                    }
                    break;
                case Y_BNE_OP:
                    if (R[inst->rs1] != R[inst->rs2]) {
                        PC += inst->imm;
                        inc_pc = false;
                    }
                    break;
                case Y_BLT_OP:
                    if (R[inst->rs1] < R[inst->rs2]) {
                        PC += inst->imm;
                        inc_pc = false;
                    }
                    break;
                case Y_BGE_OP:
                    if (R[inst->rs1] >= R[inst->rs2]) {
                        PC += inst->imm;
                        inc_pc = false;
                    }
                    break;
                case Y_BLTU_OP:
                    if (UR[inst->rs1] < UR[inst->rs2]) {
                        PC += inst->imm;
                        inc_pc = false;
                    }
                    break;
                case Y_BGEU_OP:
                    if (UR[inst->rs1] >= UR[inst->rs2]) {
                        PC = inst->imm;
                        inc_pc = false;
                    }
                    break;

                /* Jumps */
                case Y_JAL_OP:
                    res = PC + 4;
                    PC += inst->imm;
                    inc_pc = false;
                    break;
                case Y_JALR_OP:
                    res = PC + 4;
                    PC = inst->imm + R[inst->rs1];
                    inc_pc = false;
                    break;

                /* AUIPC/LUI */
                case Y_AUIPC_OP:
                    res = (inst->imm << 12) + PC;
                    break;
                case Y_LUI_OP:
                    res = inst->imm << 12;
                    break;

                /* SYSTEM */
                case Y_FENCE_OP:
                    /* NOP */
                    break;
                case Y_ECALL_OP:
                    // Test for VRV syscalls which should not be sent to kernel
                    if (R[REG_SYSCODE] < KERNEL_SYSCALL_BASE) {
                        switch (do_syscall()) {
                            case SYSRES_EXIT:
                                return RUN_STOPPED;
                            case SYSRES_WAIT_FOR_INPUT:
                                return RUN_WAIT_FOR_INPUT;
                            default: {}
                        }
                    } else {
                        raise_exception(ctx.current_access_level == U_MODE_ACCESS
                            ? EXC_U_ECALL : EXC_M_ECALL, 0);
                        inc_pc = false;
                    }
                    break;
                case Y_EBREAK_OP:
                    return RUN_BREAKPOINT;  // Return to calling function

                /* Zifencei-ext */
                case Y_FENCEI_OP:
                    /* NOP */
                    break;

                /* Zicsr-ext */
                case Y_CSRRW_OP:
                    // `rs2` directly contains CSR encoding
                    // When `rs1==x0`, signal that no write is being attempted
                    res = csr_action(inst->rs2, R[inst->rs1], inst->rs1 ? ~0u : 0u);
                    break;
                case Y_CSRRS_OP:
                    res = csr_action(inst->rs2, ~0u, R[inst->rs1]);
                    break;
                case Y_CSRRC_OP:
                    res = csr_action(inst->rs2, 0u, R[inst->rs1]);
                    break;
                case Y_CSRRWI_OP:
                    res = csr_action(inst->rs2, inst->imm, ~0u);
                    break;
                case Y_CSRRSI_OP:
                    res = csr_action(inst->rs2, ~0u, inst->imm);
                    break;
                case Y_CSRRCI_OP:
                    res = csr_action(inst->rs2, 0u, inst->imm);
                    break;

                /* M-ext */
                case Y_MUL_OP:
                    res = R[inst->rs1] * R[inst->rs2];
                    break;
                case Y_MULH_OP:
                    res = multiply_hi(R[inst->rs1], R[inst->rs2], MULH_MODE);
                    break;
                case Y_MULHSU_OP:
                    res = multiply_hi(R[inst->rs1], R[inst->rs2], MULHSU_MODE);
                    break;
                case Y_MULHU_OP:
                    res = multiply_hi(R[inst->rs1], R[inst->rs2], MULHU_MODE);
                    break;
                case Y_DIV_OP:
                    if (R[inst->rs1] == IMM_MIN(32) && R[inst->rs2] == -1)
                        // RV32 spec defines divisiion overflow to result in 2^{XLEN - 1}
                        res = R[inst->rs1];
                    else
                        // RV32 spec defines integer DivByZero to set all dest bits
                        res = R[inst->rs2] ? R[inst->rs1] / R[inst->rs2] : -1;
                    break;
                case Y_DIVU_OP:
                    // RV32 spec defines integer DivByZero to set all dest bits
                    res = UR[inst->rs2] ? UR[inst->rs1] / UR[inst->rs2] : (uregword_t)(-1);
                    break;
                case Y_REM_OP:
                    if (R[inst->rs1] == IMM_MIN(32) && R[inst->rs2] == -1)
                        // RV32 spec defines remainder overflow to result in 0
                        res = 0;
                    else
                        // RV32 spec defines integer RemByZero to return `rs1`
                        res = R[inst->rs2] ? R[inst->rs1] % R[inst->rs2] : R[inst->rs1];
                    break;
                case Y_REMU_OP:
                    // RV32 spec defines integer RemByZero to return `rs1`
                    res = UR[inst->rs2] ? UR[inst->rs1] % UR[inst->rs2] : UR[inst->rs1];
                    break;

                /* F-ext - loads and stores */
                case Y_FLW_OP:
                    res = read_word(inst->imm + R[inst->rs1], ctx.current_access_level);
                    fres = ((floatbits_u)res).f;
                    FP_OP(store_freg);
                    break;
                case Y_FSW_OP:
                    write_word(inst->imm + R[inst->rs1],
                               ((floatbits_u)FR[inst->rs2]).b,
                               ctx.current_access_level);
                    FP_CLEAR;
                    break;

                /* F-ext - int/float moves */
                case Y_FMV_X_W_OP:
                    res = ((floatbits_u)FR[inst->rs1]).b;
                    break;
                case Y_FMV_W_X_OP:
                    fres = ((floatbits_u)R[inst->rs1]).f;
                    FP_OP(store_freg);
                    break;

                /* F-ext - fused multiply-adds */
                case Y_FMADD_S_OP:
                    fres = fmaf(FR[inst->rs1], FR[inst->rs2], FR[inst->rs3]);
                    FP_OP_ARITH(fres, store_freg);
                    break;
                case Y_FMSUB_S_OP:
                    fres = fmaf(FR[inst->rs1], FR[inst->rs2], -FR[inst->rs3]);
                    FP_OP_ARITH(fres, store_freg);
                    break;
                case Y_FNMSUB_S_OP:
                    fres = fmaf(-FR[inst->rs1], FR[inst->rs2], -FR[inst->rs3]);
                    FP_OP_ARITH(fres, store_freg);
                    break;
                case Y_FNMADD_S_OP:
                    fres = fmaf(-FR[inst->rs1], FR[inst->rs2], FR[inst->rs3]);
                    FP_OP_ARITH(fres, store_freg);
                    break;

                /* F-ext - binary operators */
                case Y_FADD_S_OP:
                    fres = FR[inst->rs1] + FR[inst->rs2];
                    FP_OP_ARITH(fres, store_freg);
                    break;
                case Y_FSUB_S_OP:
                    fres = FR[inst->rs1] - FR[inst->rs2];
                    FP_OP_ARITH(fres, store_freg);
                    break;
                case Y_FMUL_S_OP:
                    fres = FR[inst->rs1] * FR[inst->rs2];
                    FP_OP_ARITH(fres, store_freg);
                    break;
                case Y_FDIV_S_OP:
                    fres = FR[inst->rs1] / FR[inst->rs2];
                    FP_OP_ARITH(fres, store_freg);
                    break;

                /* F-ext - unary operators */
                case Y_FSQRT_S_OP:
                    fres = sqrtf(FR[inst->rs1]);
                    FP_OP_ARITH(fres, store_freg);
                    break;

                /* F-ext - sign injections */
                case Y_FSGNJ_S_OP:
                    fres = copysignf(FR[inst->rs1], FR[inst->rs2]);
                    /* "Sign-injection instructions do not set floating-point
                     *   exception flags, nor do they canonicalize NaNs." */
                    FP_OP(store_freg);
                    break;
                case Y_FSGNJN_S_OP:
                    fres = copysignf(FR[inst->rs1], -FR[inst->rs2]);
                    FP_OP(store_freg);
                    break;
                case Y_FSGNJX_S_OP:
                    fres = copysignf(FR[inst->rs1],
                        signbit(FR[inst->rs1]) ^ signbit(FR[inst->rs2]) ? -1.0f : 1.0f);
                    FP_OP(store_freg);
                    break;

                /* F-ext - min/max */
                case Y_FMIN_S_OP:
                    fres = fminf(FR[inst->rs1], FR[inst->rs2]);
                    FP_OP_ARITH(fres, store_freg);
                    break;
                case Y_FMAX_S_OP:
                    fres = fmaxf(FR[inst->rs1], FR[inst->rs2]);
                    FP_OP_ARITH(fres, store_freg);
                    break;

                /* F-ext - int/float conversions */
                /* For `fcvt`, spec says "If the rounded result is not
                 *   representable in the destination format, it is clipped to
                 *   the nearest value and the invalid flag is set."
                 * However, C does not seem to generate fe flags on cast
                 *   conversions, so this is a @TODO to attempt this effect. */
                case Y_FCVT_W_S_OP:
                    res = (regword_t)FR[inst->rs1];
                    FP_CLEAR;
                    break;
                case Y_FCVT_WU_S_OP:
                    res = (uregword_t)FR[inst->rs1];
                    FP_CLEAR;
                    break;
                case Y_FCVT_S_W_OP:
                    fres = (fregword_t)R[inst->rs1];
                    FP_OP(store_freg);
                    break;
                case Y_FCVT_S_WU_OP:
                    fres = (fregword_t)UR[inst->rs1];
                    FP_OP(store_freg);
                    break;

                /* F-ext - comparisons */
                /* `fcmp` instructions can set invalid flag under certain
                 *    conditions. Thankfully, IEEE abiding fpus seem to follow
                 *    the same conventions that the RISC-V spec defines. */
                case Y_FEQ_S_OP:
                    res = FR[inst->rs1] == FR[inst->rs2];
                    set_fflags();
                    break;
                case Y_FLT_S_OP:
                    res = FR[inst->rs1] < FR[inst->rs2];
                    set_fflags();
                    break;
                case Y_FLE_S_OP:
                    res = FR[inst->rs1] <= FR[inst->rs2];
                    set_fflags();
                    break;

                /* F-ext - classifiers */
                case Y_FCLASS_S_OP:
                    if (isnan(FR[inst->rs1])) {
                        res = issignaling(FR[inst->rs1]) ? 8 : 9;
                    } else {
                        int class = fpclassify(FR[inst->rs1]);
                        res = !signbit(FR[inst->rs1]) << 2; // neg->0, pos->4
                        switch (class) {
                            case FP_ZERO:
                                res |= 3;
                                break;
                            case FP_SUBNORMAL:
                                res |= 2;
                                break;
                            case FP_NORMAL:
                                res |= 1;
                                break;
                            // FP_INFINITE does not set any bits
                        }
                    }
                    FP_CLEAR;
                    break;

                /* Unknown instruction */
                default:
                    fe->run_error("Unknown instruction type: %d at 0x%08x\n", inst->opcode, PC);
                    return RUN_STOPPED;
            }

            if (!ctx.exception_occurred) {
                // Set destination register to result
                if (store_freg) {
                    // FP operation with destination, store float result
                    FR[inst->rd] = fres;

                } else if (inst->rd) {
                    // No exception and instruction is expecting result, so store it
                    R[inst->rd] = res;
                }

                // Increment PC, skip if branch or jump has occurred
                if (inc_pc)
                    PC += BP_WORD;

            } else if (ctl.break_on_exceptions) {
                // If enabled, break to calling enviornment on exception
                fe->write_log(1, "Exception encountered, breaking to frontend...\n");
                return RUN_BREAKPOINT;
            }

            // Maintain invariant value
            R[REG_ZERO] = 0;

            // Increment cycle and instret registers
            ++(C[CSR_MCYCLE]);
            ++(C[CSR_MINSTRET]);
        }
    }

    // Executed enought steps, return `true` to signal we can continue
    return RUN_CONTINUABLE;
}
