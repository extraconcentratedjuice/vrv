/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <stdbool.h>
#include <stdio.h>

#include "frontend.h"
#include "memaccess.h"
#include "param.h"
#include "run.h"
#include "tty.h"
#include "utils.h"


/*
 * Inline functions
 */

static inline bool close_to_stack_bot(memaddr_t addr)
{
    return addr < (&M[STACK_SEG])->lower_bound
        && (&M[STACK_SEG])->lower_bound - addr < SEG_GUARD;
}

static inline bool read_access(seg_idx_e seg_i, access_level_e access_lvl)
{
    return seg_i >= 0
        && (access_lvl == EMU_ACCESS || memseg_can_read((M + seg_i), access_lvl));
}

static inline bool write_access(seg_idx_e seg_i, access_level_e access_lvl)
{
    return seg_i >= 0
        && (access_lvl == EMU_ACCESS || memseg_can_write((M + seg_i), access_lvl));
}

static inline bool check_alignment(memaddr_t addr, access_level_e access_lvl,
                                   int bytes, memop_e memop)
{
    // Alignment succeeds if access from emulator,
    //   misaligned exceptions are off, or address is aligned
    return access_lvl == EMU_ACCESS
        || !(ctl.misaligned_exceptions & memop) || !(addr & (bytes - 1));
}


/* ========================================================================== *
 * Exceptions
 * ========================================================================== */

void raise_mem_exception(excode_e ex, memaddr_t addr, int seg_i)
{
    // Adjust exception type for text segments
    if (is_text_seg(seg_i)) {
        if (ex == EXC_LOAD_ACCESS)
            ex = EXC_INST_ACCESS;
        else if (ex == EXC_LOAD_MISALIGN)
            ex = EXC_INST_MISALIGN;
    }

    // Handle exception differently based on current state of emulator
    switch (ctx.state) {
        case EXEC_STATE:
            raise_exception(ex, addr);
            break;

        case PARSE_STATE:
            ctx.exception_occurred = true;
            ctx.parse_error_occurred = true;
            print_exception(ex, addr);
            fe->error("  Exception occurred during parsing\n");
            break;

        case EMU_STATE:
            ctx.exception_occurred = true;
            print_exception(ex, addr);
            fe->error("  Exception occurred outside program execution\n");
            break;
    }
}


/* ========================================================================== *
 * Lookup
 * ========================================================================== */

seg_idx_e segment_index_from_addr(memaddr_t addr, uint32_t num_bytes)
{
    for (int i = 0; i < MEMORY_SEG_COUNT; ++i)
        if (memseg_contains_range(&M[i], addr, num_bytes))
            return i;
    if (addr >= K_DEVICE_BOT)
        return K_DEVICE_SEG;
    return UNKNOWN_SEG;
}


/* ========================================================================== *
 * MMREG Handling
 * ========================================================================== */

/* Handles a read to a memory-mapped device register, returning the read value.
 * Returns 0 if unmapped address.
 * Will only cause exceptions for access from user-mode,
 *   but device access may cause interrupts as well. */
static memword_t handle_device_read(memaddr_t addr, int num_bytes, access_level_e al)
{
    if (al < M_MODE_ACCESS) {
        raise_mem_exception(EXC_LOAD_ACCESS, addr, K_DEVICE_SEG);
        return 0;
    }

    /* @TEMP: Hard-coded handling of TTY accesses */
    if (addr_in_tty_range(addr, num_bytes)) {
        return tty_read(addr - TTY_ADDR_BOT, num_bytes, al);
    }

    fe->write_log(1, "Warning: reading from unmapped register address: 0x$08x\n", addr);
    return 0;
}

/* Handles a write to a memory-mapped device register.
 * Will only cause exceptions for access from user-mode,
 *   but device access may cause interrupts as well. */
static void handle_device_write(memaddr_t addr, memword_t data, int num_bytes, access_level_e al)
{
    if (al < M_MODE_ACCESS) {
        raise_mem_exception(EXC_STORE_ACCESS, addr, K_DEVICE_SEG);
        return;
    }

    /* @TEMP: Hard-coded handling of TTY accesses */
    if (addr_in_tty_range(addr, num_bytes)) {
        tty_write(addr - TTY_ADDR_BOT, data, num_bytes, al);
        return;
    }

    fe->write_log(1, "Warning: writing to unmapped register address: 0x$08x\n", addr);
}


/* ========================================================================== *
 * Reading
 * ========================================================================== */

uint8_t read_byte(memaddr_t addr, access_level_e al)
{
    seg_idx_e seg_i = segment_index_from_addr(addr, BP_BYTE);

    if (seg_i == K_DEVICE_SEG)
        return handle_device_read(addr, BP_BYTE, al);

    if (!read_access(seg_i, al)) {
        raise_mem_exception(EXC_LOAD_ACCESS, addr, seg_i);
        return 0;
    }

    int8_t data = 0;
    bool success = memseg_read_byte(&M[seg_i], addr, &data);

    // The prior checks should ensure this call succeeds, but just to be sure
    if (!success) {
        raise_mem_exception(EXC_LOAD_ACCESS, addr, seg_i);
        return 0;
    }

    return data;
}

uint16_t read_half(memaddr_t addr, access_level_e al)
{
    seg_idx_e seg_i = segment_index_from_addr(addr, BP_HALF);

    if (seg_i == K_DEVICE_SEG)
        return handle_device_read(addr, BP_HALF, al);

    if (!check_alignment(addr, al, BP_HALF, MEMOP_LOAD)) {
        raise_mem_exception(EXC_LOAD_MISALIGN, addr, seg_i);
        return 0;
    }
    if (!read_access(seg_i, al)) {
        raise_mem_exception(EXC_LOAD_ACCESS, addr, seg_i);
        return 0;
    }

    int16_t data = 0;
    bool success = memseg_read_half(&M[seg_i], addr, &data);

    // The prior checks should ensure this call succeeds, but just to be sure
    if (!success) {
        raise_mem_exception(EXC_LOAD_ACCESS, addr, seg_i);
        return 0;
    }

    return data;
}

uint32_t read_word(memaddr_t addr, access_level_e al)
{
    seg_idx_e seg_i = segment_index_from_addr(addr, BP_WORD);

    if (seg_i == K_DEVICE_SEG)
        return handle_device_read(addr, BP_WORD, al);

    if (!check_alignment(addr, al, BP_WORD, MEMOP_LOAD)) {
        raise_mem_exception(EXC_LOAD_MISALIGN, addr, seg_i);
        return 0;
    }

    if (!read_access(seg_i, al)) {
        raise_mem_exception(EXC_LOAD_ACCESS, addr, seg_i);
        return 0;
    }

    int32_t data = 0;
    bool success = memseg_read_word(&M[seg_i], addr, &data);

    // The prior checks should ensure this call succeeds, but just to be sure
    if (!success) {
        raise_mem_exception(EXC_LOAD_ACCESS, addr, seg_i);
        return 0;
    }

    return data;
}


/* ========================================================================== *
 * Writing
 * ========================================================================== */

static int try_expand_stack(memaddr_t addr)
{
    // Write address is close to stack bottom, so  try to expand it
    expand_memory_clamped(STACK_SEG, conf.stack_max_size);

    // Check again in case stack did not expand enough
    if (segment_index_from_addr(addr, BP_BYTE) == UNKNOWN_SEG) {
        fe->write_log(1, "Warning: stack overflow\n");
        return 0;
    }

    return 1;
}

void write_byte(memaddr_t addr, int8_t data, access_level_e al)
{
    seg_idx_e seg_i = segment_index_from_addr(addr, BP_BYTE);

    if (seg_i == K_DEVICE_SEG) {
        handle_device_write(addr, data, BP_BYTE, al);
        return;
    }

    if (seg_i == UNKNOWN_SEG) {
        if (close_to_stack_bot(addr) && try_expand_stack(addr)) {
            seg_i = STACK_SEG;
        } else {
            raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
            return;
        }
    }

    if (!write_access(seg_i, al)) {
        raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
        return;
    }

    bool success = memseg_write_byte(&M[seg_i], addr, data);

    if (!success) {
        raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
    }
}

void write_half(memaddr_t addr, int16_t data, access_level_e al)
{
    seg_idx_e seg_i = segment_index_from_addr(addr, BP_HALF);

    if (seg_i == K_DEVICE_SEG) {
        handle_device_write(addr, data, BP_HALF, al);
        return;
    }

    if (!check_alignment(addr, al, BP_HALF, MEMOP_STORE)) {
        raise_mem_exception(EXC_STORE_MISALIGN, addr, seg_i);
        return;
    }

    if (seg_i == UNKNOWN_SEG) {
        if (close_to_stack_bot(addr) && try_expand_stack(addr)) {
            seg_i = STACK_SEG;
        } else {
            raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
            return;
        }
    }

    if (!write_access(seg_i, al)) {
        raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
        return;
    }

    bool success = memseg_write_half(&M[seg_i], addr, data);

    if (!success) {
        raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
    }
}

void write_word(memaddr_t addr, int32_t data, access_level_e al)
{
    seg_idx_e seg_i = segment_index_from_addr(addr, BP_WORD);

    if (seg_i == K_DEVICE_SEG) {
        handle_device_write(addr, data, BP_WORD, al);
        return;
    }

    if (!check_alignment(addr, al, BP_WORD, MEMOP_STORE)) {
        raise_mem_exception(EXC_STORE_MISALIGN, addr, seg_i);
        return;
    }

    if (seg_i == UNKNOWN_SEG) {
        if (close_to_stack_bot(addr) && try_expand_stack(addr)) {
            seg_i = STACK_SEG;
        } else {
            raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
            return;
        }
    }

    if (!write_access(seg_i, al)) {
        raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
        return;
    }

    bool success = memseg_write_word(&M[seg_i], addr, data);

    if (!success) {
        raise_mem_exception(EXC_STORE_ACCESS, addr, seg_i);
    }
}
