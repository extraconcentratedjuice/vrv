/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef OP_H
#define OP_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#include "memseg.h"
#include "utils.h"

/*
 * Type of each entry
 */

typedef enum optype {

    ASM_DIR = 0,
    PSEUDO_OP,

    NOARG_TYPE_INST,

    R_TYPE_INST,
    R2_TYPE_INST,

    R_FP_TYPE_INST,
    R_FP_RM_TYPE_INST,
    R_F2X_TYPE_INST,
    R2_FP_RM_TYPE_INST,
    R4_FP_RM_TYPE_INST,
    R2_F2X_TYPE_INST,
    R2_F2X_RM_TYPE_INST,
    R2_X2F_TYPE_INST,
    R2_X2F_RM_TYPE_INST,

    I_TYPE_INST,
    I_LOAD_TYPE_INST,
    I_FP_LOAD_TYPE_INST,
    I_SHIFT_TYPE_INST,
    I_JALR_TYPE_INST,
    I_FENCE_TYPE_INST,
    I_CSR_TYPE_INST,
    I_CSRI_TYPE_INST,

    S_TYPE_INST,
    S_FP_TYPE_INST,

    B_TYPE_INST,

    U_TYPE_INST,
    U_PCREL_TYPE_INST,

    J_TYPE_INST,

} optype_e;


/*
 * Op entry type
 */

typedef struct op {
    char *name;
    int i_opcode;
    optype_e type;
    memword_t a_opcode;
} op_s;


/*
 * Opcodes
 */

#define O_LOAD              0x03
#define O_LOAD_FP           0x07
// BLANK                    0x0B
#define O_MISC_MEM          0x0F
#define O_OP_IMM            0x13
#define O_AUIPC             0x17
#define O_OP_IMM_32         0x1B

#define O_STORE             0x23
#define O_STORE_FP          0x27
// BLANK                    0x2B
#define O_AMO               0x2F
#define O_OP                0x33
#define O_LUI               0x37
#define O_OP_32             0x3B

#define O_MADD              0x43
#define O_MSUB              0x47
#define O_NMSUB             0x4B
#define O_NMADD             0x4F
#define O_OP_FP             0x53
// BLANK                    0x57
// BLANK                    0x5B

#define O_BRANCH            0x63
#define O_JALR              0x67
// BLANK                    0x6B
#define O_JAL               0x6F
#define O_SYSTEM            0x73
// BLANK                    0x77
// BLANK                    0x7B


/*
 * Funct shifts
 */

#define FUNCT3(B)   ENC_FUNCT3(B)
#define FUNCT7(B)   ENC_FUNCT7(B)


/*
 * Reg shifts
 */

#define R_RS2(B)    ENC_RS2(B)

#ifdef __cplusplus
}
#endif

#endif // OP_H  // OP_H
