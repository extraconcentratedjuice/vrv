/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "frontend.h"
#include "sstream.h"
#include "utils.h"

#ifndef SS_BUF_LENGTH
/* Initialize length of buffer */
#define SS_BUF_LENGTH 256
#endif

void ss_init(sstream_s *ss)
{
    ss->buf = (char *)malloc(SS_BUF_LENGTH);
    ss->max_length = SS_BUF_LENGTH;
    ss->empty_pos = 0;
    ss->initialized = 1;
}

void ss_destroy(sstream_s *ss)
{
    if (ss) {
        free(ss->buf);
    }
}

void ss_clear(sstream_s *ss)
{
    if (0 == ss->initialized)
        ss_init(ss);

    ss->empty_pos = 0;
}

void ss_erase(sstream_s *ss, int n)
{
    if (0 == ss->initialized)
        ss_init(ss);

    ss->empty_pos -= n;
    if (ss->empty_pos < 0)
        ss->empty_pos = 0;
}

int ss_length(sstream_s *ss)
{
    if (0 == ss->initialized)
        ss_init(ss);

    return ss->empty_pos;
}

char *ss_to_string(sstream_s *ss)
{
    if (0 == ss->initialized)
        ss_init(ss);

    if (ss->empty_pos == ss->max_length) {
        /* Not enough room to store output: increase buffer size and try again */
        ss->max_length = ss->max_length + 1;
        ss->buf = (char *)realloc(ss->buf, (size_t)ss->max_length);
        if (NULL == ss->buf)
            fe->fatal_error("realloc failed\n");
    }
    ss->buf[ss->empty_pos] = '\0';  /* Null terminate string */
    ss->empty_pos += 1;
    return ss->buf;
}

void ss_printf(sstream_s *ss, char *fmt, ...)
{
    int free_space;
    int n;
    va_list args;

    va_start(args, fmt);

    if (0 == ss->initialized)
        ss_init(ss);

    free_space = ss->max_length - ss->empty_pos;
#ifdef _WIN32
    /* Returns -1 when buffer is too small */
    while ((n = _vsnprintf(ss->buf + ss->empty_pos, free_space, fmt, args)) < 0)
#else
    /* Returns necessary space when buffer is too small */
    while ((n = vsnprintf(
            ss->buf + ss->empty_pos, free_space, fmt, args)) >= free_space)
#endif
    {
        /* Not enough room to store output: double buffer size and try again */
        ss->max_length = 2 * ss->max_length;
        ss->buf = (char *)realloc(ss->buf, (size_t)ss->max_length);
        free_space = ss->max_length - ss->empty_pos;
        if (NULL == ss->buf)
            fe->fatal_error("realloc failed\n");

        va_end(args);               /* Restart argument pointer */
        va_start(args, fmt);
    }
    ss->empty_pos += n;

    /* Null terminate string (for debugging) if there is enough room */
    if (ss->empty_pos < ss->max_length)
        ss->buf[ss->empty_pos] = '\0';

    va_end(args);
}

void ss_print_byte(sstream_s *ss, char byte)
{
    if (isprint(byte))
        ss_printf(ss, "%c", byte);
    else
        ss_printf(ss, ".");
}
