/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/* Information on each keyword token that can be read by VRV.
 * Used to generate compile-time arrays which can be sorted for lookups.
 * -- Must be sorted before use -- */

/*
 * Directives
 */
{ ".equ",       Y_EQU_DIR,          ASM_DIR,              -1 },
{ ".align",     Y_ALIGN_DIR,        ASM_DIR,              -1 },
{ ".globl",     Y_GLOBL_DIR,        ASM_DIR,              -1 },
{ ".comm",      Y_COMM_DIR,         ASM_DIR,              -1 },
{ ".common",    Y_COMM_DIR,         ASM_DIR,              -1 },
{ ".section",   Y_SECTION_DIR,      ASM_DIR,              -1 },
{ ".text",      Y_TEXT_DIR,         ASM_DIR,              -1 },
{ ".data",      Y_DATA_DIR,         ASM_DIR,              -1 },
{ ".ktext",     Y_KTEXT_DIR,        ASM_DIR,              -1 },
{ ".kdata",     Y_KDATA_DIR,        ASM_DIR,              -1 },
{ ".string",    Y_STRING_TERM_DIR,  ASM_DIR,              -1 },
{ ".asciz",     Y_STRING_TERM_DIR,  ASM_DIR,              -1 },
{ ".ascii",     Y_STRING_DIR,       ASM_DIR,              -1 },
{ ".byte",      Y_BYTE_DIR,         ASM_DIR,              -1 },
{ ".2byte",     Y_2BYTE_DIR,        ASM_DIR,              -1 },
{ ".half",      Y_HALF_DIR,         ASM_DIR,              -1 },
{ ".short",     Y_SHORT_DIR,        ASM_DIR,              -1 },
{ ".4byte",     Y_4BYTE_DIR,        ASM_DIR,              -1 },
{ ".word",      Y_WORD_DIR,         ASM_DIR,              -1 },
{ ".long",      Y_LONG_DIR,         ASM_DIR,              -1 },
{ ".float",     Y_FLOAT_DIR,        ASM_DIR,              -1 },
{ ".zero",      Y_ZERO_DIR,         ASM_DIR,              -1 },


/*
 * RV32I M-Mode Instructions
 */
{ "mret",       Y_MRET_OP,          NOARG_TYPE_INST,      O_SYSTEM | 0x30200000 },


/*
 * RV32I Instructions
 */
/* LOAD */
{ "lb",         Y_LB_OP,            I_LOAD_TYPE_INST,     O_LOAD | FUNCT3(0x0) },
{ "lh",         Y_LH_OP,            I_LOAD_TYPE_INST,     O_LOAD | FUNCT3(0x1) },
{ "lw",         Y_LW_OP,            I_LOAD_TYPE_INST,     O_LOAD | FUNCT3(0x2) },
{ "lbu",        Y_LBU_OP,           I_LOAD_TYPE_INST,     O_LOAD | FUNCT3(0x4) },
{ "lhu",        Y_LHU_OP,           I_LOAD_TYPE_INST,     O_LOAD | FUNCT3(0x5) },

/* STORE */
{ "sb",         Y_SB_OP,            S_TYPE_INST,          O_STORE | FUNCT3(0x0) },
{ "sh",         Y_SH_OP,            S_TYPE_INST,          O_STORE | FUNCT3(0x1) },
{ "sw",         Y_SW_OP,            S_TYPE_INST,          O_STORE | FUNCT3(0x2) },

/* OP */
{ "add",        Y_ADD_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x0) | FUNCT7(0x00) },
{ "sub",        Y_SUB_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x0) | FUNCT7(0x20) },
{ "sll",        Y_SLL_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x1) | FUNCT7(0x00) },
{ "slt",        Y_SLT_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x2) | FUNCT7(0x00) },
{ "sltu",       Y_SLTU_OP,          R_TYPE_INST,          O_OP | FUNCT3(0x3) | FUNCT7(0x00) },
{ "xor",        Y_XOR_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x4) | FUNCT7(0x00) },
{ "srl",        Y_SRL_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x5) | FUNCT7(0x00) },
{ "sra",        Y_SRA_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x5) | FUNCT7(0x20) },
{ "or",         Y_OR_OP,            R_TYPE_INST,          O_OP | FUNCT3(0x6) | FUNCT7(0x00) },
{ "and",        Y_AND_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x7) | FUNCT7(0x00) },

/* OP-IMM */
{ "addi",       Y_ADDI_OP,          I_TYPE_INST,          O_OP_IMM | FUNCT3(0x0) },
{ "slti",       Y_SLTI_OP,          I_TYPE_INST,          O_OP_IMM | FUNCT3(0x2) },
{ "sltiu",      Y_SLTIU_OP,         I_TYPE_INST,          O_OP_IMM | FUNCT3(0x3) },
{ "xori",       Y_XORI_OP,          I_TYPE_INST,          O_OP_IMM | FUNCT3(0x4) },
{ "ori",        Y_ORI_OP,           I_TYPE_INST,          O_OP_IMM | FUNCT3(0x6) },
{ "andi",       Y_ANDI_OP,          I_TYPE_INST,          O_OP_IMM | FUNCT3(0x7) },

/* OP-IMM - shamt */
{ "slli",       Y_SLLI_OP,          I_SHIFT_TYPE_INST,    O_OP_IMM | FUNCT3(0x1) | FUNCT7(0x00) },
{ "srli",       Y_SRLI_OP,          I_SHIFT_TYPE_INST,    O_OP_IMM | FUNCT3(0x5) | FUNCT7(0x00) },
{ "srai",       Y_SRAI_OP,          I_SHIFT_TYPE_INST,    O_OP_IMM | FUNCT3(0x5) | FUNCT7(0x20) },

/* BRANCH */
{ "beq",        Y_BEQ_OP,           B_TYPE_INST,          O_BRANCH | FUNCT3(0x0) },
{ "bne",        Y_BNE_OP,           B_TYPE_INST,          O_BRANCH | FUNCT3(0x1) },
{ "blt",        Y_BLT_OP,           B_TYPE_INST,          O_BRANCH | FUNCT3(0x4) },
{ "bge",        Y_BGE_OP,           B_TYPE_INST,          O_BRANCH | FUNCT3(0x5) },
{ "bltu",       Y_BLTU_OP,          B_TYPE_INST,          O_BRANCH | FUNCT3(0x6) },
{ "bgeu",       Y_BGEU_OP,          B_TYPE_INST,          O_BRANCH | FUNCT3(0x7) },

/* JAL & JALR */
{ "jal",        Y_JAL_OP,           J_TYPE_INST,          O_JAL },
{ "jalr",       Y_JALR_OP,          I_JALR_TYPE_INST,     O_JALR | FUNCT3(0x0) },

/* AUPIC & LUI */
{ "auipc",      Y_AUIPC_OP,         U_PCREL_TYPE_INST,    O_AUIPC },
{ "lui",        Y_LUI_OP,           U_TYPE_INST,          O_LUI },

/* MISC-MEM */
{ "fence",      Y_FENCE_OP,         I_FENCE_TYPE_INST,    O_MISC_MEM },

/* SYSTEM */
{ "ecall",      Y_ECALL_OP,         NOARG_TYPE_INST,      O_SYSTEM | 0x00000000 },
{ "ebreak",     Y_EBREAK_OP,        NOARG_TYPE_INST,      O_SYSTEM | 0x00100000 },

/*
 * Zifencei Instructions
 */
{ "fence.i",    Y_FENCEI_OP,        NOARG_TYPE_INST,      O_MISC_MEM | FUNCT3(0x1) },

/*
 * Zicsr Instructions
 */
{ "csrrw",      Y_CSRRW_OP,         I_CSR_TYPE_INST,      O_SYSTEM | FUNCT3(0x1) },
{ "csrrs",      Y_CSRRS_OP,         I_CSR_TYPE_INST,      O_SYSTEM | FUNCT3(0x2) },
{ "csrrc",      Y_CSRRC_OP,         I_CSR_TYPE_INST,      O_SYSTEM | FUNCT3(0x3) },
{ "csrrwi",     Y_CSRRWI_OP,        I_CSRI_TYPE_INST,     O_SYSTEM | FUNCT3(0x5) },
{ "csrrsi",     Y_CSRRSI_OP,        I_CSRI_TYPE_INST,     O_SYSTEM | FUNCT3(0x6) },
{ "csrrci",     Y_CSRRCI_OP,        I_CSRI_TYPE_INST,     O_SYSTEM | FUNCT3(0x7) },

/*
 * RV32M Instructions
 */
{ "mul",        Y_MUL_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x0) | FUNCT7(0x01) },
{ "mulh",       Y_MULH_OP,          R_TYPE_INST,          O_OP | FUNCT3(0x1) | FUNCT7(0x01) },
{ "mulhsu",     Y_MULHSU_OP,        R_TYPE_INST,          O_OP | FUNCT3(0x2) | FUNCT7(0x01) },
{ "mulhu",      Y_MULHU_OP,         R_TYPE_INST,          O_OP | FUNCT3(0x3) | FUNCT7(0x01) },
{ "div",        Y_DIV_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x4) | FUNCT7(0x01) },
{ "divu",       Y_DIVU_OP,          R_TYPE_INST,          O_OP | FUNCT3(0x5) | FUNCT7(0x01) },
{ "rem",        Y_REM_OP,           R_TYPE_INST,          O_OP | FUNCT3(0x6) | FUNCT7(0x01) },
{ "remu",       Y_REMU_OP,          R_TYPE_INST,          O_OP | FUNCT3(0x7) | FUNCT7(0x01) },

/*
 * RV32F Instructions
 */
/* LOAD-FP */
{ "flw",        Y_FLW_OP,           I_FP_LOAD_TYPE_INST,  O_LOAD_FP | FUNCT3(0x2) },

/* LOAD-FP */
{ "fsw",        Y_FSW_OP,           S_FP_TYPE_INST,       O_STORE_FP | FUNCT3(0x2) },

/* Fused Multiply-Add */
{ "fmadd.s",    Y_FMADD_S_OP,       R4_FP_RM_TYPE_INST,   O_MADD  | FUNCT7(0x00) },
{ "fmsub.s",    Y_FMSUB_S_OP,       R4_FP_RM_TYPE_INST,   O_MSUB  | FUNCT7(0x00) },
{ "fnmsub.s",   Y_FNMSUB_S_OP,      R4_FP_RM_TYPE_INST,   O_NMSUB | FUNCT7(0x00) },
{ "fnmadd.s",   Y_FNMADD_S_OP,      R4_FP_RM_TYPE_INST,   O_NMADD | FUNCT7(0x00) },

/* OP-FP */
{ "fadd.s",     Y_FADD_S_OP,        R_FP_RM_TYPE_INST,    O_OP_FP | FUNCT7(0x00) },
{ "fsub.s",     Y_FSUB_S_OP,        R_FP_RM_TYPE_INST,    O_OP_FP | FUNCT7(0x04) },
{ "fmul.s",     Y_FMUL_S_OP,        R_FP_RM_TYPE_INST,    O_OP_FP | FUNCT7(0x08) },
{ "fdiv.s",     Y_FDIV_S_OP,        R_FP_RM_TYPE_INST,    O_OP_FP | FUNCT7(0x0C) },

{ "fsqrt.s",    Y_FSQRT_S_OP,       R2_FP_RM_TYPE_INST,   O_OP_FP | FUNCT7(0x2C) },

{ "fsgnj.s",    Y_FSGNJ_S_OP,       R_FP_TYPE_INST,       O_OP_FP | FUNCT3(0x0) | FUNCT7(0x10) },
{ "fsgnjn.s",   Y_FSGNJN_S_OP,      R_FP_TYPE_INST,       O_OP_FP | FUNCT3(0x1) | FUNCT7(0x10) },
{ "fsgnjx.s",   Y_FSGNJX_S_OP,      R_FP_TYPE_INST,       O_OP_FP | FUNCT3(0x2) | FUNCT7(0x10) },
{ "fmin.s",     Y_FMIN_S_OP,        R_FP_TYPE_INST,       O_OP_FP | FUNCT3(0x0) | FUNCT7(0x14) },
{ "fmax.s",     Y_FMAX_S_OP,        R_FP_TYPE_INST,       O_OP_FP | FUNCT3(0x1) | FUNCT7(0x14) },

{ "fcvt.w.s",   Y_FCVT_W_S_OP,      R2_F2X_RM_TYPE_INST,  O_OP_FP | R_RS2(0x0) | FUNCT7(0x60) },
{ "fcvt.wu.s",  Y_FCVT_WU_S_OP,     R2_F2X_RM_TYPE_INST,  O_OP_FP | R_RS2(0x1) | FUNCT7(0x60) },

{ "fmv.x.w",    Y_FMV_X_W_OP,       R2_F2X_TYPE_INST,     O_OP_FP | FUNCT3(0x0) | R_RS2(0x0) | FUNCT7(0x70) },
{ "fclass.s",   Y_FCLASS_S_OP,      R2_F2X_TYPE_INST,     O_OP_FP | FUNCT3(0x1) | R_RS2(0x0) | FUNCT7(0x70) },

{ "feq.s",      Y_FEQ_S_OP,         R_F2X_TYPE_INST,      O_OP_FP | FUNCT3(0x2) | FUNCT7(0x50) },
{ "flt.s",      Y_FLT_S_OP,         R_F2X_TYPE_INST,      O_OP_FP | FUNCT3(0x1) | FUNCT7(0x50) },
{ "fle.s",      Y_FLE_S_OP,         R_F2X_TYPE_INST,      O_OP_FP | FUNCT3(0x0) | FUNCT7(0x50) },

{ "fcvt.s.w",   Y_FCVT_S_W_OP,      R2_X2F_RM_TYPE_INST,  O_OP_FP | R_RS2(0x0) | FUNCT7(0x68) },
{ "fcvt.s.wu",  Y_FCVT_S_WU_OP,     R2_X2F_RM_TYPE_INST,  O_OP_FP | R_RS2(0x1) | FUNCT7(0x68) },

{ "fmv.w.x",    Y_FMV_W_X_OP,       R2_X2F_TYPE_INST,     O_OP_FP | FUNCT3(0x0) | R_RS2(0x0) | FUNCT7(0x78) },


/*
 * RV32I Pseudoinstructions
 */
{ "la",         Y_LLA_POP,          PSEUDO_OP,            -1 }, /* Alias of `lla` */
{ "lla",        Y_LLA_POP,          PSEUDO_OP,            -1 },

{ "nop",        Y_NOP_POP,          PSEUDO_OP,            -1 },
{ "li",         Y_LI_POP,           PSEUDO_OP,            -1 },
{ "mv",         Y_MV_POP,           PSEUDO_OP,            -1 },
{ "not",        Y_NOT_POP,          PSEUDO_OP,            -1 },
{ "neg",        Y_NEG_POP,          PSEUDO_OP,            -1 },
{ "seqz",       Y_SEQZ_POP,         PSEUDO_OP,            -1 },
{ "snez",       Y_SNEZ_POP,         PSEUDO_OP,            -1 },
{ "sltz",       Y_SLTZ_POP,         PSEUDO_OP,            -1 },
{ "sgtz",       Y_SGTZ_POP,         PSEUDO_OP,            -1 },

{ "fmv.s",      Y_FMV_S_POP,        PSEUDO_OP,            -1 },
{ "fabs.s",     Y_FABS_S_POP,       PSEUDO_OP,            -1 },
{ "fneg.s",     Y_FNEG_S_POP,       PSEUDO_OP,            -1 },

{ "beqz",       Y_BEQZ_POP,         PSEUDO_OP,            -1 },
{ "bnez",       Y_BNEZ_POP,         PSEUDO_OP,            -1 },
{ "blez",       Y_BLEZ_POP,         PSEUDO_OP,            -1 },
{ "bgez",       Y_BGEZ_POP,         PSEUDO_OP,            -1 },
{ "bltz",       Y_BLTZ_POP,         PSEUDO_OP,            -1 },
{ "bgtz",       Y_BGTZ_POP,         PSEUDO_OP,            -1 },

{ "bgt",        Y_BGT_POP,          PSEUDO_OP,            -1 },
{ "ble",        Y_BLE_POP,          PSEUDO_OP,            -1 },
{ "bgtu",       Y_BGTU_POP,         PSEUDO_OP,            -1 },
{ "bleu",       Y_BLEU_POP,         PSEUDO_OP,            -1 },

{ "j",          Y_J_POP,            PSEUDO_OP,            -1 },
{ "jr",         Y_JR_POP,           PSEUDO_OP,            -1 },
{ "ret",        Y_RET_POP,          PSEUDO_OP,            -1 },
{ "call",       Y_CALL_POP,         PSEUDO_OP,            -1 },
{ "tail",       Y_TAIL_POP,         PSEUDO_OP,            -1 },

{ "rdinstret",  Y_RDINSTRET_POP,    PSEUDO_OP,            -1 },
{ "rdinstreth", Y_RDINSTRETH_POP,   PSEUDO_OP,            -1 },
{ "rdcycle",    Y_RDCYCLE_POP,      PSEUDO_OP,            -1 },
{ "rdcycleh",   Y_RDCYCLEH_POP,     PSEUDO_OP,            -1 },
{ "rdtime",     Y_RDTIME_POP,       PSEUDO_OP,            -1 },
{ "rdtimeh",    Y_RDTIMEH_POP,      PSEUDO_OP,            -1 },

{ "csrr",       Y_CSRR_POP,         PSEUDO_OP,            -1 },
{ "csrw",       Y_CSRW_POP,         PSEUDO_OP,            -1 },
{ "csrs",       Y_CSRS_POP,         PSEUDO_OP,            -1 },
{ "csrc",       Y_CSRC_POP,         PSEUDO_OP,            -1 },

{ "csrwi",      Y_CSRWI_POP,        PSEUDO_OP,            -1 },
{ "csrsi",      Y_CSRSI_POP,        PSEUDO_OP,            -1 },
{ "csrci",      Y_CSRCI_POP,        PSEUDO_OP,            -1 },

{ "frcsr",      Y_FRCSR_POP,        PSEUDO_OP,            -1 },
{ "fscsr",      Y_FSCSR_POP,        PSEUDO_OP,            -1 },
{ "frrm",       Y_FRRM_POP,         PSEUDO_OP,            -1 },
{ "fsrm",       Y_FSRM_POP,         PSEUDO_OP,            -1 },
{ "frflags",    Y_FRFLAGS_POP,      PSEUDO_OP,            -1 },
{ "fsflags",    Y_FSFLAGS_POP,      PSEUDO_OP,            -1 },
