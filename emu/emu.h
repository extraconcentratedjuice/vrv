/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef EMU_H
#define EMU_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdio.h>

#include "inst.h"
#include "memseg.h"
#include "run.h"


/*
 * Macros
 */

#define DEFAULT_RUN_LOCATION        "__mstart"
#define DEFAULT_SYSTEM_FILE         "<built-in system file>"

#define DEFAULT_RUN_STEPS           0x7fffffff


/* Initialization */
bool initialize_world(char *exception_filename);
void destroy_world(void);
void initialize_run_stack(int argc, char **argv);

/* Assembling */
bool read_program_buffer(void *buffer, size_t buffer_len, char *buffer_name, bool *buffer_opened);
bool read_program_file(char *filename, bool *file_opened);
bool read_exception_file(char *filename);

/* Breakpoint management */
int delete_all_breakpoints(void);
unsigned int delete_breakpoint(memaddr_t addr);
memaddr_t delete_breakpoint_id(unsigned int id);
unsigned int add_breakpoint_id(memaddr_t addr, unsigned int id);
inst_s *inst_at_breakpoint_addr(memaddr_t addr);
void print_breakpoints(void);

/* Program execution */
runres_e exec_program(int steps);


/*
 * Inline functions
 */

static inline unsigned int add_breakpoint(memaddr_t addr)
{
    return add_breakpoint_id(addr, 0);
}

#ifdef __cplusplus
}
#endif

#endif  // EMU_H
