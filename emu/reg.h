/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef REG_H
#define REG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "memseg.h"

/*
 * Typedefs
 */

typedef int32_t regword_t;
typedef uint32_t uregword_t;
typedef float fregword_t;


/*
 * Exported variables
 */

/* CPU registers */
#define R_LENGTH    32
#define UR          (uregword_t)R
extern regword_t R[R_LENGTH];
extern memaddr_t PC;

#define FR_LENGTH   32
extern fregword_t FR[FR_LENGTH];

/* Register names */
extern char *int_reg_names[R_LENGTH];


/*
 * Enums
 */

typedef enum regabi {
    REG_ZERO = 0,   // Hard-wired zero
    REG_RA,         // Return address
    REG_SP,         // Stack pointer
    REG_GP,         // Global pointer (middle of small data segment)
    REG_TP,         // Thread pointer (no internal use)
    REG_T0,         // Temporary registers, `t1` is temp address for `tail`
    REG_T1,
    REG_T2,
    REG_S0,         // Saved regiters, `s0` is frame pointer w/ no internal use
    REG_S1,
    REG_A0,         // Argument regsiters, `a0` is return value
    REG_A1,
    REG_A2,
    REG_A3,
    REG_A4,
    REG_A5,
    REG_A6,
    REG_A7,
    REG_S2,         // Extra saved registers
    REG_S3,
    REG_S4,
    REG_S5,
    REG_S6,
    REG_S7,
    REG_S8,
    REG_S9,
    REG_S10,
    REG_S11,
    REG_T3,         // Extra temporary registers
    REG_T4,
    REG_T5,
    REG_T6,
} regabi_e;

typedef enum fregabi {
    REG_FT0 = 0,    // Temporary registers
    REG_FT1,
    REG_FT2,
    REG_FT3,
    REG_FT4,
    REG_FT5,
    REG_FT6,
    REG_FT7,
    REG_FS0,        // Saved registers
    REG_FS1,
    REG_FA0,        // Argument registers, `fa0` is float return value
    REG_FA1,
    REG_FA2,
    REG_FA3,
    REG_FA4,
    REG_FA5,
    REG_FA6,
    REG_FA7,
    REG_FS2,        // Extra saved registers
    REG_FS3,
    REG_FS4,
    REG_FS5,
    REG_FS6,
    REG_FS7,
    REG_FS8,
    REG_FS9,
    REG_FS10,
    REG_FS11,
    REG_FT8,        // Extra temporary registers
    REG_FT9,
    REG_FT10,
    REG_FT11,
} fregabi_e;


/*
 * Macros
 */

/* Result registers */
#define REG_RES     REG_A0
#define FREG_RES    REG_FA0

/* Special argument registers */
#define REG_SYSCODE REG_A7

/* Pseudoinstruction registers */
#define REG_TAIL    REG_T1

/* Floating point registers. */
#define FREG_BIT    0x20 /* Equal to `R_LENGTH` */


/*
 * Exported functions
 */

void initialize_registers(void);

#ifdef __cplusplus
}
#endif

#endif  // REG_H
