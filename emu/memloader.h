/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef MEMLOADER_H
#define MEMLOADER_H

#ifdef __cplusplus
extern "C" {
#endif

#include "memseg.h"

/*
 * Memory layout definitions
 */

#define DEFAULT_SEG_SIZE    0x00010000
// Forced distance between segments to prevent accidental access
#define SEG_GUARD           0x00001000

/* Text segment: TEXT_BOT -> SMALL_DATA_BOT */
#define TEXT_BOT            0x00010000

/* Small data segment: SMALL_DATA_BOT -> SMALL_DATA_BOT + SMALL_DATA_SIZE */
#define SMALL_DATA_BOT      0x10000000
#define SMALL_DATA_SIZE     0x00001000
#define SMALL_DATA_MID      (SMALL_DATA_SIZE / 2 + SMALL_DATA_BOT)
#define SMALL_DATA_TOP      (SMALL_DATA_BOT + SMALL_DATA_SIZE)

/* Data segment: DATA_BOT (= SMALL_DATA_BOT + SMALL_DATA_SIZE) -> expandable */
#define DATA_BOT            (SMALL_DATA_BOT + SMALL_DATA_SIZE)

/* Stack segment: STACK_TOP -> expandable (downward) */
#define STACK_TOP           0x7ffff000

/* Kernel text/data segments: mirrors user segments but with highest-bit set */
#define K_TEXT_BOT          (0x80000000 | TEXT_BOT)
#define K_DATA_BOT          (0x80000000 | SMALL_DATA_BOT)

/* Kernel memory-mapped device register segment */
#define K_DEVICE_BOT        0xffff0000

/* Other layout definitions */
#define EXPANSION_FACTOR(s) ALIGN_UP((s) * 3 / 2, BP_QWORD)


/*
 * Segment indices - ordered by segment position and indexes `memory` array
 */

typedef enum seg_idx {
    UNKNOWN_SEG = -2,
    K_DEVICE_SEG,       // Not a real segment, so should not be counted
    TEXT_SEG = 0,
    SMALL_DATA_SEG,
    DATA_SEG,
    STACK_SEG,
    K_TEXT_SEG,
    K_DATA_SEG,
    MEMORY_SEG_COUNT
} seg_idx_e;


/*
 * Exported variables
 */

/* Orderd array of memory segments for use by `memacces.c` */
extern memseg_s M[MEMORY_SEG_COUNT];


/*
 * Exported functions
 */

/* Main memory management */
void clear_memory(void);
void initialize_memory(void);
void destroy_memory(void);

/* Memory loader operations */
memaddr_t loader_align(uint32_t byte_boundary);
seg_idx_e set_segment(seg_idx_e segment_index);

/* Memory loader information */
int current_segment_index(void);

/* Memory storing operations */
memaddr_t store_byte(int8_t data);
memaddr_t store_half(int16_t data);
memaddr_t store_word(int32_t data);

/* Memory segment operations (not necessarily used during loading) */
int32_t expand_memory(seg_idx_e segment_index);
int32_t expand_memory_clamped(seg_idx_e segment_index, uint32_t max_size);
int32_t memory_sbrk(seg_idx_e segment_index, int32_t byte_delta);


/*
 * Inline functions
 */

static inline bool is_text_seg(seg_idx_e seg_i)
{
    return seg_i == TEXT_SEG || seg_i == K_TEXT_SEG;
}

static inline bool is_kernel_seg(seg_idx_e seg_i)
{
    return seg_i == K_TEXT_SEG || seg_i == K_DATA_SEG || seg_i == K_DEVICE_SEG;
}

static inline memaddr_t segloc(seg_idx_e seg_i)
{
    return M[seg_i].loc_counter;
}

static inline memaddr_t loaderloc(void)
{
    return M[current_segment_index()].loc_counter;
}

#ifdef __cplusplus
}
#endif

#endif  // MEMLOADER_H
