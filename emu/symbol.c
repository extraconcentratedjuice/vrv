/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <stdio.h>
#include <stdlib.h>

#include "frontend.h"
#include "inst.h"
#include "memaccess.h"
#include "parser.tab.h"
#include "reg.h"
#include "sstream.h"
#include "symbol.h"
#include "utils.h"


/*
 * Macros
 */

#define HASHFACTOR 613
#define HASHBITS 30
#define LABEL_HASH_TABLE_SIZE 8191


/*
 * Macros
 */

#define SYM_ERROR(fmt, ...)          \
do {                                 \
    ctx.parse_error_occurred = true; \
    fe->error(fmt, __VA_ARGS__);     \
} while (0)


/*
 * Local functions
 */

/* Hashing */
static void get_hash(char *name, uint32_t *slot_no, label_s **entry);

/* Management */
static void copy_label_definition(label_s *dest, label_s *src);
static label_s *make_label(char *name, uint32_t hash_index);


/*
 * Local variables
 */

/* Keep track of the memory location that a label represents.  If we see a
 * reference to a label that is not yet defined, then record the reference so
 * that we can patch up the instruction when the label is defined.
 *
 * At the end of a file, we flush the hash table of all non-global labels so
 * they can't be seen in other files.
 */
static label_s *local_labels = NULL;    // Labels local to current file.

/* Map from name of a label to a label structure. */
static label_s *label_hash_table[LABEL_HASH_TABLE_SIZE];
static label_s *retired_label_hash_table[LABEL_HASH_TABLE_SIZE];


/* ========================================================================== *
 * Initialization + Destruction
 * ========================================================================== */

/* Clear the symbol table by removing and freeing old entries. */
void destroy_symbol_table(void)
{
    label_s *next;

    for (int i = 0; i < LABEL_HASH_TABLE_SIZE; i++) {
        // Clear entries in `label_hash_table`
        for (label_s *curr = label_hash_table[i]; curr != NULL; curr = next) {
            free(curr->name);
            next = curr->next;
            free(curr);
        }
        label_hash_table[i] = NULL;

        // Clear entries in `retired_label_hash_table`
        for (label_s *curr = retired_label_hash_table[i]; curr != NULL; curr = next) {
            free(curr->name);
            next = curr->next;
            free(curr);
        }
        retired_label_hash_table[i] = NULL;
    }

    // Local labels just references labels in the table, so we can just set to NULL
    local_labels = NULL;
}


/* ========================================================================== *
 * Hashing
 * ========================================================================== */

/* Lookup for a label with the given `name`.  Set the `slot_no` to be the
 * hash table bucket that contains (or would contain) the label's record.
 * If the record is already in the table, set `entry` to point to it.
 * Otherwise, set `entry` to be NULL. */
static void get_hash(char *name, uint32_t *slot_no, label_s **entry)
{
    // Compute hash code
    size_t len = strlen(name);
    uint32_t hi = len;
    for (size_t i = 0; i < len; ++i)
        hi = ((hi * HASHFACTOR) + (uint32_t)(name[i]));

    hi &= (1 << HASHBITS) - 1;
    hi %= LABEL_HASH_TABLE_SIZE;

    *slot_no = hi;
    // Search table for entry
    for (label_s *lab = label_hash_table[hi]; lab; lab = lab->next) {
        if (streq(lab->name, name)) {
            *entry = lab;               // <-- return if found
            return;
        }
    }
    *entry = NULL;
}


/* ========================================================================== *
 * Management
 * ========================================================================== */

/* Remove all local (non-global) label from the table. */
void flush_local_labels(void)
{
    // Iterate through current local labels
    // - No need to free or modify list since immexpr_s still reference it
    for (label_s *l = local_labels; l != NULL; l = l->next_local) {
        // This file's global labels are not flushed
        if (l->global_flag) {
            if (!symbol_is_defined(l)) {
                SYM_ERROR("VRV: (symbol) label \"%s\" marked as global in this file but left undefined\n",
                    l->name);
                return;
            }
            continue;
        }

        uint32_t hi;
        label_s *entry;
        get_hash(l->name, &hi, &entry);

        // Search for entry in hash-table bucket's chain to remove it
        label_s *prev = NULL;
        for (label_s *curr = label_hash_table[hi]; curr != NULL; curr = curr->next) {
            if (curr == entry) {
                // Remove entry from bucket
                if (prev == NULL)
                    label_hash_table[hi] = curr->next;
                else
                    prev->next = curr->next;
                // Add to retired table
                curr->next = retired_label_hash_table[hi];
                retired_label_hash_table[hi] = curr;
                // Entry found, so continue to next local label
                break;
            }
            prev = curr;
        }
    }

    // Reset local labels list
    local_labels = NULL;
}

/* Copy just the defining attributes of the label (`addr` and flags) */
static void copy_label_definition(label_s *dest, label_s *src)
{
    dest->addr = src->addr;
    dest->global_flag = src->global_flag;
    dest->gp_flag = src->gp_flag;
    dest->const_flag = src->const_flag;
}

static label_s *make_label(char *name, uint32_t hash_index)
{
    label_s *l = (label_s *)xmallocz(sizeof(label_s));
    l->name = xstrdup(name);

    // Use previously calculcated hash index to append label to chain
    l->next = label_hash_table[hash_index];
    label_hash_table[hash_index] = l;

    // Add to local label chain
    l->next_local = local_labels;
    local_labels = l;

    return l;
}


/* ========================================================================== *
 * Lookup
 * ========================================================================== */

/* Return the address of `symbol` or 0 if it is undefined. */
memaddr_t find_symbol_address(char *name)
{
    label_s *l = label_is_defined(name);
    if (l == NULL)
        return 0;

    return l->addr + (l->gp_flag ? SMALL_DATA_MID : 0);
}

/* Lookup label with `name`.  Either return its symbol table entry or NULL if it
 * is not in the table or its symbol is not currently defined. */
label_s *label_is_defined(char *name)
{
    uint32_t hi;
    label_s *entry;
    get_hash(name, &hi, &entry);

    return entry != NULL && symbol_is_defined(entry) ? entry : NULL;
}

/* Store a label with a given `name` in the given `label`.
 * Return true if an label with that name already exists.
 * Otherwise, creat the label and return false. */
bool lookup_or_make_label(char *name, label_s **label)
{
    uint32_t hi;
    get_hash(name, &hi, label);

    // If label found, return true
    if (*label != NULL)
        return true;

    // Make label and return false to signal label did not exist before
    *label = make_label(name, hi);
    return false;
}


/* ========================================================================== *
 * Recording
 * ========================================================================== */

/* Make the label named `name` global. Return its symbol. */
label_s *make_label_global(char *name)
{
    // Find label in current table
    uint32_t hi;
    label_s *l;
    get_hash(name, &hi, &l);

    // If exists and already global, exit early with an error
    // - This is necessary to program consistency, as we expect a label to
    //   only be defined in the same file in which it is marked as global
    if (l && l->global_flag) {
        SYM_ERROR("VRV: (symbol) Label \"%s\" is already marked as global\n", name);
        return l;
    }

    // Make an active label if it does already exist
    if (!l)
        l = make_label(name, hi);
    // Set global flag for active label
    l->global_flag = true;

    // If active label is undefine, we will need to move retired `label_uses` over to it
    bool l_defined = symbol_is_defined(l);

    // Check retired table for undefined symbols with the same label from other program files
    label_s *retired_prev = NULL, *retired = retired_label_hash_table[hi];
    while (retired) {
        if (streq(name, retired->name)) {
            // Match found
            if (symbol_is_defined(retired)) {
                SYM_ERROR(
                    "VRV: (symbol) Label \"%s\" cannot be made global, as another file defines a local symbol with the same label\n",
                    name);
                return l;
            }
            if (l_defined) {
                // Active label defined, so resolve retired label
                copy_label_definition(retired, l);
                resolve_label_uses(retired);
            } else {
                // Active label undefined, so move retired uses to active label
                for (label_use_s *u = retired->uses, *next; u != NULL; u = next) {
                    next = u->next;
                    u->next = l->uses;
                    l->uses = u;
                    if (u->inst)
                        u->inst->expr->symbol = l;
                    else
                        u->data_expr->symbol = l;
                }
            }
            // Remove this entry from the retired table
            label_s *retired_next = retired->next;
            free(retired);
            if (retired_prev)
                retired_prev->next = retired_next;
            else
                retired_label_hash_table[hi] = retired_next;
            retired = retired_next;
        } else {
            retired_prev = retired;
            retired = retired->next;
        }
    }

    // Return active label
    return l;
}

/* Record that a memory `addr` uses the as-yet undefined `sym`. */
void record_data_uses_symbol(immexpr_s *expr, memaddr_t addr, label_s *sym)
{
    label_use_s *u = (label_use_s *)xmallocz(sizeof(label_use_s));

    u->data_expr = copy_immexpr(expr);
    u->addr = addr;
    u->next = sym->uses;
    sym->uses = u;
}

/* Record that an `inst` uses the as-yet undefined `sym`. */
void record_inst_uses_symbol(inst_s *inst, memaddr_t inst_addr, label_s *sym)
{
    label_use_s *u = (label_use_s *)xmallocz(sizeof(label_use_s));

    // If `inst` defined outside of text segment, `inst` itself will be
    //   freed so we want to copy it into the `label_use_s` struct.
    if (!is_text_seg(segment_index_from_addr(inst_addr, BP_WORD))) {  // <-- @TODO change for C-ext
        u->inst = copy_inst(inst);
        u->free_inst = true;
    } else {
        u->inst = inst;
        u->free_inst = false;
    }
    u->addr = inst_addr;
    u->next = sym->uses;
    sym->uses = u;
}

/* Record that the label named `name` refers to `val`.
 * If `resolve_uses` is true, resolve all references to it.
 * Return the label structure. */
label_s *record_const_label(char *name, memaddr_t val, bool resolve_uses)
{
    label_s *l;
    if (lookup_or_make_label(name, &l) && symbol_is_defined(l)) {
        SYM_ERROR("VRV: (symbol) Label \"%s\" is defined for second time\n", name);
        return l;
    }
    // Set label as constant and directly store its value in `addr`
    l->const_flag = true;
    l->addr = val;

    // Resolve the label, if requested
    if (resolve_uses) {
        resolve_label_uses(l);
    }

    return l;
}

/* Record that the label named `name` refers to `addr`.
 * If `resolve_uses` is true, resolve all references to it.
 * Return the label structure. */
label_s *record_label(char *name, memaddr_t addr, bool resolve_uses)
{
    label_s *l;
    if (lookup_or_make_label(name, &l) && symbol_is_defined(l)) {
        SYM_ERROR("VRV: (symbol) Label \"%s\" is defined for second time\n", name);
        return l;
    }

    // Record label address
    l->addr = addr;

    // If `memloader` used the small data segment for this label,
    seg_idx_e seg_i = segment_index_from_addr(addr, BP_BYTE);
    if (seg_i == SMALL_DATA_SEG)
        l->gp_flag = true;

    // Resolve the labe, if requested
    if (resolve_uses) {
        resolve_label_uses(l);
    }

    return l;
}


/* ========================================================================== *
 * Resolving
 * ========================================================================== */

void data_resolve_label(label_s *sym, immexpr_s *expr, memaddr_t addr)
{
    if (expr == NULL) {
        SYM_ERROR("VRV: (symbol) Resolving symbol \"%s\" in invalid data expression\n",
            sym->name);
        return;
    }
    if (!symbol_is_defined(sym)) {
        SYM_ERROR("VRV: (symbol) Resolving undefined symbol: %s\n", sym->name);
        return;
    }

    // Evaluate expression with newly resolved symbol and write to address
    write_word(addr, eval_immexpr(expr, addr, false), EMU_ACCESS);
    return;
}

void inst_resolve_label(label_s *sym, inst_s *inst)
{
    // Determine immediate value for particular instruction,
    //   re-encode instruction, and rewrite in memory
    if (inst->expr == NULL || inst->expr->symbol == NULL) {
        char *inst_str = inst_to_string(inst);
        SYM_ERROR("VRV: (symbol) Resolving symbol \"%s\" in immediate expression with no symbol:\n--> %s\n",
            sym->name, inst_str);
        free(inst_str);
        return;
    }
    if (!symbol_is_defined(sym)) {
        SYM_ERROR("VRV: (symbol) Resolving undefined symbol: %s\n", sym->name);
        return;
    }

    // Check if address base register should be set to `gp`
    if (sym->gp_flag && inst->expr->base_reg == 0) {
        // Base register always stored in `rs1`
        inst->expr->base_reg = REG_GP;
        inst->rs1 = REG_GP;
    }

    // Validate that resulting value fits in `inst` immediate field
    if (!validate_immexpr(inst->expr, inst->addr)) {
        sstream_s ss;
        ss_init(&ss);
        ss_printf(&ss, "VRV: (symbol) Resolved symbol value (%s: 0x%08x) does not fit into immediate field:\n--> ",
            sym->name, sym->addr);
        format_an_inst(&ss, inst, inst->addr);
        char *str = ss_to_string(&ss);
        SYM_ERROR("%s\n", str);
        free(str);
    }

    // Place final masked value in instruction's immediate field
    inst->imm = eval_immexpr(inst->expr, inst->addr, true);

    // Re-encode instruction and write it to memory
    inst->encoding = inst_encode(inst);
    write_word(inst->addr, inst->encoding, EMU_ACCESS);
}

/* Given a newly-defined `label`, resolve the previously encountered
 * instructions and data locations that refer to the label. */
void resolve_label_uses(label_s *sym)
{
    label_use_s *next_use;
    for (label_use_s *use = sym->uses; use != NULL; use = next_use) {
        if (use->inst == NULL)
            data_resolve_label(sym, use->data_expr, use->addr);
        else
            inst_resolve_label(sym, use->inst);
        next_use = use->next;
        if (use->free_inst)
            free(use->inst);
        if (use->data_expr)
            free(use->data_expr);
        free(use);
    }
    sym->uses = NULL;
}


/* ========================================================================== *
 * Printing
 * ========================================================================== */

/* Print all symbols in the table. */
void print_symbols(bool print_retired)
{
    sstream_s ss;
    ss_init(&ss);

    ss_printf(&ss, "--- Global Symbols ---\n");
    for (int i = 0; i < LABEL_HASH_TABLE_SIZE; i++) {
        for (label_s *l = label_hash_table[i]; l != NULL; l = l->next) {
            ss_printf(&ss, "g%c  %s: 0x%08x\n",
                l->const_flag ? 'c' : ' ', l->name, l->addr);
        }
    }

    if (print_retired) {
        ss_printf(&ss, "--- Retired Local Symbols ---\n");
        for (int i = 0; i < LABEL_HASH_TABLE_SIZE; i++) {
            for (label_s *l = retired_label_hash_table[i]; l != NULL; l = l->next) {
                ss_printf(&ss, " %c  %s: 0x%08x\n",
                    l->const_flag ? 'c' : ' ', l->name, l->addr);
            }
        }
    }

    char *str = ss_to_string(&ss);
    fe->error("%s", str);
    free(str);
}

/* Return a string containing the names of all undefined symbols in the table,
 * separated by a newline character.  Return NULL if no symbols are undefined.
 */
char *undefined_symbol_string(void)
{
    sstream_s ss;
    ss_init(&ss);

    for (int i = 0; i < LABEL_HASH_TABLE_SIZE; i++) {
        for (label_s *l = label_hash_table[i]; l != NULL; l = l->next) {
            if (!symbol_is_defined(l)) {
                ss_printf(&ss, "- %s\n", l->name);
            }
        }
    }
    for (int i = 0; i < LABEL_HASH_TABLE_SIZE; i++) {
        for (label_s *l = retired_label_hash_table[i]; l != NULL; l = l->next) {
            if (!symbol_is_defined(l)) {
                ss_printf(&ss, "- %s\n", l->name);
            }
        }
    }

    char *str = ss_to_string(&ss);
    if (!strlen(str)) {
        free(str);  // Necessary because this is the canonical way to clear `ss` buffer
        return NULL;
    }
    return str;
}
