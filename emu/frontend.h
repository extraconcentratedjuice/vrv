/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef INTERFACE_IO_H
#define INTERFACE_IO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>

#include "param.h"


/* IO functions to be implemented individually by each specific frontend
 *   interface that uses the VRV emulation backend.
 * Interfaces  should fill in function definitions into the `io` struct
 *   before calling any VRV funtions.
 * Default stdio implementations provided already. */

typedef struct fe_api {
    /* Returns true if input is available for MMIO.
     * Used to check when to signal IO interupts. */
    bool (*console_input_available)(void);

    /* Read a character */
    char (*get_console_char)(void);

    /* Write a character to the console */
    void (*put_console_char)(char c);

    /* Read input until `n` characters or newline (same semantics as `fgets`)
     * - Returns true if a line of input was read into the buffer
     *     or if the buffer was filled.
     * - This is to support non-blocking implementations.
     * - Blocking implementations should always return true.
     * - When returning false, causes the execution to return
     *     `RUN_WAIT_FOR_INPUT` result which defers the wait to the frontend. */
    bool (*read_input)(char *str, int n);

    /* Write the output string (same semantics as `printf`) */
    void (*write_output)(const char *fmt, ...);

    /* Conditonally write a logging string with a given verbosity level */
    void (*write_log)(int vlvl, const char *fmt, ...);

    /* Same semantics as `write_output` but used for errors */
    void (*error)(const char *fmt, ...);

    /* Error that occurs during program execution, should return control to frontend */
    void (*run_error)(const char *fmt, ...);

    /* Unrecoverable error, should abort emulator */
    void (*fatal_error)(const char *fmt, ...);

} fe_api_s;

extern fe_api_s *fe;


/*
 * Macros
 */

#define FE_PERROR(str)                                  \
do {                                                    \
    fe->write_output("%s: %s\n", str, strerror(errno)); \
} while (0)


/*
 * Exported functions
 */

void write_startup_message(void);

#ifdef __cplusplus
}
#endif

#endif  // INTERFACE_IO_H
