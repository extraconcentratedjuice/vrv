/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef SYSCALL_H
#define SYSCALL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>


/*
 * Syscalls
 */

/* Console print syscalls */
#define PRINT_DEC_SYSCALL        0
#define PRINT_HEX_SYSCALL        1
#define PRINT_FLOAT_SYSCALL      2
#define PRINT_CHAR_SYSCALL       3
#define PRINT_STRING_SYSCALL     4

/* Console read syscalls */
#define READ_DEC_SYSCALL         10
#define READ_HEX_SYSCALL         11
#define READ_FLOAT_SYSCALL       12
#define READ_CHAR_SYSCALL        13
#define READ_STRING_SYSCALL      14

/* Management syscalls */
#define EXIT_SYSCALL             20
#define SBRK_SYSCALL             21

/* File IO syscalls */
#define OPEN_SYSCALL             30
#define CLOSE_SYSCALL            31
#define READ_SYSCALL             32
#define WRITE_SYSCALL            33

/* All syscalls at least this large will be sent to the exception handler */
#define KERNEL_SYSCALL_BASE      100


/*
 * Syscall result
 */

typedef enum sysres {
    SYSRES_EXIT = 0,
    SYSRES_CONTINUE,
    SYSRES_WAIT_FOR_INPUT,
} sysres_e;


/*
 * Exported functions
 */

sysres_e do_syscall(void);

#ifdef __cplusplus
}
#endif

#endif  // SYSCALL_H
