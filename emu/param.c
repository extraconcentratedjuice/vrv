/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include "param.h"

struct configuration conf = {
    .timer_tick_ms          = 10,       /* 1 tick every 10ms */
    .stack_max_size         = 1 << 22,  /* 4MiB */
    .small_data_max_size    = 1,

    .io_interval            = 100,
    .mapped_tty             = false,
};

struct control ctl = {
    .verbose_lvl            = 1,
    .misaligned_exceptions  = MEMOP_LOAD | MEMOP_STORE,
    .break_on_exceptions    = 0,
};

struct context ctx = {
    .state                  = EMU_STATE,
    .program_addr           = 0,
    .current_access_level   = M_MODE_ACCESS,
    .vrv_exit_code          = 0,
    .exception_occurred     = false,
    .parse_error_occurred   = false,
};
