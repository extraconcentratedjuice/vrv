/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "frontend.h"
#include "utils.h"


/* Mangles the string so that the first period of its extension is nullified.
 * Returns the same pointer as was given. */
char *filepath_noext(char *path)
{
    int first_period = -1;
    int i = 0;
    for (; path[i]; ++i) {
        if (path[i] == '/')
            first_period = -1;
        else if (path[i] == '.' && first_period < 0)
            first_period = i;
    }

    if (first_period >= 0)
        path[first_period] = '\0';
    return path;
}

int strcmp_ci(const char *a, const char *b)
{
    while (1) {
        int diff = tolower(*a) - tolower(*b);
        if (diff != 0 || *a == '\0')
            return diff;
        ++a; ++b;
    }
}

/* Checks if `a` is a prefix of `b` with at least `min_match` characters. */
bool str_is_ciprefix(const char *a, const char *b, int min_match)
{
    while (*a && *b) {
        int diff = tolower(*a) - tolower(*b);
        if (diff != 0)
            return false;
        --min_match;
        ++a, ++b;
    }
    if (*a || min_match > 0)
        return false;
    return true;
}

char *xstrdup(const char *str)
{
    char *out = strdup(str);
    if (out == NULL)
        fe->fatal_error("Out of memory at request to duplicate \"%s\".\n", str);
    return out;
}

char *xstrdup_tolower(const char *str)
{
    int len = strlen(str) + 1;
    char *out = (char *)xmalloc(len);
    for (int i = 0; i < len; ++i) {
        out[i] = tolower(str[i]);
    }
    return out;
}

void *xmalloc(int size)
{
    void *x = malloc(size);
    if (x == NULL)
        fe->fatal_error("Out of memory at request for %d bytes.\n", size);
    return x;
}

/* Allocate a zero'ed block of storage. */
void *xmallocz(int size)
{
    void *z = xmalloc(size);
    memclr(z, size);
    return z;
}

void *xrealloc(void *src, size_t size)
{
    void *r = realloc(src, size);
    if (r == NULL)
        fe->fatal_error("Out of memory at request for %d bytes.\n", size);
    return r;
}

void *xreallocz(void *src, size_t size, size_t old_size)
{
    void *r = realloc(src, size);
    if (r == NULL)
        fe->fatal_error("Out of memory at request for %d bytes.\n", size);
    if (size > old_size)
        memclr(r + old_size, size - old_size);
    return r;
}
