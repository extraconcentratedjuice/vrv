/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/* Floating-point registers */
FPR_MAP("ft0",     0)
FPR_MAP("ft1",     1)
FPR_MAP("ft2",     2)
FPR_MAP("ft3",     3)
FPR_MAP("ft4",     4)
FPR_MAP("ft5",     5)
FPR_MAP("ft6",     6)
FPR_MAP("ft7",     7)
FPR_MAP("fs0",     8)
FPR_MAP("fs1",     9)
FPR_MAP("fa0",    10)
FPR_MAP("fa1",    11)
FPR_MAP("fa2",    12)
FPR_MAP("fa3",    13)
FPR_MAP("fa4",    14)
FPR_MAP("fa5",    15)
FPR_MAP("fa6",    16)
FPR_MAP("fa7",    17)
FPR_MAP("fs2",    18)
FPR_MAP("fs3",    19)
FPR_MAP("fs4",    20)
FPR_MAP("fs5",    21)
FPR_MAP("fs6",    22)
FPR_MAP("fs7",    23)
FPR_MAP("fs8",    24)
FPR_MAP("fs9",    25)
FPR_MAP("fs10",   26)
FPR_MAP("fs11",   27)
FPR_MAP("ft8",    28)
FPR_MAP("ft9",    29)
FPR_MAP("ft10",   30)
FPR_MAP("ft11",   31)
