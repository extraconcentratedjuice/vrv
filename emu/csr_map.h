/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/* The order of the macros below is:
 * CSR_MAP(name, index, number, mask, shift) */

/*
 * User Mode Read/Write
 */

/* Floating-Point CSRs */
CSR_MAP("fflags",       CSR_FCSR,       0x001,      CSR_FFLAGS_MASK,    0)
CSR_MAP("frm",          CSR_FCSR,       0x002,      CSR_FRM_MASK,       5)
CSR_MAP("fcsr",         CSR_FCSR,       0x003,      ~0u,                0)

/* User-Mode Counters and Timers */
CSR_MAP("cycle",        CSR_MCYCLE,     0xc00,      ~0u,                0)
CSR_MAP("instret",      CSR_MINSTRET,   0xc01,      ~0u,                0)
CSR_MAP("time",         CSR_MTIME,      0xc02,      ~0u,                0)
CSR_MAP("cycleh",       CSR_MCYCLEH,    0xc80,      ~0u,                0)
CSR_MAP("instreth",     CSR_MINSTRETH,  0xc81,      ~0u,                0)
CSR_MAP("timeh",        CSR_MTIMEH,     0xc82,      ~0u,                0)

/* Machine-Mode Trap Setup */
CSR_MAP("mstatus",      CSR_MSTATUS,    0x300,      CSR_MSTATUS_MASK,   0)
CSR_MAP("mie",          CSR_MIE,        0x304,      CSR_MIP_MASK,       0)
CSR_MAP("mtvec",        CSR_MTVEC,      0x305,      ~0u,                0)
CSR_MAP("mstatush",     CSR_MSTATUSH,   0x310,      0u,                 0)

/* Machine-Mode Trap Handling */
CSR_MAP("mscratch",     CSR_MSCRATCH,   0x340,      ~0u,                0)
CSR_MAP("mepc",         CSR_MEPC,       0x341,      ~3u,                0)
CSR_MAP("mcause",       CSR_MCAUSE,     0x342,      ~0u,                0)
CSR_MAP("mtval",        CSR_MTVAL,      0x343,      ~0u,                0)
CSR_MAP("mip",          CSR_MIP,        0x344,      CSR_MIP_MASK,       0)

/* Machine-Mode Counters */
CSR_MAP("mcycle",       CSR_MCYCLE,     0xb00,      ~0u,                0)
CSR_MAP("minstret",     CSR_MINSTRET,   0xb01,      ~0u,                0)
CSR_MAP("mcycleh",      CSR_MCYCLEH,    0xb80,      ~0u,                0)
CSR_MAP("minstreth",    CSR_MINSTRETH,  0xb81,      ~0u,                0)

/* Machine-Mode Memory-Mapped Timing Registers */
/*   These use encodings in a "Custom read/write" range as they have no defined
 *   encodings in the spec. */
CSR_MAP("mtime",        CSR_MTIME,      0xbc0,      ~0u,                0)
CSR_MAP("mtimecmp",     CSR_MTIMECMP,   0xbc1,      ~0u,                0)
CSR_MAP("mtimeh",       CSR_MTIMEH,     0xbe0,      ~0u,                0)
CSR_MAP("mtimecmph",    CSR_MTIMECMPH,  0xbe1,      ~0u,                0)
