/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef MEMACCESS_H
#define MEMACCESS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "memloader.h"
#include "memseg.h"
#include "run.h"


/*
 * Exported functions
 */

/* Exceptions */
void raise_mem_exception(excode_e ex, memaddr_t addr, int seg_i);

/* Lookup */
seg_idx_e segment_index_from_addr(memaddr_t addr, uint32_t num_bytes);

/* Reading */
uint8_t read_byte(memaddr_t addr, access_level_e al);
uint16_t read_half(memaddr_t addr, access_level_e al);
uint32_t read_word(memaddr_t addr, access_level_e al);

/* Writing */
void write_byte(memaddr_t addr, int8_t data, access_level_e al);
void write_half(memaddr_t addr, int16_t data, access_level_e al);
void write_word(memaddr_t addr, int32_t data, access_level_e al);

#ifdef __cplusplus
}
#endif

#endif  // MEMACCESS_H
