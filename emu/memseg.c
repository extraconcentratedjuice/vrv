/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <stdio.h>
#include <stdlib.h>

#include "frontend.h"
#include "memseg.h"
#include "utils.h"


/* ========================================================================== *
 * Initialization + Destruction
 * ========================================================================== */

void memseg_init(memseg_s *seg, bool grows_down, memaddr_t anchor_bound,
                 uint32_t size, uint8_t priv)
{
    seg->data = (int8_t *)xmallocz(size);
    seg->size = size;
    if ((seg->grows_down = grows_down)) {
        // Stack segments
        seg->lower_bound = anchor_bound - size;
        seg->upper_bound = anchor_bound;
    } else {
        // Text and data segments
        seg->lower_bound = anchor_bound;
        seg->upper_bound = anchor_bound + size;
    }
    seg->loc_counter = anchor_bound;
    seg->priv = priv;
}

void memseg_destroy(memseg_s *seg) {
    free(seg->data);
    // Clear all values in the segment
    memclr(seg, sizeof(memseg_s));
}


/* ========================================================================== *
 * Management
 * ========================================================================== */

memaddr_t memseg_align(memseg_s *seg, uint32_t byte_boundary)
{
    // Can't align downwards-growing segments as start of element is
    //   dependent on size.
    if (seg->grows_down)
        return seg->loc_counter;
    if (byte_boundary == 0 || byte_boundary - LSB(byte_boundary) != 0) {
        fe->error("Cannot align on a byte boundary that is not a power of two.\n");
        return seg->loc_counter;
    }

    seg->loc_counter = ALIGN_UP(seg->loc_counter, byte_boundary);
    return seg->loc_counter;
}

void memseg_clear(memseg_s *seg)
{
    free(seg->data);
    memclr(seg, sizeof(memseg_s));
}

/* Change the size (both allocation and bounds) of the given memory segment.
 * NOTE: This does not check if there is room in the address space to actually
 *       fit the expanded segment. Use `expand_memory` whenever possible. */
void memseg_set_size(memseg_s *seg, uint32_t new_size)
{
    if (!seg->grows_down) {
        seg->data = (int8_t *)xreallocz(seg->data, new_size, seg->size);
        seg->upper_bound = seg->lower_bound + new_size;

    } else {
        // Expand stack downards.
        //   Can't use `realloc` since it copies from bottom of memory blocks,
        //   so manually copy bytes to new array.
        int8_t *new_data = (int8_t *)xmalloc(new_size);
        int32_t byte_delta = new_size - seg->size;
        if (byte_delta > 0)
            memclr(new_data, byte_delta);
        memcpy(new_data + MAX(0, byte_delta),
               seg->data - MIN(0, byte_delta),
               seg->size + MIN(0, byte_delta));

        free(seg->data);
        seg->data = new_data;
        seg->lower_bound = seg->upper_bound - new_size;
    }

    seg->size = new_size;
}


/* -------------------------------------------------------------------------- *
 * VRV emulates a little-endian system. This is not explicitly enforced,
 *   so half and word sized reading and writing operations assume user's
 *   system is little-endian.
 * -------------------------------------------------------------------------- */
/* ========================================================================== *
 * Reading
 * ========================================================================== */

bool memseg_read_byte(memseg_s *seg, memaddr_t addr, int8_t *data)
{
    if (!memseg_contains_range(seg, addr, BP_BYTE))
        return false;

    // Get relative address within segment
    addr -= seg->lower_bound;
    // Read byte from target address
    *data = seg->data[addr];

    return true;
}

bool memseg_read_half(memseg_s *seg, memaddr_t addr, int16_t *data)
{
    if (!memseg_contains_range(seg, addr, BP_HALF))
        return false;

    // Get relative address within segment
    addr -= seg->lower_bound;
    // Read halfword from target address, casting to directly read 2 bytes
    *data = *((uint16_t *)(seg->data + addr));

    return true;
}

bool memseg_read_word(memseg_s *seg, memaddr_t addr, int32_t *data)
{
    if (!memseg_contains_range(seg, addr, BP_WORD))
        return false;

    // Get relative address within segment
    addr -= seg->lower_bound;
    // Read word from target address, casting to directly read 4 bytes
    *data = *((uint32_t *)(seg->data + addr));

    return true;
}


/* ========================================================================== *
 * Writing
 * ========================================================================== */

memaddr_t memseg_append_byte(memseg_s *seg, int8_t data)
{
    memaddr_t loc = seg->loc_counter - (seg->grows_down ? BP_BYTE : 0);

    if (!memseg_write_byte(seg, loc, data))
        return 0;

    seg->loc_counter = seg->grows_down ? loc : loc + BP_BYTE;
    // Return address of appended byte
    return loc;
}

memaddr_t memseg_append_half(memseg_s *seg, int16_t data)
{
    memaddr_t loc = seg->loc_counter - (seg->grows_down ? BP_HALF : 0);

    if (!memseg_write_half(seg, loc, data))
        return 0;

    seg->loc_counter = seg->grows_down ? loc : loc + BP_HALF;
    // Return address of appended halfword
    return loc;
}

memaddr_t memseg_append_word(memseg_s *seg, int32_t data)
{
    memaddr_t loc = seg->loc_counter - (seg->grows_down ? BP_WORD : 0);

    if (!memseg_write_word(seg, loc, data))
        return 0;

    seg->loc_counter = seg->grows_down ? loc : loc + BP_WORD;
    // Return address of appended word
    return loc;
}

bool memseg_write_byte(memseg_s *seg, memaddr_t addr, int8_t data)
{
    if (!memseg_contains_range(seg, addr, BP_BYTE))
        return false;

    // Get relative address within segment
    addr -= seg->lower_bound;
    // Write byte to target address
    seg->data[addr] = data;

    seg->modified = true;
    return true;
}

bool memseg_write_half(memseg_s *seg, memaddr_t addr, int16_t data)
{
    if (!memseg_contains_range(seg, addr, BP_HALF))
        return false;

    // Get relative address within segment
    addr -= seg->lower_bound;
    // Write halfword to target address, casting to directly write 2 bytes
    *((uint16_t *)(seg->data + addr)) = data;

    seg->modified = true;
    return true;
}

bool memseg_write_word(memseg_s *seg, memaddr_t addr, int32_t data)
{
    if (!memseg_contains_range(seg, addr, BP_WORD))
        return false;

    // Get relative address within segment
    addr -= seg->lower_bound;
    // Write word to target address, casting to directly write 4 bytes
    *((uint32_t *)(seg->data + addr)) = data;

    seg->modified = true;
    return true;
}
