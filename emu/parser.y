/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

// %expect 25           /* Suppress warning about 25 shift-reduce conflicts */
%locations

%start LINE

%token Y_EOF

%token Y_COMMA
%token Y_NL
%token Y_INT
%token Y_LABEL
%token Y_ID
%token Y_ID_CONST
%token Y_REG
%token Y_FP_REG
%token Y_STR
%token Y_FP
%token Y_RM
%token Y_CSR
%token Y_MEM_FLAGS
%token Y_PC

/*
 * RV32 instructions op codes
 */
%token Y_ADD_OP
%token Y_ADDI_OP
%token Y_AND_OP
%token Y_ANDI_OP
%token Y_AUIPC_OP
%token Y_BEQ_OP
%token Y_BGE_OP
%token Y_BGEU_OP
%token Y_BLT_OP
%token Y_BLTU_OP
%token Y_BNE_OP
%token Y_CSRRW_OP
%token Y_CSRRS_OP
%token Y_CSRRC_OP
%token Y_CSRRWI_OP
%token Y_CSRRSI_OP
%token Y_CSRRCI_OP
%token Y_DIV_OP
%token Y_DIVU_OP
%token Y_EBREAK_OP
%token Y_ECALL_OP
%token Y_FADD_S_OP
%token Y_FCLASS_S_OP
%token Y_FCVT_S_W_OP
%token Y_FCVT_S_WU_OP
%token Y_FCVT_W_S_OP
%token Y_FCVT_WU_S_OP
%token Y_FDIV_S_OP
%token Y_FENCE_OP
%token Y_FENCEI_OP
%token Y_FENCETSO_OP
%token Y_FEQ_S_OP
%token Y_FLE_S_OP
%token Y_FLT_S_OP
%token Y_FLW_OP
%token Y_FMADD_S_OP
%token Y_FMAX_S_OP
%token Y_FMIN_S_OP
%token Y_FMSUB_S_OP
%token Y_FMUL_S_OP
%token Y_FMV_W_X_OP
%token Y_FMV_X_W_OP
%token Y_FNMADD_S_OP
%token Y_FNMSUB_S_OP
%token Y_FSGNJ_S_OP
%token Y_FSGNJN_S_OP
%token Y_FSGNJX_S_OP
%token Y_FSQRT_S_OP
%token Y_FSUB_S_OP
%token Y_FSW_OP
%token Y_JAL_OP
%token Y_JALR_OP
%token Y_LB_OP
%token Y_LBU_OP
%token Y_LH_OP
%token Y_LHU_OP
%token Y_LUI_OP
%token Y_LW_OP
%token Y_MRET_OP
%token Y_MUL_OP
%token Y_MULH_OP
%token Y_MULHSU_OP
%token Y_MULHU_OP
%token Y_OR_OP
%token Y_ORI_OP
%token Y_PAUSE_OP
%token Y_REM_OP
%token Y_REMU_OP
%token Y_SB_OP
%token Y_SH_OP
%token Y_SLL_OP
%token Y_SLLI_OP
%token Y_SLT_OP
%token Y_SLTI_OP
%token Y_SLTIU_OP
%token Y_SLTU_OP
%token Y_SRA_OP
%token Y_SRAI_OP
%token Y_SRL_OP
%token Y_SRLI_OP
%token Y_SUB_OP
%token Y_SW_OP
%token Y_XOR_OP
%token Y_XORI_OP

/*
 * Assembler pseudo operations op codes
 */
%token Y_LLA_POP
%token Y_NOP_POP
%token Y_LI_POP
%token Y_MV_POP
%token Y_NOT_POP
%token Y_NEG_POP
%token Y_SEQZ_POP
%token Y_SNEZ_POP
%token Y_SLTZ_POP
%token Y_SGTZ_POP
%token Y_FMV_S_POP
%token Y_FABS_S_POP
%token Y_FNEG_S_POP
%token Y_BEQZ_POP
%token Y_BNEZ_POP
%token Y_BLEZ_POP
%token Y_BGEZ_POP
%token Y_BLTZ_POP
%token Y_BGTZ_POP
%token Y_BGT_POP
%token Y_BLE_POP
%token Y_BGTU_POP
%token Y_BLEU_POP
%token Y_J_POP
%token Y_JR_POP
%token Y_RET_POP
%token Y_CALL_POP
%token Y_TAIL_POP
%token Y_RDINSTRET_POP
%token Y_RDINSTRETH_POP
%token Y_RDCYCLE_POP
%token Y_RDCYCLEH_POP
%token Y_RDTIME_POP
%token Y_RDTIMEH_POP
%token Y_CSRR_POP
%token Y_CSRW_POP
%token Y_CSRS_POP
%token Y_CSRC_POP
%token Y_CSRWI_POP
%token Y_CSRSI_POP
%token Y_CSRCI_POP
%token Y_FRCSR_POP
%token Y_FSCSR_POP
%token Y_FRRM_POP
%token Y_FSRM_POP
%token Y_FRFLAGS_POP
%token Y_FSFLAGS_POP


/*
 * Assembler directives
 */
%token Y_EQU_DIR
%token Y_ALIGN_DIR
%token Y_GLOBL_DIR
%token Y_COMM_DIR
%token Y_SECTION_DIR
%token Y_TEXT_DIR
%token Y_DATA_DIR
%token Y_KTEXT_DIR
%token Y_KDATA_DIR
%token Y_STRING_DIR
%token Y_STRING_TERM_DIR
%token Y_BYTE_DIR
%token Y_2BYTE_DIR
%token Y_HALF_DIR
%token Y_SHORT_DIR
%token Y_4BYTE_DIR
%token Y_WORD_DIR
%token Y_LONG_DIR
%token Y_FLOAT_DIR
%token Y_ZERO_DIR


%{

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "csr.h"
#include "frontend.h"
#include "inst.h"
#include "memaccess.h"
#include "memloader.h"
#include "param.h"
#include "parser.h"
#include "reg.h"
#include "scanner.h"
#include "sstream.h"
#include "symbol.h"
#include "utils.h"


/*
 * Macros
 */

/* return (0) */
#define LINE_PARSE_DONE YYACCEPT

/* return (1) */
#define FILE_PARSE_DONE YYABORT

/*
 * Structs
 */

typedef struct label_list {
    char *name;
    struct label_list *next;
} label_list_s;


/*
 * Local functions
 */

/* Error Messaging */
static void error_at_token(char *str, int start, int end);
static void yywarn(char *);

/* Line Management */
static label_list_s *add_line_label(char *name);
static void clear_labels(void);

/* Instruction Storage */
static void i_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2, immexpr_s *expr);
static void r_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2);
static void r4_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2, reg_t rs3);

/* Data Storage */
static memaddr_t store_expr(int32_t value);
static memaddr_t store_string(char *str);
static memaddr_t store_word_data(int32_t value);

/* Instruction Classification */
static bool inst_is_load(opc_t opcode);

/* Pseudoinstruction Helpers */
static void expand_load_store(opc_t opcode, reg_t rd, reg_t rt, immexpr_s *expr);
static void split_addr(immexpr_s *src_to_hi, immexpr_s **lo);


/*
 * Local variables
 */

static int store_size; // Byte amount to store items in an EXPR_LST
static bool null_terminate;

static label_list_s *this_line_labels = NULL; // List of labels for curent line
static memaddr_t this_line_addr = 0;

static char *input_file_name; // Name of file being parsed

%}

%%

/* ========================================================================== *
 * Top-level Rules
 * ========================================================================== */

LINE:
        {
            ctx.parse_error_occurred = false;
            this_line_addr = 0;
            store_size = BP_WORD;
            scanner_start_line();
        }
        LINE_TEXT
    ;


LINE_TEXT:
        LINE_LBL LINE_TEXT
    |   CMD
    ;


LINE_LBL:
        Y_LABEL
        {
            if ($1.str) {
                add_line_label(xstrdup($1.str));
                free($1.str);
            }
        }
    ;


CMD:
        ASM_CODE  TERM_CLEAR
    |   PSEUDO_ASM_CODE  TERM_CLEAR
    |   ASM_DIRECTIVE  TERM_CLEAR
    |   ERRONEOUS_LINE
    |   TERM
    ;


/* A collection of top-level erroneous line patterns */
ERRONEOUS_LINE:
        /* Non-keyword strings with no tokens after,
         * probably an attempted label. */
        Y_ID  Y_NL
        {
            if ($1.str[0] == '.') {
                error_at_token("Unknown assembly directive",
                           @1.first_column, @1.last_column);
            } else {
                error_at_token("Expected ':' after label",
                           @1.first_column, @1.last_column);
            }
            /* Since we are consuming `Y_NL`, we must perform the same line
             *   cleanup steps as done in `TERM` */
            clear_labels();
            free($1.str);
            LINE_PARSE_DONE;
        }

        /* Non-keyword strings with some extra tokens afterwards,
         * potentially an attempted instruction or directive. */
    |   Y_ID  /* EMPTY */
        {
            if ($1.str[0] == '.') {
                error_at_token("Unknown assembly directive",
                           @1.first_column, @1.last_column);
            } else {
                error_at_token("Unknown instruction mnemonic",
                           @1.first_column, @1.last_column);
            }
            free($1.str);
        }
    ;


TERM:
        Y_NL
        {
            line_no += 1;
            LINE_PARSE_DONE;
        }

    |   Y_EOF
        {
            clear_labels();
            FILE_PARSE_DONE;
        }
    ;


TERM_CLEAR:
        Y_NL
        {
            clear_labels();
            line_no += 1;
            LINE_PARSE_DONE;
        }

    |   Y_EOF
        {
            clear_labels();
            FILE_PARSE_DONE;
        }
    ;


SEP:    /* empty */
    |   Y_COMMA
    ;


/* ========================================================================== *
 * Assembly Instructions
 * ========================================================================== */

ASM_CODE:
    // No Argument
        NOARG_OPS
        {
            r_type_inst($1.i, 0, 0, 0);
        }

    // M-Mode No Argument
    |   M_NOARG_OPS
        {
            if (!is_kernel_seg(current_segment_index()))
                yyerror("Privileged instruction used outside kernel segment");
            else
                r_type_inst($1.i, 0, 0, 0);
        }

    // R-Type Instructions
    |   R_OPS  DEST  SRC1  SRC2
        {
            r_type_inst($1.i, $2.i, $3.i, $4.i);
        }
    |   R_FP_OPS  F_DEST  F_SRC1  F_SRC2
        {
            r_type_inst($1.i, $2.i, $3.i, $4.i);
        }
    |   R_FP_RM_OPS  F_DEST  F_SRC1  F_SRC2  F_RM
        {
            r_type_inst($1.i, $2.i, $3.i, $4.i);
        }
    |   R_F2X_OPS  DEST  F_SRC1  F_SRC2
        {
            r_type_inst($1.i, $2.i, $3.i, $4.i);
        }
    |   R2_FP_RM_OPS  F_DEST  F_SRC1  F_RM
        {
            r_type_inst($1.i, $2.i, $3.i, 0);
        }
    |   R4_FP_RM_OPS  F_DEST  F_SRC1  F_SRC2  F_SRC3  F_RM
        {
            r4_type_inst($1.i, $2.i, $3.i, $4.i, $5.i);
        }
    |   R2_F2X_OPS  DEST  F_SRC1
        {
            r_type_inst($1.i, $2.i, $3.i, 0);
        }
    |   R2_F2X_RM_OPS  DEST  F_SRC1  F_RM
        {
            r_type_inst($1.i, $2.i, $3.i, 0);
        }
    |   R2_X2F_OPS  F_DEST  SRC1
        {
            r_type_inst($1.i, $2.i, $3.i, 0);
        }
    |   R2_X2F_RM_OPS  F_DEST  SRC1  F_RM
        {
            r_type_inst($1.i, $2.i, $3.i, 0);
        }

    // I-Type Instructions
    |   I_OPS  DEST  SRC1  IMM12
        {
            i_type_inst($1.i, $2.i, $3.i, 0, $4.expr);
        }
    |   I_LOAD_OPS  DEST  ADDR_OPR
        {
            if ($3.expr->symbol && $3.expr->base_reg == 0) {
                // Pseudoinstruction Symbol-Expanding Load
                expand_load_store($1.i, $2.i, $2.i, $3.expr);
            } else {
                // Direct load instruction
                $3.expr->width = 12;
                if (!validate_immexpr($3.expr, loaderloc()))
                    error_at_token(
                        "Expression value does not fit into immediate field",
                        @3.first_column, @3.last_column);
                else
                    i_type_inst($1.i, $2.i, $3.expr->base_reg, 0, $3.expr);
            }
        }
    |   I_FP_LOAD_OPS  F_DEST  ADDR12
        {
            i_type_inst($1.i, $2.i, $3.expr->base_reg, 0, $3.expr);
        }
    |   I_FP_LOAD_OPS  F_DEST  SYM_OPR  TEMP
        {
            // Pseudoinstruction Symbol-Expanding Load
            expand_load_store($1.i, $2.i, $4.i, $3.expr);
        }
    |   I_SHIFT_OPS  DEST  SRC1  UIMM5
        {
            i_type_inst($1.i, $2.i, $3.i, 0, $4.expr);
        }
    |   I_JALR_OPS  DEST  ADDR12_PCREL
        {
            i_type_inst($1.i, $2.i, $3.expr->base_reg, 0, $3.expr);
        }
    |   I_JALR_OPS  SRC1
        {
            // `jalr` Pseudoinstruction Alias
            immexpr_s *expr = make_immexpr(0, NULL);
            expr->base_reg = $2.i;
            i_type_inst($1.i, REG_RA, expr->base_reg, 0, expr);
        }
    |   I_FENCE_OPS  MEM_FLAGS  MEM_FLAGS
        {
            immexpr_s *mem_expr = make_immexpr(($2.i << 4) | $3.i, NULL);
            mem_expr->width = 8;
            mem_expr->uimm = true;
            i_type_inst($1.i, 0, 0, 0, mem_expr);
        }
    |   Y_FENCE_OP
        {
            // Pseudoinstruction Alias
            immexpr_s *mem_expr = make_immexpr(0xff, NULL);
            i_type_inst($1.i, 0, 0, 0, mem_expr);
        }
    |   I_CSR_OPS  DEST  CSR_OPR  SRC1
        {
            // @TODO: give warning when writing to Read-Only CSRs
            r_type_inst($1.i, $2.i, $4.i, $3.i);
        }
    |   I_CSRI_OPS  DEST  CSR_OPR  UIMM5
        {
            i_type_inst($1.i, $2.i, 0, $3.i, $4.expr);
        }

    // S-Type Instructions
    |   S_OPS  SRC2  ADDR12
        {
            i_type_inst($1.i, 0, $3.expr->base_reg, $2.i, $3.expr);
        }
    |   S_OPS  SRC2  SYM_OPR  TEMP
        {
            // Pseudoinstruction Symbol-Expanding Store
            expand_load_store($1.i, $2.i, $4.i, $3.expr);
        }
    |   S_FP_OPS  F_SRC2  ADDR12
        {
            i_type_inst($1.i, 0, $3.expr->base_reg, $2.i, $3.expr);
        }
    |   S_FP_OPS  F_SRC2  SYM_OPR  TEMP
        {
            // Pseudoinstruction Address-Expanding Store
            expand_load_store($1.i, $2.i, $4.i, $3.expr);
        }

    // B-Type Instructions
    |   B_OPS  SRC1  SRC2  IMM13_PCREL
        {
            i_type_inst($1.i, 0, $2.i, $3.i, $4.expr);
        }

    // U-Type Instructions
    |   U_OPS  DEST  IMM20
        {
            i_type_inst($1.i, $2.i, 0, 0, $3.expr);
        }
    |   U_PCREL_OPS  DEST  IMM20_PCREL
        {
            i_type_inst($1.i, $2.i, 0, 0, $3.expr);
        }

    // J-Type Instructions
    |   J_OPS  DEST  IMM21_PCREL
        {
            i_type_inst($1.i, $2.i, 0, 0, $3.expr);
        }
    |   J_OPS  IMM21_PCREL
        {
            // `jal` Pseudoinstruction Alias
            i_type_inst($1.i, REG_RA, 0, 0, $2.expr);
        }
    ;


/* -------------------------------------------------------------------------- *
 * RV32 Instructions by Type
 * -------------------------------------------------------------------------- */

NOARG_OPS:
        Y_FENCETSO_OP
    |   Y_PAUSE_OP
    |   Y_ECALL_OP
    |   Y_EBREAK_OP
    |   Y_FENCEI_OP
    ;

M_NOARG_OPS:
        Y_MRET_OP
    ;

R_OPS:
        Y_ADD_OP
    |   Y_SUB_OP
    |   Y_SLL_OP
    |   Y_SLT_OP
    |   Y_SLTU_OP
    |   Y_XOR_OP
    |   Y_SRL_OP
    |   Y_SRA_OP
    |   Y_OR_OP
    |   Y_AND_OP
    |   Y_MUL_OP
    |   Y_MULH_OP
    |   Y_MULHSU_OP
    |   Y_MULHU_OP
    |   Y_DIV_OP
    |   Y_DIVU_OP
    |   Y_REM_OP
    |   Y_REMU_OP
    ;

R_FP_OPS:
        Y_FSGNJ_S_OP
    |   Y_FSGNJN_S_OP
    |   Y_FSGNJX_S_OP
    |   Y_FMIN_S_OP
    |   Y_FMAX_S_OP
    ;

R_FP_RM_OPS:
        Y_FADD_S_OP
    |   Y_FSUB_S_OP
    |   Y_FMUL_S_OP
    |   Y_FDIV_S_OP
    ;

R_F2X_OPS:
        Y_FEQ_S_OP
    |   Y_FLT_S_OP
    |   Y_FLE_S_OP
    ;

R2_FP_RM_OPS:
        Y_FSQRT_S_OP
    ;

R4_FP_RM_OPS:
        Y_FMADD_S_OP
    |   Y_FMSUB_S_OP
    |   Y_FNMSUB_S_OP
    |   Y_FNMADD_S_OP
    ;

R2_F2X_OPS:
        Y_FMV_X_W_OP
    |   Y_FCLASS_S_OP
    ;

R2_F2X_RM_OPS:
        Y_FCVT_W_S_OP
    |   Y_FCVT_WU_S_OP
    ;

R2_X2F_OPS:
        Y_FMV_W_X_OP
    ;

R2_X2F_RM_OPS:
        Y_FCVT_S_W_OP
    |   Y_FCVT_S_WU_OP
    ;

I_OPS:
        Y_ADDI_OP
    |   Y_SLTI_OP
    |   Y_SLTIU_OP
    |   Y_XORI_OP
    |   Y_ORI_OP
    |   Y_ANDI_OP
    ;

I_LOAD_OPS:
        Y_LB_OP
    |   Y_LH_OP
    |   Y_LW_OP
    |   Y_LBU_OP
    |   Y_LHU_OP
    ;

I_FP_LOAD_OPS:
        Y_FLW_OP
    ;

I_SHIFT_OPS:
        Y_SLLI_OP
    |   Y_SRLI_OP
    |   Y_SRAI_OP
    ;

I_JALR_OPS:
        Y_JALR_OP
    ;

I_FENCE_OPS:
        Y_FENCE_OP
    ;

I_CSR_OPS:
        Y_CSRRW_OP
    |   Y_CSRRS_OP
    |   Y_CSRRC_OP
    ;

I_CSRI_OPS:
        Y_CSRRWI_OP
    |   Y_CSRRSI_OP
    |   Y_CSRRCI_OP
    ;

S_OPS:
        Y_SB_OP
    |   Y_SH_OP
    |   Y_SW_OP
    ;

S_FP_OPS:
        Y_FSW_OP
    ;

B_OPS:
        Y_BEQ_OP
    |   Y_BNE_OP
    |   Y_BLT_OP
    |   Y_BGE_OP
    |   Y_BLTU_OP
    |   Y_BGEU_OP
    ;

U_OPS:
        Y_LUI_OP
    ;

U_PCREL_OPS:
        Y_AUIPC_OP
    ;

J_OPS:
        Y_JAL_OP
    ;

/* Used as catch-all for error handling */
ANY_OP:
        NOARG_OPS
    |   R_OPS
    |   R_FP_OPS
    |   R_FP_RM_OPS
    |   R_F2X_OPS
    |   R2_FP_RM_OPS
    |   R4_FP_RM_OPS
    |   R2_F2X_OPS
    |   R2_F2X_RM_OPS
    |   R2_X2F_OPS
    |   R2_X2F_RM_OPS
    |   I_OPS
    |   I_LOAD_OPS
    |   I_FP_LOAD_OPS
    |   I_SHIFT_OPS
    |   I_FENCE_OPS
    |   I_CSR_OPS
    |   I_CSRI_OPS
    |   S_OPS
    |   S_FP_OPS
    |   B_OPS
    |   U_OPS
    |   J_OPS
    ;

/* ========================================================================== *
 * Assembly Pseudoinstructions
 * ========================================================================== */

PSEUDO_ASM_CODE:
        Y_LLA_POP  DEST  SYM_OPR
        {
            immexpr_s *hi = $3.expr, *lo;
            split_addr(hi, &lo);
            i_type_inst(Y_AUIPC_OP, $2.i, 0, 0, hi);
            i_type_inst(Y_ADDI_OP, $2.i, $2.i, 0, lo);
        }

    |   Y_NOP_POP
        {
            immexpr_s *expr = make_immexpr(0, NULL);
            i_type_inst(Y_ADDI_OP, 0, 0, 0, expr);
        }

    |   Y_LI_POP  DEST  IMM_OPR
        {
            // `li` will use either `lui` or `addi` or both to construct imm
            bool undef_sym = $3.expr->symbol != NULL && !symbol_is_defined($3.expr->symbol);
            imm_t val = undef_sym ? 0 : eval_immexpr($3.expr, 0, false);

            // Use `lui` if unknown value or if there is information in the top 21 bits
            // - Need to check top 21 bits because may need to clear high 20 bits if
            //     `addi` ends up sign-extending the 12-bit value
            // - Don't use `lui` if all the top 21 bits are set, as this can be signed 12-bit
            bool has_high_info = (val & IMM_MIN(12)) && (val & IMM_MIN(12)) != IMM_MIN(12);
            bool use_high_part = undef_sym || has_high_info;
            if (use_high_part) {
                immexpr_s *hi_expr = copy_immexpr($3.expr);
                hi_expr->width = 20;
                hi_expr->hi_part = true;
                i_type_inst(Y_LUI_OP, $2.i, 0, 0, hi_expr);
            }

            // Use `addi` if unknown value, if `lui` skipped, or if any low 12 bits set
            if (undef_sym || !use_high_part || (val & UIMM_MAX(12))) {
                immexpr_s *lo_expr = copy_immexpr($3.expr);
                lo_expr->width = 12;
                lo_expr->lo_part = true;
                i_type_inst(Y_ADDI_OP, $2.i, (use_high_part ? $2.i : 0), 0, lo_expr);
            }

            // Free original expression
            free($3.expr);
        }

    |   Y_MV_POP  DEST  SRC1
        {
            immexpr_s *expr = make_immexpr(0, NULL);
            i_type_inst(Y_ADDI_OP, $2.i, $3.i, 0, expr);
        }

    |   Y_NOT_POP  DEST  SRC1
        {
            immexpr_s *expr = make_immexpr(-1, NULL);
            i_type_inst(Y_XORI_OP, $2.i, $3.i, 0, expr);
        }
    |   Y_NEG_POP  DEST  SRC1
        {
            r_type_inst(Y_SUB_OP, $2.i, 0, $3.i);
        }

    |   Y_SEQZ_POP  DEST  SRC1
        {
            immexpr_s *expr = make_immexpr(1, NULL);
            i_type_inst(Y_SLTIU_OP, $2.i, $3.i, 0, expr);
        }

    |   Y_SNEZ_POP  DEST  SRC1
        {
            r_type_inst(Y_SLTU_OP, $2.i, 0, $3.i);
        }

    |   Y_SLTZ_POP  DEST  SRC1
        {
            r_type_inst(Y_SLT_OP, $2.i, $3.i, 0);
        }

    |   Y_SGTZ_POP  DEST  SRC1
        {
            r_type_inst(Y_SLT_OP, $2.i, 0, $3.i);
        }

    |   Y_FMV_S_POP  F_DEST  F_SRC1
        {
            r_type_inst(Y_FSGNJ_S_OP, $2.i, $3.i, $3.i);
        }

    |   Y_FABS_S_POP  F_DEST  F_SRC1
        {
            r_type_inst(Y_FSGNJX_S_OP, $2.i, $3.i, $3.i);
        }

    |   Y_FNEG_S_POP  F_DEST  F_SRC1
        {
            r_type_inst(Y_FSGNJN_S_OP, $2.i, $3.i, $3.i);
        }

    |   Y_BEQZ_POP  SRC1  IMM13_PCREL
        {
            i_type_inst(Y_BEQ_OP, 0, $2.i, 0, $3.expr);
        }

    |   Y_BNEZ_POP  SRC1  IMM13_PCREL
        {
            i_type_inst(Y_BNE_OP, 0, $2.i, 0, $3.expr);
        }

    |   Y_BLEZ_POP  SRC1  IMM13_PCREL
        {
            i_type_inst(Y_BGE_OP, 0, 0, $2.i, $3.expr);
        }

    |   Y_BGEZ_POP  SRC1  IMM13_PCREL
        {
            i_type_inst(Y_BGE_OP, 0, $2.i, 0, $3.expr);
        }

    |   Y_BLTZ_POP  SRC1  IMM13_PCREL
        {
            i_type_inst(Y_BLT_OP, 0, $2.i, 0, $3.expr);
        }

    |   Y_BGTZ_POP  SRC1  IMM13_PCREL
        {
            i_type_inst(Y_BLT_OP, 0, 0, $2.i, $3.expr);
        }

    |   Y_BGT_POP  SRC1  SRC2  IMM13_PCREL
        {
            i_type_inst(Y_BLT_OP, 0, $3.i, $2.i, $4.expr);
        }

    |   Y_BLE_POP  SRC1  SRC2  IMM13_PCREL
        {
            i_type_inst(Y_BGE_OP, 0, $3.i, $2.i, $4.expr);
        }

    |   Y_BGTU_POP  SRC1  SRC2  IMM13_PCREL
        {
            i_type_inst(Y_BLTU_OP, 0, $3.i, $2.i, $4.expr);
        }

    |   Y_BLEU_POP  SRC1  SRC2  IMM13_PCREL
        {
            i_type_inst(Y_BGEU_OP, 0, $3.i, $2.i, $4.expr);
        }

    |   Y_J_POP  IMM21_PCREL
        {
            i_type_inst(Y_JAL_OP, 0, 0, 0, $2.expr);
        }

    |   Y_JR_POP  SRC1
        {
            immexpr_s *expr = make_immexpr(0, NULL);
            expr->base_reg = $2.i;
            i_type_inst(Y_JALR_OP, 0, expr->base_reg, 0, expr);
        }

    |   Y_RET_POP
        {
            immexpr_s *expr = make_immexpr(0, NULL);
            expr->base_reg = REG_RA;
            i_type_inst(Y_JALR_OP, 0, expr->base_reg, 0, expr);
        }

    |   Y_CALL_POP  IMM_OPR
        {
            immexpr_s *hi = $2.expr, *lo;
            split_addr(hi, &lo);
            lo->base_reg = REG_RA;
            i_type_inst(Y_AUIPC_OP, REG_RA, 0, 0, hi);
            i_type_inst(Y_JALR_OP, REG_RA, lo->base_reg, 0, lo);
        }

    |   Y_TAIL_POP  IMM_OPR
        {
            immexpr_s *hi = $2.expr, *lo;
            split_addr(hi, &lo);
            lo->base_reg = REG_TAIL;
            i_type_inst(Y_AUIPC_OP, REG_TAIL, 0, 0, hi);
            i_type_inst(Y_JALR_OP, 0, lo->base_reg, 0, lo);
        }

    |   Y_RDINSTRET_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_INSTRET_ENC);
        }

    |   Y_RDINSTRETH_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_INSTRETH_ENC);
        }

    |   Y_RDCYCLE_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_CYCLE_ENC);
        }

    |   Y_RDCYCLEH_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_CYCLEH_ENC);
        }

    |   Y_RDTIME_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_TIME_ENC);
        }

    |   Y_RDTIMEH_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_TIMEH_ENC);
        }

    |   Y_CSRR_POP  DEST  CSR_OPR
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, $3.i);
        }

    |   Y_CSRW_POP  CSR_OPR  SRC1
        {
            r_type_inst(Y_CSRRW_OP, 0, $3.i, $2.i);
        }

    |   Y_CSRS_POP  CSR_OPR  SRC1
        {
            r_type_inst(Y_CSRRS_OP, 0, $3.i, $2.i);
        }

    |   Y_CSRC_POP  CSR_OPR  SRC1
        {
            r_type_inst(Y_CSRRC_OP, 0, $3.i, $2.i);
        }

    |   Y_CSRWI_POP  CSR_OPR  UIMM5
        {
            i_type_inst(Y_CSRRWI_OP, 0, 0, $2.i, $3.expr);
        }

    |   Y_CSRSI_POP  CSR_OPR  UIMM5
        {
            i_type_inst(Y_CSRRSI_OP, 0, 0, $2.i, $3.expr);
        }

    |   Y_CSRCI_POP  CSR_OPR  UIMM5
        {
            i_type_inst(Y_CSRRCI_OP, 0, 0, $2.i, $3.expr);
        }

    |   Y_FRCSR_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_FCSR_ENC);
        }

    |   Y_FSCSR_POP  DEST  SRC1
        {
            r_type_inst(Y_CSRRW_OP, $2.i, $3.i, CSR_FCSR_ENC);
        }

    |   Y_FSCSR_POP  SRC1
        {
            r_type_inst(Y_CSRRW_OP, 0, $2.i, CSR_FCSR_ENC);
        }

    |   Y_FRRM_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_FRM_ENC);
        }

    |   Y_FSRM_POP  DEST  SRC1
        {
            r_type_inst(Y_CSRRW_OP, $2.i, $3.i, CSR_FRM_ENC);
        }

    |   Y_FSRM_POP  SRC1
        {
            r_type_inst(Y_CSRRW_OP, 0, $2.i, CSR_FRM_ENC);
        }

    |   Y_FRFLAGS_POP  DEST
        {
            r_type_inst(Y_CSRRS_OP, $2.i, 0, CSR_FFLAGS_ENC);
        }

    |   Y_FSFLAGS_POP  DEST  SRC1
        {
            r_type_inst(Y_CSRRW_OP, $2.i, $3.i, CSR_FFLAGS_ENC);
        }

    |   Y_FSFLAGS_POP  SRC1
        {
            r_type_inst(Y_CSRRW_OP, 0, $2.i, CSR_FFLAGS_ENC);
        }
    ;

ANY_POP:
        Y_LLA_POP
    |   Y_NOP_POP
    |   Y_LI_POP
    |   Y_MV_POP
    |   Y_NOT_POP
    |   Y_NEG_POP
    |   Y_SEQZ_POP
    |   Y_SNEZ_POP
    |   Y_SLTZ_POP
    |   Y_SGTZ_POP
    |   Y_FMV_S_POP
    |   Y_FABS_S_POP
    |   Y_FNEG_S_POP
    |   Y_BEQZ_POP
    |   Y_BNEZ_POP
    |   Y_BLEZ_POP
    |   Y_BGEZ_POP
    |   Y_BLTZ_POP
    |   Y_BGTZ_POP
    |   Y_BGT_POP
    |   Y_BLE_POP
    |   Y_BGTU_POP
    |   Y_BLEU_POP
    |   Y_J_POP
    |   Y_JR_POP
    |   Y_RET_POP
    |   Y_CALL_POP
    |   Y_TAIL_POP
    |   Y_RDINSTRET_POP
    |   Y_RDINSTRETH_POP
    |   Y_RDCYCLE_POP
    |   Y_RDCYCLEH_POP
    |   Y_RDTIME_POP
    |   Y_RDTIMEH_POP
    |   Y_CSRR_POP
    |   Y_CSRW_POP
    |   Y_CSRS_POP
    |   Y_CSRC_POP
    |   Y_CSRWI_POP
    |   Y_CSRSI_POP
    |   Y_CSRCI_POP
    |   Y_FRCSR_POP
    |   Y_FSCSR_POP
    |   Y_FRRM_POP
    |   Y_FSRM_POP
    |   Y_FRFLAGS_POP
    |   Y_FSFLAGS_POP
    ;


/* ========================================================================== *
 * Assembly Directives
 * ========================================================================== */

ASM_DIRECTIVE:
        Y_EQU_DIR  ID  SEP  EXPR
        {
            if ($2.str) {
                label_s *l;
                if ((l = label_is_defined($2.str))) {
                    error_at_token("Label is defined for second time",
                                   @2.first_column, @2.last_column);
                } else {
                    record_const_label($2.str, (memaddr_t)$4.i, 1);
                }
                free($2.str);
            }
        }

    |   Y_EQU_DIR  Y_ID_CONST  SEP  EXPR
        {
            error_at_token("Label is defined for second time",
                           @2.first_column, @2.last_column);
            free($2.const_id.str);
        }

    |   Y_ALIGN_DIR  Y_INT
        {
            loader_align(1U << $2.i);
        }

    |   Y_GLOBL_DIR  ID
        {
            if ($2.str) {
                make_label_global($2.str);
                free($2.str);
            }
        }

    |   Y_COMM_DIR  ID  SEP  EXPR
        {
            if ($4.i <= 0) {
                yyerror("Non-positive common object size");
            } else if ($2.str) {
                label_s *l;
                if ((l = label_is_defined($2.str))) {
                    error_at_token("Label is defined for second time",
                                   @2.first_column, @2.last_column);
                } else {
                    seg_idx_e old_seg_i;

                    if (   $4.i <= conf.small_data_max_size
                        && segloc(SMALL_DATA_SEG) + $4.i < SMALL_DATA_TOP) {
                        // Use small data segment
                        old_seg_i = set_segment(SMALL_DATA_SEG);
                    } else {
                        old_seg_i = set_segment(DATA_SEG);
                    }

                    // Store zeroes
                    memaddr_t addr = store_byte(0);
                    for (int i = 1; i < $4.i; ++i)
                        store_byte(0);
                    record_label($2.str, addr, true);

                    // Restore current segment
                    set_segment(old_seg_i);
                }
                free($2.str);
            }
        }
    |   Y_COMM_DIR  ID  SEP  ID_IMM
        {
            yyerror("Undefined symbol used in size argument");
        }

    |   Y_GLOBL_DIR  Y_ID_CONST
        {
            make_label_global($2.const_id.str);
            free($2.const_id.str);
        }

    |   Y_SECTION_DIR  Y_TEXT_DIR   { set_segment(TEXT_SEG); }
    |   Y_TEXT_DIR                  { set_segment(TEXT_SEG); }

    |   Y_SECTION_DIR  Y_DATA_DIR   { set_segment(DATA_SEG); }
    |   Y_DATA_DIR                  { set_segment(DATA_SEG); }

    |   Y_SECTION_DIR  Y_KTEXT_DIR  { set_segment(K_TEXT_SEG); }
    |   Y_KTEXT_DIR                 { set_segment(K_TEXT_SEG); }

    |   Y_SECTION_DIR  Y_KDATA_DIR  { set_segment(K_DATA_SEG); }
    |   Y_KDATA_DIR                 { set_segment(K_DATA_SEG); }

    /* `STR_LIST` token calls `store_string` as an action */
    |   Y_STRING_DIR  { null_terminate = false; }  STR_LIST
    |   Y_STRING_TERM_DIR  { null_terminate = true; }  STR_LIST

    |   Y_BYTE_DIR   { store_size = BP_BYTE; }  EXPR_LIST
    |   Y_2BYTE_DIR  { store_size = BP_HALF; }  EXPR_LIST
    |   Y_HALF_DIR   { store_size = BP_HALF; loader_align(BP_HALF); }  EXPR_LIST
    |   Y_SHORT_DIR  { store_size = BP_HALF; loader_align(BP_HALF); }  EXPR_LIST
    |   Y_4BYTE_DIR  { store_size = BP_WORD; }  EXPR_LIST
    |   Y_WORD_DIR   { store_size = BP_WORD; loader_align(BP_WORD); }  EXPR_LIST
    |   Y_LONG_DIR   { store_size = BP_WORD; loader_align(BP_WORD); }  EXPR_LIST
    |   Y_FLOAT_DIR  { store_size = BP_WORD; loader_align(BP_WORD); }  FP_EXPR_LIST

    |   Y_ZERO_DIR  EXPR
        {
            if ($2.i > 0) {
                // Assume this is for padding data, do not store in small data
                this_line_addr = store_byte(0);

                for (int i = 1; i < $2.i; ++i)
                    store_byte(0);
            }
        }
    |   Y_ZERO_DIR  ID_IMM
        {
            yyerror("Undefined symbol used in size argument");
        }
    ;

/* Used as catch-all for error handling */
ANY_DIRECTIVE:
        Y_EQU_DIR
    |   Y_ALIGN_DIR
    |   Y_GLOBL_DIR
    |   Y_COMM_DIR
    |   Y_SECTION_DIR
    |   Y_TEXT_DIR
    |   Y_DATA_DIR
    |   Y_KTEXT_DIR
    |   Y_KDATA_DIR
    |   Y_STRING_DIR
    |   Y_BYTE_DIR
    |   Y_2BYTE_DIR
    |   Y_HALF_DIR
    |   Y_SHORT_DIR
    |   Y_4BYTE_DIR
    |   Y_WORD_DIR
    |   Y_LONG_DIR
    |   Y_FLOAT_DIR
    |   Y_ZERO_DIR
    ;


/* ========================================================================== *
 * Expression Primitives
 * ========================================================================== */

INT:    Y_INT
    |   '-'  Y_INT
        { $$.i = - $2.i; }
    |   Y_ID_CONST
        {
            $$.i = $1.const_id.i;
            free($1.const_id.str);
        }
    |   '-'  Y_ID_CONST
        {
            $$.i = - $2.const_id.i;
            free($2.const_id.str);
        }
    ;


ID:     Y_ID
    |   ANY_OP
        {
            yyerror("Cannot use instruction mnemonic as symbol");
            // `ID`s string will be freed, set NULL to prevent segfault
            //   and signal that `ID` is invalid.
            $$.str = NULL;
        }
    |   ANY_POP
        {
            yyerror("Cannot use pseudoinstruction mnemonic as symbol");
            $$.str = NULL;
        }
    |   ANY_DIRECTIVE
        {
            yyerror("Cannot use assembly directive as symbol");
            $$.str = NULL;
        }
    |   Y_RM
        {
            yyerror("Cannot use rounding mode as symbol");
            $$.str = NULL;
        }
    |   Y_CSR
        {
            yyerror("Cannot use CSR name as symbol");
            $$.str = NULL;
        }
    |   Y_MEM_FLAGS
        {
            yyerror("Cannot use memory flags operand (some subset of \"iorw\") as symbol");
            $$.str = NULL;
        }
    |   Y_PC
        {
            yyerror("Cannot use PC as symbol");
            $$.str = NULL;
        }
    ;


/* ========================================================================== *
 * Expression Rules
 * ========================================================================== */

EXPR:
        TRM
    |   EXPR  '+'  TRM
        { $$.i =  $1.i + $3.i; }
    |   EXPR  '-'  TRM
        { $$.i =  $1.i - $3.i; }
    ;

TRM:
        FACTOR
    |   TRM  '*'  FACTOR
        { $$.i = $1.i * $3.i; }
    |   TRM  '/'  FACTOR
        {
            if ($3.i == 0)
                error_at_token("Divide by zero",
                               @2.first_column, @3.last_column);
            $$.i = $1.i / $3.i;
        }
    ;

FACTOR:
        INT
    |   '(' EXPR ')'
        { $$ = $2; }
    ;


/* ========================================================================== *
 * Immediate Expression Rules
 * ========================================================================== */

ID_IMM:
        EXPR  '+'  ID
        {
            $$.expr = make_immexpr($1.i, $3.str);
            free($3.str);
        }

    |   EXPR  '-'  ID
        {
            $$.expr = make_immexpr($1.i, $3.str);
            $$.expr->neg_symbol = true;
            free($3.str);
        }

    |   ID  '+'  EXPR
        {
            $$.expr = make_immexpr($3.i, $1.str);
            free($1.str);
        }

    |   ID  '-'  EXPR
        {
            $$.expr = make_immexpr(- $3.i, $1.str);
            free($1.str);
        }

    |   '-'  ID
        {
            $$.expr = make_immexpr(0, $2.str);
            $$.expr->neg_symbol = true;
            free($2.str);
        }

    |   ID
        {
            $$.expr = make_immexpr(0, $1.str);
            free($1.str);
        }
    ;


SHIFT_INT:
        Y_INT   // `Y_INT` is always positive
        { $$.i = MIN($1.i, IMMEXPR_SHIFT_MAX); }

SHIFTED_ID_IMM:
    /* Arithmetic Right Shift */
        '('  ID_IMM  ')'  '>'  '>'  SHIFT_INT
        {
            $$.expr = $2.expr;
            $$.expr->shift = $6.i;
        }

    |   ID  '>'  '>'  SHIFT_INT
        {
            $$.expr = make_immexpr(0, $1.str);
            $$.expr->shift = $4.i;
            free($1.str);
        }

    |   '('  EXPR  ')'  '>'  '>'  SHIFT_INT
        {
            $$.expr = make_immexpr($2.i >> $6.i, NULL);
        }

    |   INT  '>'  '>'  SHIFT_INT
        {
            $$.expr = make_immexpr($1.i >> $4.i, NULL);
        }

    /* Left Shift */
    |   '('  ID_IMM  ')'  '<'  '<'  SHIFT_INT
        {
            $$.expr = $2.expr;
            $$.expr->shift = $6.i | IMMEXPR_SHIFT_LEFT;
        }

    |   ID  '<'  '<'  SHIFT_INT
        {
            $$.expr = make_immexpr(0, $1.str);
            $$.expr->shift = $4.i | IMMEXPR_SHIFT_LEFT;
            free($1.str);
        }

    |   '('  EXPR  ')'  '<'  '<'  SHIFT_INT
        {
            $$.expr = make_immexpr($2.i << $6.i, NULL);
        }

    |   INT  '<'  '<'  SHIFT_INT
        {
            $$.expr = make_immexpr($1.i << $4.i, NULL);
        }

    /* Logical Right Shift */
    |   '('  ID_IMM  ')'  '>'  '>'  '>'  SHIFT_INT
        {
            $$.expr = $2.expr;
            $$.expr->shift = $7.i | IMMEXPR_SHIFT_LOGIC;
        }

    |   ID  '>'  '>'  '>'  SHIFT_INT
        {
            $$.expr = make_immexpr(0, $1.str);
            $$.expr->shift = $5.i | IMMEXPR_SHIFT_LOGIC;
            free($1.str);
        }

    |   '('  EXPR  ')'  '>'  '>'  '>'  SHIFT_INT
        {
            $$.expr = make_immexpr((uint32_t)($2.i) >> $7.i, NULL);
        }

    |   INT  '>'  '>'  '>'  SHIFT_INT
        {
            $$.expr = make_immexpr((uint32_t)($1.i) >> $5.i, NULL);
        }
    ;


/* ========================================================================== *
 * Immediate Expression Operands
 * ========================================================================== */

IMMEXPR:
        SHIFTED_ID_IMM
    |   ID_IMM
    |   EXPR
        {
            $$.expr = make_immexpr($1.i, NULL);
        }
    ;

IMM_OPR:
        SEP  IMMEXPR
        {
            $$ = $2;
            @$ = @2;
        } ;

SYM_OPR:
        IMM_OPR
        {
            $$ = $1;
            if ($1.expr->symbol == NULL) {
                error_at_token("Expected symbol in expression",
                               @1.first_column, @1.last_column);
            }
        } ;

UIMM_OPR: IMM_OPR
        {
            $$ = $1;
            $$.expr->uimm = true;
        } ;


UIMM5:  UIMM_OPR
        {
            $$ = $1;
            $$.expr->width = 5;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
        } ;

IMM12:  IMM_OPR
        {
            $$ = $1;
            $$.expr->width = 12;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
         } ;

IMM13_PCREL:
         IMM_OPR
        {
            $$ = $1;
            $$.expr->width = 13;
            $$.expr->mask = ~1;
            $$.expr->pc_relative = true;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
        } ;

IMM20:  IMM_OPR
        {
            $$ = $1;
            $$.expr->width = 20;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
        } ;

IMM20_PCREL:
        IMM_OPR
        {
            $$ = $1;
            $$.expr->width = 20;
            $$.expr->pc_relative = true;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
        } ;

IMM21_PCREL:
        IMM_OPR
        {
            $$ = $1;
            $$.expr->width = 21;
            $$.expr->mask = ~1;
            $$.expr->pc_relative = true;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
        } ;


/* ========================================================================== *
 * Base+Offset Address Operands
 * ========================================================================== */

ADDREXPR:
        '('  REGISTER  ')'
        {
            $$.expr = make_immexpr(0, NULL);
            $$.expr->base_reg = $2.i;
        }

    |   IMMEXPR  '('  REGISTER  ')'
        {
            $$ = $1;
            $$.expr->base_reg = $3.i;
        }

    |   IMMEXPR
        {
            $$ = $1;
            $$.expr->base_reg = 0;
        }
    ;

ADDR_OPR:
        SEP  ADDREXPR
        {
            $$ = $2;
            @$ = @2;
        } ;

ADDR12: ADDR_OPR
        {
            $$ = $1;
            $$.expr->width = 12;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
        } ;

ADDR12_PCREL:
        ADDR_OPR
        {
            $$ = $1;
            $$.expr->width = 12;
            $$.expr->pc_relative = true;
            if (!validate_immexpr($$.expr, loaderloc()))
                error_at_token(
                    "Expression value does not fit into immediate field",
                    @1.first_column, @1.last_column);
        } ;


/* ========================================================================== *
 * Register Operands
 * ========================================================================== */

REGISTER:
        Y_REG
    |   Y_FP_REG
        {
            yyerror("Expected integer register, but found FP register.");
        }
    ;

REG_OPR:
        SEP  REGISTER
        {
            $$ = $2;
            @$ = @2;
        } ;

DEST:   REG_OPR ;
SRC1:   REG_OPR ;
SRC2:   REG_OPR ;
TEMP:   REG_OPR ;


FP_REGISTER:
        Y_FP_REG
    |   Y_REG
        {
            yyerror("Expected FP register, but found integer register.");
        }
    ;

F_REG_OPR:
        SEP  FP_REGISTER
        {
            $$ = $2;
            @$ = @2;
        } ;

F_DEST: F_REG_OPR ;
F_SRC1: F_REG_OPR ;
F_SRC2: F_REG_OPR ;
F_SRC3: F_REG_OPR ;


/* ========================================================================== *
 * Special Operands
 * ========================================================================== */

F_RM:   /* empty */
        {
            // Default to `dyn`
            $$.i = 7;
        }
    |   SEP  Y_RM
        {
            $$ = $2;
            @$ = @2;
            if ($$.i != 7)
                yyerror("FP rounding mode not supported");
        }
    ;


CSR_OPR:
        SEP  Y_CSR
        {
            $$ = $2;
            @$ = @2;
        } ;


MEM_FLAGS:
        SEP  Y_MEM_FLAGS
        {
            $$ = $2;
            @$ = @2;
        }
    |   SEP  Y_OR_OP
        {
            // Scanner priotizes instruction mnemonics instead of mem operand
            $$.i = 0x6;
            @$ = @2;
        }
    |   SEP  Y_ORI_OP
        {
            $$.i = 0xe;
            @$ = @2;
        }
    ;


/* ========================================================================== *
 * Data Storage Rules
 * ========================================================================== */

STR:    Y_STR
        {
            store_string($1.str);
            free($1.str);
        }

    |   Y_STR  ':'  INT
        {
            for (int i = 0; i < $3.i; ++i)
                store_string($1.str);
            free ($1.str);
        }
    ;

STR_LIST:
        STR_LIST  SEP  STR
    |   STR
    ;


EXPR_LIST:
        // Need `Y_COMMA` since `-` is ambiguous
        EXPR_LIST  Y_COMMA  EXPR_LIST_ITEM
    |   EXPR_LIST_ITEM
    ;

EXPR_LIST_ITEM:
        IMMEXPR
        {
            memaddr_t addr = 0;
            if ($1.expr->symbol == NULL) {
                // Integer-only, no symbol
                addr = store_expr($1.expr->offset);
            } else if (store_size != BP_WORD) {
                // Can only store symbols in `.word` directive
                error_at_token("Storing symbol expression with non-word sizing",
                               @1.first_column, @1.last_column);
            } else {
                // Store symbol expression, so grab address then write into it
                addr = store_expr(0);
                if (symbol_is_defined($1.expr->symbol)) {
                    // Symbol is defined, write directly into memory
                    write_word(addr, eval_immexpr($1.expr, addr, 0), EMU_ACCESS);
                } else {
                    // Symbol is undefined, record use
                    record_data_uses_symbol($1.expr, addr, $1.expr->symbol);
                }
            }
            free($1.expr);

            if (!this_line_addr)
                this_line_addr = addr;
        }

    |   EXPR  ':'  EXPR
        {
            int i = 0;

            if ($3.i > 0 && !this_line_addr) {
                this_line_addr = store_expr($1.i);
                ++i;
            }

            for ( ; i < $3.i; ++i)
                store_expr($1.i);
        }
    ;


FP_EXPR_LIST:
        FP_EXPR_LIST  Y_COMMA  Y_FP
        {
            // Value is loaded into union as float but we store it as an int
            memaddr_t addr = store_word_data($3.u);
            if (!this_line_addr)
                this_line_addr = addr;
        }

    |   Y_FP
        {
            // Value is loaded into union as float but we store it as an int
            memaddr_t addr = store_word_data($1.u);
            if (!this_line_addr)
                this_line_addr = addr;
        }

    |   Y_FP ':' EXPR
        {
            int i = 0;

            if ($3.i > 0 && !this_line_addr) {
                this_line_addr = store_expr($1.u);
                ++i;
            }

            for ( ; i < $3.i; ++i)
                // Value is loaded into union as float but we store it as an int
                store_expr($1.i);
        }
    ;


%%

/*
 * Exported Functions
 */

// Maintain and update the address of labels for the current line.
void fix_current_label_address(memaddr_t new_addr)
{
    /* @EMPTY: Not sure if necessary anymore */
    // label_list_s *list;
    // for (list = this_line_labels; list != NULL; list = list->next) {
    //     list->l->addr = new_addr;
    // }
}

void initialize_parser(char *file_name)
{
    input_file_name = file_name;
}

static void error_at_token(char *str, int start, int end)
{
    set_error_token(start, end - start);
    yyerror(str);
}

void yyerror(char *s)
{
    // Revert current memory segment's location counter
    if (this_line_addr) {
        M[current_segment_index()].loc_counter = this_line_addr;
        this_line_addr = 0;
    }
    // Signal a parsing erorr and then clear the line labels
    ctx.parse_error_occurred = true;
    clear_labels();
    yywarn(s);
}


/*
 * Internal Functions
 */

/* ========================================================================== *
 * Error Messaging
 * ========================================================================== */

static void yywarn(char *s)
{
    char *error_str = erroneous_line();
    fe->error("VRV: (parser) %s on line %d of file %s\n%s%s",
          s, line_no, input_file_name, *error_str ? "--> " : "", error_str);
    free(error_str);
}


/* ========================================================================== *
 * Line Management
 * ========================================================================== */

static label_list_s *add_line_label(char *name)
{
    label_list_s *list_node = (label_list_s *)xmalloc(sizeof(label_list_s));

    list_node->name = name;
    list_node->next = this_line_labels;
    this_line_labels = list_node;

    return list_node;
}

/* Clear the labels associated with the current line.
 * Should be called whenever memory is stored or on errors. */
static void clear_labels(void)
{
    label_list_s *n;
    for ( ; this_line_labels != NULL; this_line_labels = n) {
        if (this_line_addr)
            record_label(this_line_labels->name, this_line_addr, true);
        n = this_line_labels->next;
        free(this_line_labels);
    }
    this_line_labels = NULL;
}


/* ========================================================================== *
 * Instruction Storage
 * ========================================================================== */

/* Call `store_x_type_inst` from `inst.c` while cleaning up memory */
static void i_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2, immexpr_s *expr)
{
    if (!ctx.parse_error_occurred) {
        char *line = source_line();
        memaddr_t addr = store_i_type_inst(opcode, rd, rs1, rs2, expr, line);
        if (!this_line_addr)
            this_line_addr = addr;
        if (!addr) {
            set_error_current_line();
            yyerror("Error when storing instruction");
            free(line);
        }
    }
    free(expr);
}

static void r_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2)
{
    if (!ctx.parse_error_occurred) {
        char *line = source_line();
        memaddr_t addr = store_r_type_inst(opcode, rd, rs1, rs2, line);
        if (!this_line_addr)
            this_line_addr = addr;
        if (!addr) {
            set_error_current_line();
            yyerror("Error when storing instruction");
            free(line);
        }
    }
}

static void r4_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2, reg_t rs3)
{
    if (!ctx.parse_error_occurred) {
        char *line = source_line();
        memaddr_t addr = store_r4_type_inst(opcode, rd, rs1, rs2, rs3, line);
        if (!this_line_addr)
            this_line_addr = addr;
        if (!addr) {
            set_error_current_line();
            yyerror("Error when storing instruction");
            free(line);
        }
    }
}


/* ========================================================================== *
 * Data Storage
 * ========================================================================== */

/* Select storage function based on `store_size` */
static memaddr_t store_expr(int32_t value)
{
    if (ctx.parse_error_occurred)
        return 0;

    memaddr_t addr;
    int8_t trunc8;
    int16_t trunc16;

    if (is_text_seg(current_segment_index())) {
        if (store_size == BP_WORD)
            return store_word_data(value);
        yyerror("Can't put data in text segment");
        return 0;
    }

    switch (store_size) {
        case BP_BYTE:
            trunc8 = (int8_t)value;
            if (trunc8 != value && (uint8_t)trunc8 != (uint32_t)value)
                yyerror("Expression value too large for byte element");
            addr = store_byte(trunc8);
            break;
        case BP_HALF:
            trunc16 = (int16_t)value;
            if (trunc16 != value && (uint16_t)trunc16 != (uint32_t)value)
                yyerror("Expression value too large for halfword element");
            addr = store_half(trunc16);
            break;
        case BP_WORD:
            // Call `store_word_data` to allow for binary instruction writing
            addr = store_word_data(value);
            break;
        default:
            yyerror("Internal error: invalid storage size");
            return 0;
    }

    if (!this_line_addr)
        this_line_addr = addr;
    return addr;
}

static memaddr_t store_string(char *str)
{
    if (ctx.parse_error_occurred)
        return 0;

    size_t i = 0;
    memaddr_t addr = store_byte(str[i]);
    while (str[i]) {
        ++i;        // Increment after check to catch & store null-terminator
        store_byte(str[i]);
    }

    if (!null_terminate) {
        // Decrement location to revert null-terminator storage
        M[current_segment_index()].loc_counter -= BP_BYTE;
    }

    if (!this_line_addr)
        this_line_addr = addr;
    return addr;
}

/* Store the value either as a datum or instruction. */
static memaddr_t store_word_data(int32_t value)
{
    if (ctx.parse_error_occurred)
        return 0;

    memaddr_t addr;
    if (is_text_seg(current_segment_index()))
        addr = store_inst(inst_decode(value));
    else
        addr = store_word(value);

    if (!this_line_addr)
        this_line_addr = addr;
    return addr;
}


/* ========================================================================== *
 * Instruction Classification
 * ========================================================================== */

static bool inst_is_load(opc_t opcode)
{
    return opcode == Y_LB_OP
        || opcode == Y_LH_OP
        || opcode == Y_LW_OP
        || opcode == Y_LBU_OP
        || opcode == Y_LHU_OP
        || opcode == Y_FLW_OP;
}


/* ========================================================================== *
 * Pseudoinstruction Helpers
 * ========================================================================== */

static void expand_load_store(opc_t opcode, reg_t rd, reg_t rt, immexpr_s *expr)
{
    if (expr_is_gp_rel(expr)) {
        expr->width = 12;
        expr->base_reg = REG_GP;
        i_type_inst(opcode, rd, expr->base_reg, 0, expr);
    } else {
        immexpr_s *hi = expr, *lo;
        split_addr(hi, &lo);
        hi->base_reg = -1;
        lo->base_reg = rt;

        i_type_inst(Y_AUIPC_OP, rt, 0, 0, hi);
        // Loads use `rd` while stores use `rs2`
        if (inst_is_load(opcode))
            i_type_inst(opcode, rd, rt, 0, lo);
        else
            i_type_inst(opcode, 0, rt, rd, lo);
    }
}

static void split_addr(immexpr_s *src_to_hi, immexpr_s **lo)
{
    src_to_hi->pc_relative = true;

    *lo = copy_immexpr(src_to_hi);
    (*lo)->width = 12;
    (*lo)->lo_part = true;

    src_to_hi->width = 20;
    src_to_hi->hi_part = true;
}
