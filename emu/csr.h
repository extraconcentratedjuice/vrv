/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#ifndef CSR_H
#define CSR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "inst.h"
#include "memseg.h"
#include "reg.h"


/*
 * CSR privileges
 */

static inline bool csr_readonly(imm_t csr_encoding) {
    return (csr_encoding & 0xc00) == 0xc00;
}

static inline imm_t csr_mode(imm_t csr_encoding) {
    return (csr_encoding & 0x300) >> 8;
}

static inline bool csr_can_read(imm_t csr_encoding, access_level_e access_lvl) {
    return access_lvl || csr_mode(csr_encoding) == U_MODE_ACCESS;
}

static inline bool csr_can_write(imm_t csr_encoding, access_level_e access_lvl) {
    return access_lvl == EMU_ACCESS
        || (csr_can_read(csr_encoding, access_lvl) && !csr_readonly(csr_encoding))
        || (access_lvl == M_MODE_ACCESS && csr_mode(csr_encoding) == U_MODE_ACCESS);
}


/*
 * CSR indices
 * - Only for CSRs that are actually used by VRV
 */

typedef enum csrindex {

    /* Floating-Point CSRs */
    CSR_FCSR = 0,

    /* User-Mode Counters and Timers */
    /* These CSRs are read-only versions of their machine-mode counterparts.
     *   Besides `time`, which we have chosen to define similarly, the spec
     *   refers to these user-mode registers as "shadows" of the machine-mode
     *   counterpart. This set up allows read-write privileges from machine-mode
     *   and read-only privileges from user-mode purely from the privilege bits
     *   in the CSR encoding.
     * CSR_CYCLE,
     * CSR_CYCLEH,
     * CSR_INSTRET,
     * CSR_INSTRETH,
     * CSR_TIME,
     * CSR_TIMEH,
     */

    /* Machine-Mode Trap Setup */
    CSR_MSTATUS,
    CSR_MSTATUSH,
    CSR_MIE,
    CSR_MTVEC,

    /* Machine-Mode Trap Handling */
    CSR_MSCRATCH,
    CSR_MEPC,
    CSR_MCAUSE,
    CSR_MTVAL,
    CSR_MIP,

    /* Machine-Mode Counters and Timers */
    CSR_MCYCLE,
    CSR_MINSTRET,
    CSR_MCYCLEH,
    CSR_MINSTRETH,

    /* Machine-Mode Memory-Mapped Timing Registers */
    /* Though the spec defines these as memory-mapped, this leads to unnecessary
     *   complexity in VRV. So we map them to actual CSRs instead. */
    CSR_MTIME,
    CSR_MTIMEH,
    CSR_MTIMECMP,
    CSR_MTIMECMPH,

    /* CSR Count */
    CSR_ARRAY_SIZE,

} csrindex_e;


/*
 * Explicit encodings (for parser aliases)
 */

#define CSR_FFLAGS_ENC          1
#define CSR_FRM_ENC             2
#define CSR_FCSR_ENC            3

#define CSR_CYCLE_ENC           0xc00
#define CSR_INSTRET_ENC         0xc01
#define CSR_TIME_ENC            0xc02
#define CSR_CYCLEH_ENC          0xc80
#define CSR_INSTRETH_ENC        0xc81
#define CSR_TIMEH_ENC           0xc82


/*
 * CSR map entry structs
 */

typedef struct csr_encoding {
    char *name;
    csrindex_e index;
    imm_t encoding;
    uregword_t mask;
    uint8_t shift;
} csr_encoding_s;

typedef struct csr_name {
    char *name;
    imm_t encoding;
} csr_name_s;


/*
 * Exported CSR array
 */

extern uregword_t C[];


/*
 * CSR field information
 */

/* fflags */
#define CSR_FFLAGS_INDEX        CSR_FCSR
#define CSR_FFLAGS_MASK         UIMM_MAX(5)
#define CSR_FFLAGS_SHIFT        0

/* frm */
#define CSR_FRM_INDEX           CSR_FCSR
#define CSR_FRM_MASK            UIMM_MAX(5)
#define CSR_FRM_SHIFT           5

/* mstatus.mie */
#define CSR_MSTATUS_MIE_SHIFT   3
#define CSR_MSTATUS_MIE_MASK    UIMM_MAX(1)

/* mstatus.mpie */
#define CSR_MSTATUS_MPIE_SHIFT  7
#define CSR_MSTATUS_MPIE_MASK   UIMM_MAX(1)


/*
 * CSR Masks
 */

#define CSR_MSTATUS_MASK                              \
      (CSR_MSTATUS_MIE_MASK << CSR_MSTATUS_MIE_SHIFT) \
    | (CSR_MSTATUS_MPIE_MASK << CSR_MSTATUS_MPIE_SHIFT)
#define CSR_MIP_MASK                                            \
    ( INT_M_SOFTWARE_BIT | INT_M_TIMER_BIT | INT_M_EXTERNAL_BIT \
    | 0xffff0000) /* Top 16 interrupt bits are for platform use */


/*
 * CSR specialized access
 */

static inline uregword_t get_csr_field(csrindex_e index, uint8_t shift,
                                       uregword_t mask)
{
    return (C[index] >> shift) & mask;
}

static inline void set_csr_field(csrindex_e index, uint8_t shift,
                                 uregword_t mask, uregword_t value)
{
    C[index] = (C[index] & ~(mask << shift)) | ((value & mask) << shift);
}


/*
 * CSR access aliases
 */

/* fflags */
#define GET_FFLAGS          get_csr_field(CSR_FFLAGS_INDEX, CSR_FFLAGS_SHIFT, CSR_FFLAGS_MASK)
#define SET_FFLAGS(v)       set_csr_field(CSR_FFLAGS_INDEX, CSR_FFLAGS_SHIFT, CSR_FFLAGS_MASK, v)

/* frm */
#define GET_FRM             get_csr_field(CSR_FRM_INDEX, CSR_FRM_SHIFT, CSR_FRM_MASK)
#define SET_FRM(v)          set_csr_field(CSR_FRM_INDEX, CSR_FRM_SHIFT, CSR_FRM_MASK, v)

/* mstatus.mie */
#define GET_MSTATUS_MIE     get_csr_field(CSR_MSTATUS, CSR_MSTATUS_MIE_SHIFT, CSR_MSTATUS_MIE_MASK)
#define SET_MSTATUS_MIE(v)  set_csr_field(CSR_MSTATUS, CSR_MSTATUS_MIE_SHIFT, CSR_MSTATUS_MIE_MASK, v)

/* mstatus.mpie */
#define GET_MSTATUS_MPIE    get_csr_field(CSR_MSTATUS, CSR_MSTATUS_MPIE_SHIFT, CSR_MSTATUS_MPIE_MASK)
#define SET_MSTATUS_MPIE(v) set_csr_field(CSR_MSTATUS, CSR_MSTATUS_MPIE_SHIFT, CSR_MSTATUS_MPIE_MASK, v)

#ifdef __cplusplus
}
#endif

#endif  // CSR_H
