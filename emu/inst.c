/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */


#include <stddef.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "csr.h"
#include "emu.h"
#include "frontend.h"
#include "inst.h"
#include "map.h"
#include "memaccess.h"
#include "memloader.h"
#include "op.h"
#include "param.h"
#include "parser.tab.h"
#include "reg.h"
#include "symbol.h"
#include "utils.h"


/*
 * Macros
 */

#define STARTING_INST_CAP       (DEFAULT_SEG_SIZE / BP_WORD)

#define FORMAT_IMMEXPR_DEC_MAX  IMM_MAX(9)
#define FORMAT_IMMEXPR_DEC_MIN  IMM_MIN(9)

#define MAKE_INVALID_INST       (inst_s *)xmallocz(sizeof(inst_s))


/*
 * Local variables
 */

/* Capacity of instruction pointer */
static inst_s *inst_memory;
static size_t inst_capacity;
static inst_s *k_inst_memory;
static size_t k_inst_capacity;

/* Instruction used as breakpoint (by frontend) */
static inst_s *db_break_inst = NULL;

/* Space for read decoded inst */
static inst_s *decoded_inst = NULL;


/*
 * Local functions
 */
static void format_csr(sstream_s *ss, imm_t csr_val);
static void format_imm_val(sstream_s *ss, imm_t val);
static void format_memoprs(sstream_s *ss, imm_t mem_imm);
static void inst_cmp(inst_s *inst1, inst_s *inst2);
static inst_s *make_inst_from_val(memword_t val, opc_t opcode,
                                  reg_t rd, reg_t rs1, imm_t rs2, reg_t rs3,
                                  imm_t imm);


/* ========================================================================== *
 * Immediate Expression Generation and Evaluation
 * ========================================================================== */

/* Return a shallow copy of the `old_expr`. */
immexpr_s *copy_immexpr(immexpr_s *old_expr)
{
    if (!old_expr)
        return NULL;

    immexpr_s *expr = (immexpr_s *)xmalloc(sizeof(immexpr_s));

    *expr = *old_expr;
    return expr;
}

/* Return the value of the `expr`. */
imm_t eval_immexpr(immexpr_s *expr, memaddr_t loc, bool mask_bits)
{
    imm_t value = expr->offset;

    if (expr->symbol != NULL) {
        if (symbol_is_defined(expr->symbol)) {
            // Adjust for `gp` relative address expressions
            memaddr_t addr = expr->symbol->addr;
            if (expr_is_gp_rel(expr))
                addr -= SMALL_DATA_MID;
            // Apply symbol negation
            if (expr->neg_symbol)
                addr *= -1;
            // Apply symbol address to offset
            value += addr;
        } else {
            fe->error("Evaluated undefined symbol: %s\n", expr->symbol->name);
            value = 0;
        }
    }

    // Adjust for pc-relative expressions, but only if a symbol is present
    // - Do this before shifts so that the offset is actually relative to the
    //   given symbol's address.
    if (expr_is_pc_rel(expr)) {
        value -= loc;
        // Adjust `lo` offset so that it's relative PC is the same as `hi`
        // @TODO: It is currently assumed `lo` will be immeidately after `hi`.
        //        This is not necessarily the case when `%pcrel_lo` is added
        // @TODO: This will need to be adjusted for C-ext
        if (expr->lo_part)
            value += BP_WORD;
    }

    // Apply shift
    uint8_t shift_val = expr->shift & IMMEXPR_SHIFT_MAX;
    if (expr->shift & IMMEXPR_SHIFT_LEFT)
        value <<= shift_val;
    else if (expr->uimm || (expr->shift & IMMEXPR_SHIFT_LOGIC))
        value = (uimm_t)value >> shift_val;
    else
        value >>= shift_val;

    if (expr->hi_part) {
        // Adjust as high part of address
        value += (value & (1 << 11));
        value >>= 12;

    } else if (mask_bits || expr->lo_part) {
        // Apply bit mask, if requested or if low part of address
        value = value & expr->mask & UIMM_MAX(expr->width);
        if (!(expr->uimm)) {
            // Sign extend value based on the highest-bit in the field
            value = sign_ex(value, expr->width);
        }
    }

    return value;
}

/* Make and return a new immediate expression */
immexpr_s *make_immexpr(imm_t offs, char *sym)
{
    immexpr_s *expr = (immexpr_s *)xmallocz(sizeof(immexpr_s));

    expr->offset = offs;
    expr->mask = ~0;
    expr->width = 32;
    expr->base_reg = -1;
    if (sym != NULL)
        lookup_or_make_label(sym, &(expr->symbol));
    else
        expr->symbol = NULL;
    return expr;
}


/* ========================================================================== *
 * Instruction Production
 * ========================================================================== */

/* Make and return a deep copy of `inst`. */
inst_s *copy_inst(inst_s *inst)
{
    inst_s *new_inst = (inst_s *)xmalloc(sizeof(inst_s));

    *new_inst = *inst;
    new_inst->expr = copy_immexpr(inst->expr);
    return new_inst;
}

void free_inst(inst_s *inst)
{
    free(inst->source_line);
    free(inst->expr);
    free(inst);
}

/* Produce an immediate instruction. */
memaddr_t store_i_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2,
                            immexpr_s *expr, char *source_line)
{
    // Get empty `inst_s` from instruction memory to fill with values
    inst_s *inst;
    memaddr_t addr = store_empty_inst(&inst);
    if (addr == 0)
        return 0;

    // If inst outside of text sements (i.e. in a data segment),
    //   allocate here just to generate encoding for writing into memory
    bool transient_inst = inst == NULL;
    if (transient_inst) {
        inst = (inst_s *)xmallocz(sizeof(inst_s));
        free(source_line);
        source_line = NULL;
    }

    inst->source_line = source_line;
    inst->addr = addr;
    inst->opcode = opcode;
    inst->rd = rd;
    inst->rs1 = rs1;
    inst->rs2 = rs2;
    inst->expr = copy_immexpr(expr);

    if (expr->symbol == NULL) {
        // No symbol, just evaluate immediate expression
        inst->imm = eval_immexpr(expr, addr, true);

    } else if (symbol_is_defined(expr->symbol)) {
        // Symbol exists and is already defined, use its value
        inst_resolve_label(expr->symbol, inst);

    } else {
        // Record that this instruction references this undefined symbol
        record_inst_uses_symbol(inst, addr, inst->expr->symbol);
    }

    // Write encoded instruction to memory segment
    inst->encoding = inst_encode(inst);
    write_word(addr, inst->encoding, EMU_ACCESS);

    // Free transient `inst_s`
    if (transient_inst)
        free_inst(inst);

    return addr;
}

/* Produce a register-type instruction. */
memaddr_t store_r_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2,
                            char *source_line)
{
    // Get empty `inst_s` from instruction memory to fill with values
    inst_s *inst;
    memaddr_t addr = store_empty_inst(&inst);
    if (addr == 0)
        return 0;

    // If inst outside of text sements (i.e. in a data segment),
    //   allocate here just to generate encoding for writing into memory
    bool transient_inst = inst == NULL;
    if (transient_inst) {
        inst = (inst_s *)xmallocz(sizeof(inst_s));
        free(source_line);
        source_line = NULL;
    }

    inst->source_line = source_line;
    inst->addr = addr;
    inst->opcode = opcode;
    inst->rd = rd;
    inst->rs1 = rs1;
    inst->rs2 = rs2;
    inst->expr = NULL;

    // Write encoded instruction to memory segment
    inst->encoding = inst_encode(inst);
    write_word(addr, inst->encoding, EMU_ACCESS);

    // Free transient `inst_s`
    if (transient_inst)
        free_inst(inst);

    return addr;
}

/* Produce a register-type instruction with 4 registers. */
memaddr_t store_r4_type_inst(opc_t opcode, reg_t rd, reg_t rs1, imm_t rs2,
                             reg_t rs3, char *source_line)
{
    // Get empty `inst_s` from instruction memory to fill with values
    inst_s *inst;
    memaddr_t addr = store_empty_inst(&inst);
    if (addr == 0)
        return 0;

    // If inst outside of text sements (i.e. in a data segment),
    //   allocate here just to generate encoding for writing into memory
    bool transient_inst = inst == NULL;
    if (transient_inst) {
        inst = (inst_s *)xmallocz(sizeof(inst_s));
        free(source_line);
        source_line = NULL;
    }

    inst->source_line = source_line;
    inst->addr = addr;
    inst->opcode = opcode;
    inst->rd = rd;
    inst->rs1 = rs1;
    inst->rs2 = rs2;
    inst->rs3 = rs3;
    inst->expr = NULL;

    // Write encoded instruction to memory segment
    inst->encoding = inst_encode(inst);
    write_word(addr, inst->encoding, EMU_ACCESS);

    // Free transient `inst_s`
    if (transient_inst)
        free_inst(inst);

    return addr;
}


/* ========================================================================== *
 * Instruction Memory
 * ========================================================================== */

void initialize_inst_memory(void)
{
    inst_memory = (inst_s *)xmallocz(STARTING_INST_CAP * sizeof(inst_s));
    inst_capacity = STARTING_INST_CAP;

    k_inst_memory = (inst_s *)xmallocz(STARTING_INST_CAP * sizeof(inst_s));
    k_inst_capacity = STARTING_INST_CAP;
}

void destroy_inst_memory(void)
{
    free(inst_memory);
    inst_memory = NULL;
    inst_capacity = 0;

    free(k_inst_memory);
    k_inst_memory = NULL;
    k_inst_capacity = 0;
}

void free_inst_memory(void)
{
    for (size_t i = 0; i < inst_capacity; ++i) {
        free(inst_memory[i].source_line);
        free(inst_memory[i].expr);
    }
    free(inst_memory);
    inst_memory = NULL;
    inst_capacity = 0;

    for (size_t i = 0; i < k_inst_capacity; ++i) {
        free(k_inst_memory[i].source_line);
        free(k_inst_memory[i].expr);
    }
    free(k_inst_memory);
    k_inst_memory = NULL;
    k_inst_capacity = 0;

    free(decoded_inst);
    decoded_inst = NULL;

    free(db_break_inst);
    db_break_inst = NULL;
}

inst_s *read_inst(memaddr_t addr, access_level_e access_level)
{
    // @TODO: Change checks to 2 bytes and check opcode if implementing C-ext

    // Attempt to read actual memory location first to check for access faults
    // - Force misalignment checks
    ctx.exception_occurred = false;
    int old_me = ctl.misaligned_exceptions;
    ctl.misaligned_exceptions |= MEMOP_LOAD;
    memword_t word = read_word(addr, access_level);
    ctl.misaligned_exceptions = old_me;

    if (ctx.exception_occurred) {
        free(decoded_inst);
        decoded_inst = MAKE_INVALID_INST;
        return decoded_inst;
    }

    seg_idx_e seg_i = segment_index_from_addr(addr, BP_WORD); // <-- @TODO change for C-ext
    size_t index = (addr - M[seg_i].lower_bound) / BP_WORD;

    // Determine which instruction memory to read from.
    if (seg_i == TEXT_SEG && index < inst_capacity) {
        return &inst_memory[index];
    } else if (seg_i == K_TEXT_SEG && index < k_inst_capacity) {
        return &k_inst_memory[index];
    } else {
        // Not in logical instruction memory segment, so decode from byte memory
        free(decoded_inst);
        decoded_inst = inst_decode(word);
        return decoded_inst;
    }
}

/* Store an empty inst in memory and provide its address and inst struct. */
memaddr_t store_empty_inst(inst_s **inst)
{
    // Default `inst` pointer to NULL in case of error
    *inst = NULL;

    // If storing in a text segment, ensure 4-byte alignment
    seg_idx_e seg_i = current_segment_index();
    if (is_text_seg(seg_i) && (segloc(seg_i) & (BP_WORD - 1))) {
        // Unaligned, print error
        fe->error("%s segment has unaligned location counter, cannot store instruction\n",
            seg_i == K_TEXT_SEG ? "Kernel text" : "Text");
        return 0;
    }

    // Store empty instruction in the current memory segment
    // - Misalignment already checked with better messaging
    memaddr_t addr = store_word(0);
    size_t inst_index = (addr - M[seg_i].lower_bound) / BP_WORD;

    // If in a text segment, give the next struct in the appropriate inst memory
    if (seg_i == TEXT_SEG) {
        while (inst_index >= inst_capacity) {
            // Instruction memory full, expand
            size_t old_capacity = inst_capacity;
            inst_capacity = EXPANSION_FACTOR(inst_capacity);
            inst_memory = (inst_s *)xreallocz(inst_memory,
                                              inst_capacity, old_capacity);
        }

        *inst = &inst_memory[inst_index];

    } else if (seg_i == K_TEXT_SEG) {
        while (inst_index == k_inst_capacity) {
            // Instruction memory full, expand
            size_t old_capacity = inst_capacity;
            k_inst_capacity = EXPANSION_FACTOR(k_inst_capacity);
            k_inst_memory = (inst_s *)xreallocz(k_inst_memory,
                                                inst_capacity, old_capacity);
        }

        *inst = &k_inst_memory[inst_index];
    }

    // Return the stored address
    return addr;
}

/* Store `inst` in memory at the next location. */
memaddr_t store_inst(inst_s *inst)
{
    // Get next inst struct and address
    inst_s *im_inst;
    memaddr_t addr = store_empty_inst(&im_inst);
    if (addr == 0)
        return 0;

    // Ensure instruction's address and encoding are set
    inst->addr = addr;
    if (inst->encoding == 0)
        inst->encoding = inst_encode(inst);

    // Write encoded value at address
    write_word(addr, inst->encoding, EMU_ACCESS);

    // Store `inst` in instruction memory, if address in text segment
    if (im_inst != NULL)
        *im_inst = *inst;

    return addr;
}

/* Write `inst` in memory at the given address.
 * Should only be used to overwrite existing instructions in IM. If no existing
 *   instruction (such as data seg addr), memory state may still be changed. */
void write_inst(memaddr_t addr, inst_s *inst, access_level_e access_level)
{
    // Ensure instruction's address and encoding are set
    inst->addr = addr;
    if (inst->encoding == 0)
        inst->encoding = inst_encode(inst);

    // Attempt to get instruction memory slot at address
    ctx.exception_occurred = false;
    inst_s *im_inst = read_inst(addr, access_level);
    if (ctx.exception_occurred)
        return;

    // Store `inst` in instruction memory, if address has IM slot
    if (im_inst)
        *im_inst = *inst;

    // Write encoded value at address
    // - Force misalignment checks
    int old_me = ctl.misaligned_exceptions;
    ctl.misaligned_exceptions |= MEMOP_LOAD;
    write_word(addr, inst->encoding, EMU_ACCESS);
    ctl.misaligned_exceptions = old_me;
}


/* ========================================================================== *
 * Breakpoint Management
 * ========================================================================== */

/* Return true if a debugger breakpoint is set at `addr`. */
bool db_breakpoint_at_addr(memaddr_t addr)
{
    inst_s *inst = read_inst(addr, EMU_ACCESS);
    return inst && inst_is_db_breakpoint(inst);
}

/* Return true if given `inst` is a debugger breakpoint. */
bool inst_is_db_breakpoint(inst_s *inst)
{
    return inst->opcode == Y_EBREAK_OP && inst->rd == 1;
}

/* Set a debugger breakpoint at `addr` and return the old instruction.
 * If the breakpoint cannot be set, return NULL. */
inst_s *set_breakpoint(memaddr_t addr)
{
    inst_s *im_inst, *old_inst;

    if (db_break_inst == NULL) {
        db_break_inst = make_inst_from_val(0, Y_EBREAK_OP, 1, 0, 0, 0, 0);
        db_break_inst->encoding = inst_encode(db_break_inst);
    }

    ctx.exception_occurred = false;
    im_inst = read_inst(addr, EMU_ACCESS);

    // Error reading instruction
    if (ctx.exception_occurred) {
        ctx.exception_occurred = false;
        fe->error("Cannot put a breakpoint at address 0x%08x\n", addr);
        return NULL;
    }
    if (!im_inst) {
        fe->error("No instruction to breakpoint at address 0x%08x\n", addr);
        return NULL;
    }

    // Breakpoint already set here (either debugger or explicit)
    if (im_inst->opcode == Y_EBREAK_OP) {
        fe->error("Breakpoint already set at address 0x%08x\n", addr);
        return NULL;
    }

    old_inst = copy_inst(im_inst);
    write_inst(addr, db_break_inst, EMU_ACCESS);
    ctx.exception_occurred = false; // Ensure no exceptions propogate out
    return old_inst;
}


/* ========================================================================== *
 * Instruction Encoding
 * ========================================================================== */

/* Map between `inst_s` and the binary representation of the instruction. */
memword_t inst_encode(inst_s * inst)
{
    if (inst == NULL)
        return 0;

    op_s *entry = SEARCH_I_OPCODE(inst->opcode);
    if (entry == NULL)
        return 0;

    switch (entry->type) {
        case NOARG_TYPE_INST:
            return entry->a_opcode;

        case R_TYPE_INST:
        case R_FP_TYPE_INST:
        case R_F2X_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_RS1(inst->rs1)
                    | ENC_RS2(inst->rs2);

        case R_FP_RM_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_FUNCT3(7)         // Force `rm=0b111` (dynamic)
                    | ENC_RS1(inst->rs1)
                    | ENC_RS2(inst->rs2);

        case R2_TYPE_INST:
        case R2_F2X_TYPE_INST:
        case R2_X2F_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_RS1(inst->rs1);

        case R2_FP_RM_TYPE_INST:
        case R2_F2X_RM_TYPE_INST:
        case R2_X2F_RM_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_FUNCT3(7)         // Force `rm=0b111` (dynamic)
                    | ENC_RS1(inst->rs1);

        case R4_FP_RM_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_FUNCT3(7)         // Force `rm=0b111` (dynamic)
                    | ENC_RS1(inst->rs1)
                    | ENC_RS2(inst->rs2)
                    | ENC_RS3(inst->rs3);

        case I_TYPE_INST:
        case I_LOAD_TYPE_INST:
        case I_FP_LOAD_TYPE_INST:
        case I_SHIFT_TYPE_INST:
        case I_JALR_TYPE_INST:
        case I_FENCE_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_RS1(inst->rs1)
                    | ENC_FIELD(inst->imm, 12, 20);

        case I_CSR_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_RS1(inst->rs1)
                    | ENC_FIELD(inst->rs2, 12, 20);

        case I_CSRI_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_RS1(inst->imm)
                    | ENC_FIELD(inst->rs2, 12, 20);

        case S_TYPE_INST:
        case S_FP_TYPE_INST:
            // VRV S-Type has source in `rd` instead of `rs2` within `inst_s`
            return entry->a_opcode
                    | ENC_FIELD(inst->imm, 5, 7)
                    | ENC_RS1(inst->rs1)
                    | ENC_RS2(inst->rs2)
                    | ENC_FIELD(inst->imm >> 5, 7, 25);

        case B_TYPE_INST:
            return entry->a_opcode
                    | ENC_FIELD(inst->imm >> 11,  1,  7)
                    | ENC_FIELD(inst->imm >>  1,  4,  8)
                    | ENC_RS1(inst->rs1)
                    | ENC_RS2(inst->rs2)
                    | ENC_FIELD(inst->imm >>  5,  6, 25)
                    | ENC_FIELD(inst->imm >> 12,  1, 31);

        case U_TYPE_INST:
        case U_PCREL_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_FIELD(inst->imm, 20, 12);

        case J_TYPE_INST:
            return entry->a_opcode
                    | ENC_RD(inst->rd)
                    | ENC_FIELD(inst->imm >> 12,  8, 12)
                    | ENC_FIELD(inst->imm >> 11,  1, 20)
                    | ENC_FIELD(inst->imm >>  1, 10, 21)
                    | ENC_FIELD(inst->imm >> 20,  1, 31);

        default:
            fe->fatal_error("Unknown instruction type in inst_encoding\n");
            return 0;           // Not reached
    }
}


/* ========================================================================== *
 * Instruction Decoding
 * ========================================================================== */

/* Generate an `inst_s` from the encoded VAL and all decoded operands. */
static inst_s *make_inst_from_val(memword_t val, opc_t opcode,
                                  reg_t rd, reg_t rs1, imm_t rs2, reg_t rs3,
                                  imm_t imm)
{
    inst_s *inst = (inst_s *)xmallocz(sizeof(inst_s));

    inst->expr = NULL;
    inst->encoding = val;
    inst->addr = 0;
    inst->opcode = opcode;

    inst->rd = rd;
    inst->rs1 = rs1;
    inst->rs2 = rs2;
    inst->rs3 = rs3;
    inst->imm = imm;

    return inst;
}

/* Generate a VRV `inst_s` struct from the binary instruction value. */
inst_s *inst_decode(memword_t val)
{
    int32_t opcode = val & 0x7f;
    int32_t a_opcode = opcode;
    op_s *entry;

    // Use opcode to determine and include instruction-specific fields

    // Check for instructions that have no operands
    if (   (opcode == O_SYSTEM && DEC_FUNCT3(val) == 0) /* ecall/ebreak */
        || (opcode == O_MISC_MEM && DEC_FUNCT3(val) == 1)) /* fence.i */ {
        // Use entire encoded value as opcode
        a_opcode = val;

    } else {
        // Check for instructions that need funct3
        if (  (opcode == O_OP_FP && (DEC_FUNCT7(val) & 0x10)) /* OP-FP w/o rm */
            || opcode == O_BRANCH
            || opcode == O_LOAD
            || opcode == O_LOAD_FP
            || opcode == O_OP
            || opcode == O_OP_IMM
            || opcode == O_STORE
            || opcode == O_STORE_FP
            || opcode == O_SYSTEM) {
            // Include funct3
            a_opcode |= MASK_FUNCT3(val);
        }

        // Check for instructions that need static rs2
        if (opcode == O_OP_FP && (DEC_FUNCT7(val) & 0x20)) {
            // Include rs2
            a_opcode |= MASK_RS2(val);
        }

        // Check for instructions that need funct7
        if (  (opcode == O_OP_IMM && (DEC_FUNCT3(val) & 3) == 1) /* shift-I */
            || opcode == O_OP
            || opcode == O_OP_FP) {
            // Include funct7
            a_opcode |= MASK_FUNCT7(val);
        }
    }

    entry = SEARCH_A_OPCODE(a_opcode);
    if (entry == NULL)
        return MAKE_INVALID_INST;

    switch (entry->type) {
        case NOARG_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode, 0, 0, 0, 0, 0);

        case R_TYPE_INST:
        case R_FP_TYPE_INST:
        case R_FP_RM_TYPE_INST:
        case R_F2X_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),
                                        DEC_RS1(val),
                                        DEC_RS2(val), 0, 0);

        case R2_TYPE_INST:
        case R2_FP_RM_TYPE_INST:
        case R2_F2X_TYPE_INST:
        case R2_F2X_RM_TYPE_INST:
        case R2_X2F_TYPE_INST:
        case R2_X2F_RM_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),
                                        DEC_RS1(val), 0, 0, 0);

        case R4_FP_RM_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),  DEC_RS1(val),
                                        DEC_RS2(val), DEC_RS3(val),
                                        0);

        case I_TYPE_INST:
        case I_LOAD_TYPE_INST:
        case I_FP_LOAD_TYPE_INST:
        case I_JALR_TYPE_INST:
        case I_FENCE_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),  DEC_RS1(val),
                                        0,            0,
                                        sign_ex(DEC_FIELD(val, 12, 20), 12));

        case I_SHIFT_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),  DEC_RS1(val),
                                        0,            0,
                                        DEC_FIELD(val, 5, 20));

        case I_CSR_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),  DEC_RS1(val),
                                        DEC_FIELD(val, 12, 20), 0,
                                        0);

        case I_CSRI_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),  0,
                                        DEC_FIELD(val, 12, 20), 0,
                                        DEC_RS1(val));

        case S_TYPE_INST:
        case S_FP_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        0,            DEC_RS1(val),
                                        DEC_RS2(val), 0,
                                        sign_ex(
                                            DEC_FIELD(val, 5, 7)
                                          | DEC_FIELD(val, 7, 25) << 5,
                                            12));

        case B_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        0,            DEC_RS1(val),
                                        DEC_RS2(val), 0,
                                        sign_ex(
                                            DEC_FIELD(val, 4,  8) <<  1
                                          | DEC_FIELD(val, 6, 25) <<  5
                                          | DEC_FIELD(val, 1,  7) << 11
                                          | DEC_FIELD(val, 1, 31) << 12,
                                            13));

        case U_TYPE_INST:
        case U_PCREL_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),  0,
                                        0,            0,
                                        sign_ex(DEC_FIELD(val, 20, 12), 20));

        case J_TYPE_INST:
            return make_inst_from_val(val, entry->i_opcode,
                                        DEC_RD(val),  0,
                                        0,            0,
                                        sign_ex(
                                            DEC_FIELD(val, 10, 21) <<  1
                                          | DEC_FIELD(val,  1, 20) << 11
                                          | DEC_FIELD(val,  8, 12) << 12
                                          | DEC_FIELD(val,  1, 31) << 20,
                                            21));

        default:
            return MAKE_INVALID_INST;
    }
}


/* ========================================================================== *
 * Instruction Formatting and Printing
 * ========================================================================== */

char *inst_to_string(inst_s *inst)
{
    sstream_s ss;
    ss_init(&ss);

    format_an_inst(&ss, inst, inst->addr);

    return ss_to_string(&ss);
}

char *inst_to_string_at_addr(memaddr_t addr)
{
    inst_s *inst = read_inst(addr, EMU_ACCESS);
    if (inst == NULL) {
        fe->error("Can't stringify instruction, address (0x%08x) not in text segment\n",
            addr);
        return NULL;
    }

    return inst_to_string(inst);
}

static void format_csr(sstream_s *ss, imm_t csr_val)
{
    csr_encoding_s *entry = SEARCH_CSR_ENCODING(csr_val);
    if (entry == NULL) {
        // CSR unknown, just encode the hex value
        ss_printf(ss, "0x%03x", csr_val);
    } else {
        // Print CSR name
        ss_printf(ss, "%s", entry->name);
    }
}

static void format_imm_val(sstream_s *ss, imm_t val) {
    if (val < FORMAT_IMMEXPR_DEC_MIN || val > FORMAT_IMMEXPR_DEC_MAX)
        ss_printf(ss, "0x%08x", val);
    else
        ss_printf(ss, "%d", (int)val);
}


static char mem_opr_map[] = { 'i', 'o', 'r', 'w' };
static void format_memoprs(sstream_s *ss, imm_t mem_imm)
{
    for (int i = 0; i < 2; ++i) {
        ss_printf(ss, ", ");
        for (int j = 0; j < 4; ++j) {
            if (mem_imm & 0x80)
                ss_printf(ss, "%c", mem_opr_map[j]);
            mem_imm <<= 1;
        }
    }
}

void format_an_inst(sstream_s *ss, inst_s *inst, memaddr_t addr)
{
    op_s *entry;
    int line_start = ss_length(ss);

    if (inst && inst_is_db_breakpoint(inst)) {
        inst = inst_at_breakpoint_addr(addr);
        ss_printf(ss, "*");
        format_an_inst(ss, inst, addr);
        return;
    }

    // Print instruction address
    ss_printf(ss, "[0x%08x]    ", inst->addr);

    // Null inst
    if (!inst) {
        ss_printf(ss, "    <none>");
        return;
    }

    // Ensure instruction is encoded
    if (inst->encoding == 0) {
        inst->encoding = inst_encode(inst);
        // Encoding is still 0, treat as empty instruction slot
        if (inst->encoding == 0) {
            ss_printf(ss, "    <none>");
            return;
        }
    }

    // Search for instruction information
    entry = SEARCH_I_OPCODE(inst->opcode);
    if (entry == NULL) {
        ss_printf(ss, "<unknown instruction %d>", inst->opcode);
        return;
    }

    // Print instruction encoding and assembly
    ss_printf(ss, "0x%08x  %s", inst->encoding, entry->name);
    switch (entry->type) {
        case NOARG_TYPE_INST:
            break;

        case R_TYPE_INST:
            ss_printf(ss, " x%d, x%d, x%d", inst->rd, inst->rs1, inst->rs2);
            break;

        case R2_TYPE_INST:
            ss_printf(ss, " x%d, x%d", inst->rd, inst->rs1);
            break;

        case R_FP_TYPE_INST:
            ss_printf(ss, " f%d, f%d, f%d", inst->rd, inst->rs1, inst->rs2);
            break;

        case R_FP_RM_TYPE_INST:
            ss_printf(ss, " f%d, f%d, f%d", inst->rd, inst->rs1, inst->rs2);
            break;

        case R_F2X_TYPE_INST:
            ss_printf(ss, " x%d, f%d, f%d", inst->rd, inst->rs1, inst->rs2);
            break;

        case R2_FP_RM_TYPE_INST:
            ss_printf(ss, " f%d, f%d", inst->rd, inst->rs1);
            break;

        case R4_FP_RM_TYPE_INST:
            ss_printf(ss, " f%d, f%d, f%d, f%d", inst->rd, inst->rs1,
                        inst->rs2, inst->rs3);
            break;

        case R2_F2X_TYPE_INST:
            ss_printf(ss, " x%d, f%d", inst->rd, inst->rs1);
            break;

        case R2_F2X_RM_TYPE_INST:
            ss_printf(ss, " x%d, f%d", inst->rd, inst->rs1);
            break;

        case R2_X2F_TYPE_INST:
            ss_printf(ss, " f%d, x%d", inst->rd, inst->rs1);
            break;

        case R2_X2F_RM_TYPE_INST:
            ss_printf(ss, " f%d, x%d", inst->rd, inst->rs1);
            break;

        case I_TYPE_INST:
        case I_SHIFT_TYPE_INST:
            ss_printf(ss, " x%d, x%d, ", inst->rd, inst->rs1);
            format_imm_val(ss, inst->imm);
            break;

        case I_LOAD_TYPE_INST:
        case I_JALR_TYPE_INST:
            ss_printf(ss, " x%d, ", inst->rd);
            format_imm_val(ss, inst->imm);
            ss_printf(ss, "(x%d)", inst->rs1);
            break;

        case I_FP_LOAD_TYPE_INST:
            ss_printf(ss, " f%d, ", inst->rd);
            format_imm_val(ss, inst->imm);
            ss_printf(ss, "(x%d)", inst->rs1);
            break;

        case I_FENCE_TYPE_INST:
            format_memoprs(ss, inst->imm);
            break;

        case I_CSR_TYPE_INST:
            ss_printf(ss, " x%d, ", inst->rd);
            format_csr(ss, inst->rs2);
            ss_printf(ss, ", x%d", inst->rs1);
            break;

        case I_CSRI_TYPE_INST:
            ss_printf(ss, " x%d, ", inst->rd);
            format_csr(ss, inst->rs2);
            ss_printf(ss, ", 0x%02x", inst->imm);
            break;

        case S_TYPE_INST:
            ss_printf(ss, " x%d, ", inst->rs2);
            format_imm_val(ss, inst->imm);
            ss_printf(ss, "(x%d)", inst->rs1);
            break;

        case S_FP_TYPE_INST:
            ss_printf(ss, " f%d, ", inst->rs2);
            format_imm_val(ss, inst->imm);
            ss_printf(ss, "(x%d)", inst->rs1);
            break;

        case B_TYPE_INST:
            ss_printf(ss, " x%d, x%d, ", inst->rs1, inst->rs2);
            format_imm_val(ss, inst->imm);
            break;

        case U_TYPE_INST:
        case U_PCREL_TYPE_INST:
            ss_printf(ss, " x%d, ", inst->rd);
            format_imm_val(ss, inst->imm);
            break;

        case J_TYPE_INST:
            if (inst->imm < FORMAT_IMMEXPR_DEC_MIN || inst->imm > FORMAT_IMMEXPR_DEC_MAX)
                ss_printf(ss, " x%d, 0x%08x", inst->rd, inst->imm);
            else
                ss_printf(ss, " x%d, %d", inst->rd, inst->imm);
            break;

        default:
            ss_printf(ss, "<unknown instruction %d>", inst->opcode);
            return;
    }

    if (inst->expr != NULL
        && inst->expr->symbol != NULL
        && !inst->expr->symbol->const_flag) {
        // Print immediate expression for calculations leading to final value
        ss_printf(ss, " [");
        format_immexpr(ss, inst);
        ss_printf(ss, "]");
    }

    if (inst->source_line != NULL) {
        // Comment is source line text of current line
        int gap_length = MAX(1, 57 - (ss_length(ss) - line_start));
        for (; 0 < gap_length; gap_length -= 1) {
            ss_printf(ss, " ");
        }

        ss_printf(ss, "; ");
        ss_printf(ss, "%s", inst->source_line);
    }
}

/* Print the `expr`. */
void format_immexpr(sstream_s *ss, inst_s *inst)
{
    immexpr_s *expr = inst->expr;
    bool term_before = false;

    if (expr->hi_part)
        ss_printf(ss, "%%%shi(", expr_is_pc_rel(expr) ? "pcrel_" : "");
    else if (expr->lo_part)
        ss_printf(ss, "%%%slo(", expr_is_pc_rel(expr) ? "pcrel_" : "");

    if (expr->shift)
        ss_printf(ss, "(");

    if (expr->symbol != NULL) {
        const char *neg = expr->neg_symbol ? "-" : "";
        if (expr_is_gp_rel(expr)) {
            ss_printf(ss, "%s%s(= gp %c 0x%08x)",
                neg, expr->symbol->name,
                expr->symbol->addr >= SMALL_DATA_MID ? '+' : '-',
                expr->symbol->addr);
        } else {
            ss_printf(ss, "%s%s(= %s0x%08x)",
                neg, expr->symbol->name, neg, expr->symbol->addr);
        }
        term_before = true;
    }

    if (expr->offset != 0) {
        if (expr->offset < 0 && expr->offset >= FORMAT_IMMEXPR_DEC_MIN)
            ss_printf(ss, "%s%d", term_before ? " - " : "-", - expr->offset);
        else {
            ss_printf(ss, "%s", term_before ? " + " : "+");
            format_imm_val(ss, expr->offset);
        }
        term_before = true;
    }

    if (expr->shift) {
        if (!term_before)
            ss_printf(ss, "0");
        ss_printf(ss, ") >> %u", expr->shift);
        term_before = true;
    }

    if (expr->hi_part || expr->lo_part)
        ss_printf(ss, ")");

    if (expr_is_pc_rel(expr) && !expr->hi_part && !expr->lo_part) {
        ss_printf(ss, "%spc", term_before ? " - " : "-", inst->addr);
        term_before = true;
    }

    if (expr->base_reg != -1 && term_before) {
        if (expr_is_gp_rel(expr))
            ss_printf(ss, " + (gp)");
        else
            ss_printf(ss, " + (x%d)", expr->base_reg);
    }
}

/* Print the given instruction */
void print_inst(inst_s *inst)
{
    char *inst_str = inst_to_string(inst);
    fe->write_output("%s\n", inst_str);
    free(inst_str);
}

/* Print the instruction stored at the memory `addr`. */
void print_inst_at_addr(memaddr_t addr)
{
    char *inst_str = inst_to_string_at_addr(addr);
    if (inst_str) {
        fe->write_output("%s\n", inst_str);
        free(inst_str);
    }
}


/* ========================================================================== *
 * Utility - Validation
 * ========================================================================== */

/* If expression can be evaluated, compare its value against the limits
 * and throw an error if the provided value does not fit in immediate field. */
bool validate_immexpr(immexpr_s *expr, memaddr_t addr)
{
    // Symbol not-yet defined, nothing to validate yet
    if (expr->symbol != NULL && !symbol_is_defined(expr->symbol))
        return true;


    imm_t raw_value = eval_immexpr(expr, addr, false);
    imm_t ex_value = sign_ex(raw_value, expr->width);
    if (expr->uimm) {
        // Unsigned bounds check
        uimm_t u_value = (uimm_t)raw_value;
        if (u_value > UIMM_MAX(expr->width)) {
            fe->error("Immediate value (%u=0x%08x) out of range (%u .. %u)\n",
                  u_value, u_value, 0, UIMM_MAX(expr->width));
            return false;
        }

    } else {
        // Signed bounds check
        if (ex_value < IMM_MIN(expr->width) || ex_value > IMM_MAX(expr->width)) {
            fe->error("Immediate value (%d=0x%08x) out of range (%d .. %d)\n",
                  ex_value, ex_value, IMM_MIN(expr->width), IMM_MAX(expr->width));
            return false;
        }
        // Check for overwritten information in sign-extended bitspan
        uimm_t sign_span = raw_value & ~UIMM_MAX(expr->width);
        if (sign_span != 0 && sign_span != ~UIMM_MAX(expr->width)) {
            fe->error("Immediate value (%d=0x%08x) out of range (%d .. %d)\n",
                  raw_value, raw_value, IMM_MIN(expr->width), IMM_MAX(expr->width));
            return false;
        }
    }

    // Test for bits truncated from mask
    imm_t value = expr->uimm ? raw_value : ex_value;
    imm_t masked_value = eval_immexpr(expr, addr, true);
    if (value != masked_value) {
        fe->error("Immediate value (0x%08x) truncated to (0x%08x)\n",
              value, masked_value);
        return false;
    }

    return true;
}

/* Code to test encode/decode of instructions. */
void test_assembly(inst_s *inst)
{
    if (inst_is_db_breakpoint(inst))
        return;

    inst_s *new_inst = inst_decode(inst_encode(inst));

    inst_cmp(inst, new_inst);
    free_inst(new_inst);
}

static void inst_cmp(inst_s *inst1, inst_s *inst2)
{
    static sstream_s ss;

    ss_clear(&ss);
    if (inst1->opcode != inst2->opcode
            || inst1->encoding != inst2->encoding
            || inst1->rd  != inst2->rd
            || inst1->rs1 != inst2->rs1
            || inst1->rs2 != inst2->rs2
            || inst1->rs3 != inst2->rs3) {
        ss_printf(&ss, "=================== Not Equal ===================\n");
        format_an_inst(&ss, inst1, inst1->addr);
        ss_printf(&ss, "\n===================\n");
        format_an_inst(&ss, inst2, inst2->addr);
        ss_printf(&ss, "\n=================== Not Equal ===================\n");
        fe->error("%s", ss_to_string(&ss));
    }
}
