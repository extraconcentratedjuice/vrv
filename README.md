VRV (Virtual RISC-V) is an RISC-V system emulator, mostly meant for teaching.

VRV implements the instruction set `RV32IMF_Zicsr` (32-bit base integer +
integer multiplication/division support, single-precision floating point
support, and control and status registers support) and offers two privilege
levels: machine and user modes.
