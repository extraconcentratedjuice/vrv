/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

#include <ctype.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "frontend.h"
#include "inst.h"
#include "map.h"
#include "memaccess.h"
#include "memloader.h"
#include "reg.h"
#include "param.h"
#include "parser.h"
#include "parser.tab.h"
#include "scanner.h"
#include "symbol.h"
#include "utils.h"

#define USAGE "[infile [outfile]]"
#define PRINT_USAGE fprintf(stderr, "%s %s\n", argv[0], USAGE)

#define CONSOLE_IN STDIN_FILENO
#define CONSOLE_OUT stdout
#define CONSOLE_ERR stderr

#define BUF_SIZE 4096

static memaddr_t prev_loc_counters[MEMORY_SEG_COUNT];
void print_latest_parsed(void)
{
    sstream_s ss;

    for (seg_idx_e seg_i = 0; seg_i < MEMORY_SEG_COUNT; ++seg_i) {
        // Text segments
        if (is_text_seg(seg_i)) {
            while (segloc(seg_i) > prev_loc_counters[seg_i]) {
                print_inst_at_addr(prev_loc_counters[seg_i]);
                fe->write_output("\n");
                prev_loc_counters[seg_i] += BP_WORD;
            }
        } else {
            // Check current data segment
            while (segloc(seg_i) > prev_loc_counters[seg_i]) {
                memaddr_t addr = prev_loc_counters[seg_i];
                uint8_t byte = read_byte(addr, EMU_ACCESS);
                ss_init(&ss);
                ss_printf(&ss, "[0x%08x]\t0x%02x (", addr, byte);
                ss_print_byte(&ss, byte);
                ss_printf(&ss, ")");
                char *str = ss_to_string(&ss);
                fe->write_output("%s\n", str);
                free(str);
                prev_loc_counters[seg_i] += BP_BYTE;
            }
        }
    }
}

static char tok[256];
/* Returns true when a valid query has been read */
bool special_input_logic(char *line)
{
    sstream_s ss;

    int i = 0;
    // Skip leading spaces
    while (isspace(line[i])) ++i;

    // Read special operator
    char op = tolower(line[i]);
    if (op == 'q') {
        // Quit
        if (isspace(line[i + 1]))
            exit(0);
        return false;

    } else if (op == '?') {
        // Read memory bytes
        // - "? [addr|label]" - Read 4 bytes from the address
        // - "?[num] [addr|label]" - Read `num` bytes from the address
        // - "?i [addr|label]" - Read the instruction at the address
        // - "?i[num] [addr|label]" - Read `num` instructions from the address

        ++i;
        int num_read;

        // Read num bytes or instruction char ('i')
        bool read_inst = line[i] == 'i';
        if (read_inst)
            ++i;
        int num_bytes = read_inst ? BP_BYTE : BP_WORD;
        if (isdigit(line[i])) {
            sscanf(line + i, "%d%n", &num_bytes, &num_read);
            num_bytes = MAX(1, num_bytes);
            i += num_read;
        }

        // Skip leading spaces
        while (isspace(line[i])) ++i;

        // Read address
        memaddr_t addr;
        if (line[i] == '0') {
            // Hex number
            ++i;
            if (line[i] == 'x')
                ++i;
            sscanf(line + i, "%x", &addr);
        } else if (isdigit(line[i])) {
            // Decimal number
            sscanf(line + i, "%d", &addr);
        } else {
            // Label
            sscanf(line + i, "%255s", tok);
            label_s *l;
            if ((l = label_is_defined(tok))) {
                if (l->const_flag) {
                    printf("%s = %d (0x%08x)\n", tok, l->addr, l->addr);
                    return true;
                }
                addr = l->addr;
            }
            else {
                printf("Label \"%s\" is not defined.\n", tok);
                return true;
            }
        }

        // Print bytes or instruction
        for (int i = 0; i < num_bytes; ++i) {
            if (read_inst) {
                print_inst_at_addr(addr);
                addr += BP_WORD;
            } else {
                int8_t byte = read_byte(addr, EMU_ACCESS);
                ss_init(&ss);
                ss_printf(&ss, "[0x%08x]\t0x%02x (", addr, byte);
                ss_print_byte(&ss, byte);
                ss_printf(&ss, ")");
                char *str = ss_to_string(&ss);
                printf("%s\n", str);
                free(str);
                ++addr;
            }
        }

        return true;
    }

    return false;
}

static char *buff = NULL;
static size_t buffsize = 0;
bool read_special_input(void)
{
    ssize_t num_read = getline(&buff, &buffsize, stdin);
    bool valid_query = special_input_logic(buff);
    if (!valid_query) {
        for (ssize_t i = num_read - 1; i >= 0; --i) {
            ungetc(buff[i], stdin);
        }
        return false;
    }
    return true;
}

bool read_from_file(char *name)
{
    int fd = open(name, O_RDONLY);
    if (fd < 0) {
        fe->error("Cannot open file: `%s'\n", name);
        return false;
    }

    dup2(fd, STDIN_FILENO);
    close(fd);
    char c;
    while ((c=getc(stdin)) != EOF) {
        ungetc(c, stdin);
        // Parse file line
        ctx.state = PARSE_STATE;
        yyparse();
        ctx.state = EMU_STATE;
        // If just parsed an instruction, print it
        print_latest_parsed();
    }
    return true;
}

void read_from_stdin(void)
{
    fprintf(stderr, "Interactive mode (enter 'q' to quit):\n");
    while (true) {
        // Attempt to parse special input like quit commands and memory queries
        while (read_special_input());

        // Parse stdin line
        ctx.state = PARSE_STATE;
        yyparse();
        ctx.state = EMU_STATE;

        // If just parsed an instruction, print it
        print_latest_parsed();
    }
}

/* Frontend IO */
static void write_output(const char *fmt, ...);
static void error(const char *fmt, ...);
static void fatal_error(const char *fmt, ...);

fe_api_s parser_fe = {
    .write_output = write_output,
    .error = error,
    .fatal_error = fatal_error,
};


/* Main function */
int main(int argc, char **argv)
{
    if (argc > 3) {
        PRINT_USAGE;
        return 1;
    }

    // Print usage
    if (argc >= 2 && !strcmp(argv[1], "-h")) {
        PRINT_USAGE;
        return 0;
    }

    // Attempt to open and redirect to output file
    if (argc >= 3) {
        int outfd = open(argv[2], O_WRONLY | O_CREAT | O_TRUNC, 0644);
        if (outfd < 0) {
            fprintf(stderr, "Failed to open output file: %s\n", argv[2]);
            return 1;
        }
        dup2(outfd, STDOUT_FILENO);
        close(outfd);
    }

    // Register frontend
    fe = &parser_fe;

    // Initialize structures
    initialize_maps();
    initialize_scanner(stdin);
    initialize_parser(argc >= 2 ? argv[1] : "stdin");
    initialize_memory();
    initialize_inst_memory();
    initialize_symbol_table();

    // Initialize local variables
    for (int i = 0; i < MEMORY_SEG_COUNT; ++i)
        prev_loc_counters[i] = segloc(i);

    // Parse from file or from stdin
    if (argc >= 2) {
        read_from_file(argv[1]);
    } else {
        read_from_stdin();
    }
}


/* ========================================================================== *
 * CLI Platform IO
 * ========================================================================== */

static void write_output(const char *fmt, ...)
{
    va_list args;

    va_start(args, fmt);

    vfprintf(CONSOLE_OUT, fmt, args);
    fflush(CONSOLE_OUT);

    va_end(args);
}

static void error(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    vfprintf(CONSOLE_ERR, fmt, args);
    fflush(CONSOLE_ERR);

    va_end(args);
}

static void fatal_error(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    vfprintf(CONSOLE_ERR, fmt, args);
    fflush(CONSOLE_ERR);

    va_end(args);
    exit(-1);
}
