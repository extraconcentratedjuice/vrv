##
# Copyright (c) 2023 LupLab.
#
# SPDX-License-Identifier: AGPL-3.0-only
##

# This system file includes a TTY IRQ handler and custom TTY syscalls
#   as well as an adapted version of the default core that integrates these.
# The machine trap vector is configured for "vectored" mode which causes
#   interrupts to jump to a PC offset by their interrupt code.
# The TTY IRQ echos recieved characters, loads them into a buffer for future
#   reads, and transmits any buffered characters destined for the device.
# There are PUT, GET, and FLUSH syscalls currently implemented.

# ============================================================================ #
# Constants
# ============================================================================ #

.equ    PRINT_HEX   1
.equ    PRINT_CHR   3
.equ    PRINT_STR   4
.equ    EXIT        20

.equ    NEWLN_CHR   '\n'
.equ    SPACE_CHR   ' '

.equ    MSTATUS_MPIE    0x80
.equ    MCAUSE_INT      0x80000000

.equ    M_EXT_INT   11
.equ    M_EXT_INT_BIT   0x800

.equ    U_ECALL     8
.equ    TTY_PUT     100
.equ    TTY_GET     101
.equ    TTY_FLUSH   102

.equ    TTY_ADDR    0xffff0000
.equ    TTY_WRIT    0x0
.equ    TTY_READ    0x4
.equ    TTY_CTRL    0x8
.equ    TTY_STAT    0xc

.equ    TTY_WBIT    0x1
.equ    TTY_RBIT    0x2

.equ    TTY_BUFSIZE 0x1000  # Must be power-of-2


# ============================================================================ #
# Kernel Data
# ============================================================================ #


    .kdata
.align 12
__kstack_top:   .zero 0x1000 - 4
__kstack:       .zero 4

__m_exc:    .string "  Exception"
__m_int:    .string "  Interrupt"

__m_mcause: .string "\n    MCAUSE: "
__m_mepc:   .string "\n    MEPC:   "
__m_mtval:  .string "\n    MTVAL:  "

__e0:   .string " [Misaligned instruction address]"
__e1:   .string " [Instruction access fault]"
__e2:   .string " [Illegal instruction]"
__e3:   .string " [Breakpoint]"
__e4:   .string " [Misaligned load address]"
__e5:   .string " [Load access fault]"
__e6:   .string " [Misaligned store address]"
__e7:   .string " [Store access fault]"
__e8:   .string " [User-mode ecall]"
__e11:  .string " [Machine-mode ecall]"

__i3:   .string " [Software]"
__i7:   .string " [Timer]"
__i11:  .string " [External]"

__evec: .word __e0, __e1, __e2, __e3, __e4, __e5, __e6, __e7, __e8, 0, 0, __e11
__ivec: .word 0, 0, 0, __i3, 0, 0, 0, __i7, 0, 0, 0, __i11

.align 4
__tty_recv_size:    .word 0
__tty_recv_frnt:    .word 0
__tty_tran_size:    .word 0
__tty_tran_frnt:    .word 0
.align 12
__tty_recvbuf:      .zero TTY_BUFSIZE
__tty_tranbuf:      .zero TTY_BUFSIZE


# ============================================================================ #
# System File Core
# ============================================================================ #

## Kernel text section
    .ktext

### Boot code
    .globl  __mstart
__mstart:
    # Initialize `mtvec` machine trap vector with our trap vector
    la      t0, __trap_handler
    addi    t0, t0, 1       # Use "vectored" trap mode for interrupts
    csrw    mtvec, t0

    # Initialize `mepc` with bootstrap address so we can enter user-mode there
    la      t0, __user_bootstrap
    csrw    mepc, t0

    # Initialize `mscratch` with kernel stack address
    la      t0, __kstack
    csrw    mscratch, t0

    # Initialize `msatus.mpie` to globally enable interrupts
    li      t0, MSTATUS_MPIE
    csrs    mstatus, t0

    # Initialize `mie` to enable external machine interrupts for the TTY
    li      t0, M_EXT_INT_BIT
    csrs    mie, t0

    # Initialize `tty.ctrl` to enable interrupts for both recv and tran
    li      t0, TTY_WBIT + TTY_RBIT
    li      t1, TTY_ADDR
    sw      t0, TTY_CTRL(t1)

    # Enter user boostrap
    mv      t0, x0
    mv      t1, x0
    mret

### Machine trap handler (prints detailed error message and aborts)
__trap_handler:
    # 0. Include jump table for vectored interrupt handling
    j       ____is_exception    #  0 -> Excpetions
    j       ____is_interrupt    #  1 -> Supervisor software interrupt
    j       ____is_interrupt    #  2 -> Reserved
    j       ____is_interrupt    #  3 -> Machine software interrupt
    j       ____is_interrupt    #  4 -> Reserved
    j       ____is_interrupt    #  5 -> Supervisor timer interrupt
    j       ____is_interrupt    #  6 -> Reserved
    j       ____is_interrupt    #  7 -> Machine timer interrupt
    j       ____is_interrupt    #  8 -> Reserved
    j       ____is_interrupt    #  9 -> Supervisor external interrupt
    j       ____is_interrupt    # 10 -> Reserved
    j       __tty_irq           # 11 -> Machine external interrupt
    j       ____is_interrupt    # 12 -> Reserved
    j       ____is_interrupt    # 13 -> Reserved
    j       ____is_interrupt    # 14 -> Reserved
    j       ____is_interrupt    # 15 -> Reserved
    j       ____is_interrupt    # 16 -> Platform-defined
    j       ____is_interrupt    # 17 -> Platform-defined
    j       ____is_interrupt    # 18 -> Platform-defined
    j       ____is_interrupt    # 19 -> Platform-defined
    j       ____is_interrupt    # 20 -> Platform-defined
    j       ____is_interrupt    # 21 -> Platform-defined
    j       ____is_interrupt    # 22 -> Platform-defined
    j       ____is_interrupt    # 23 -> Platform-defined
    j       ____is_interrupt    # 24 -> Platform-defined
    j       ____is_interrupt    # 25 -> Platform-defined
    j       ____is_interrupt    # 26 -> Platform-defined
    j       ____is_interrupt    # 27 -> Platform-defined
    j       ____is_interrupt    # 28 -> Platform-defined
    j       ____is_interrupt    # 29 -> Platform-defined
    j       ____is_interrupt    # 30 -> Platform-defined
    j       ____is_interrupt    # 31 -> Platform-defined

    # 1a. Interrupt, isolate cause and get interrupt message
____is_interrupt:
    csrrw   sp, mscratch, sp
    addi    sp, sp, -32 * 4
    sw      ra, 1 * 4(sp)
    jal     __save_uregs

    la      a0, __m_int     # Interrupt header message
    csrr    t0, mcause
    li      t1, MCAUSE_INT
    xor     t0, t0, t1      # Isolate interrupt code
    la      t1, __ivec      # Interrupt vector
    j       ____print_trap_message

    # 1b. Exception, check for defined-syscall or get exception message
____is_exception:
    csrrw   sp, mscratch, sp
    addi    sp, sp, -32 * 4
    sw      ra, 1 * 4(sp)
    jal     __save_uregs    # Checking for syscalls needs to be recoverable

    csrr    t0, mcause
    li      t1, U_ECALL         # Test for user-ecall with a valid syscall code
    bne     t0, t1, ____not_u_ecall
    li      t1, TTY_PUT
    beq     t1, a7, __tty_put
    li      t1, TTY_GET
    beq     t1, a7, __tty_get
    li      t1, TTY_FLUSH
    beq     t1, a7, __tty_flush

____not_u_ecall:
    la      a0, __m_exc         # Exception header message
    la      t1, __evec          # Isolate exception code

    # 2. Print header message
____print_trap_message:
    li      a7, PRINT_STR
    ecall

    # 3. Print vector entry for this exception/interrupt
    slli    a0, t0, 2       # mcause * 4
    add     a0, t1, a0      # Index in vector
    lw      a0, (a0)        # Entry from vector
    ecall


    # 4. Print mcause
    la      a0, __m_mcause
    ecall
    csrr    a0, mcause
    li      a7, PRINT_HEX
    ecall

    # 5. Print mepc
    la      a0, __m_mepc
    li      a7, PRINT_STR
    ecall
    csrr    a0, mepc
    li      a7, PRINT_HEX
    ecall

    # 6. Print mtval
    la      a0, __m_mtval
    li      a7, PRINT_STR
    ecall
    csrr    a0, mtval
    li      a7, PRINT_HEX
    ecall
    li      a0, NEWLN_CHR
    li      a7, PRINT_CHR
    ecall

    # 7. Restore regs so user can debug properly
    jal     __load_uregs
    lw      ra, 1 * 4(sp)
    addi    sp, sp, 32 * 4
    csrrw   sp, mscratch, sp

    # 8. Exit with code -1
    li      a0, -1
    li      a7, EXIT
    ecall

## User text section
    .text

### User bootstrap:
__user_bootstrap:
    # Invoke the routine `main` with arguments: `main(argc, argv, envp)`.
    # The stack is initialized with the arguments for `main`. Arg registers
    # are preloaded with their values: `a0<-argc`, `a1<-argv`, and `a2<-envp`

    # Jump to `main` function defined in user program
    jal     main

    # Exit (exit code is expected to be returned by main in a0)
    li      a7, EXIT
    ecall


# ============================================================================ #
# Kernel Utilities
# ============================================================================ #

    .ktext
## Save User Registers
__save_uregs:
    addi    sp, sp, -32 * 4
    sw      x5,   5 * 4(sp)
    sw      x6,   6 * 4(sp)
    sw      x7,   7 * 4(sp)
    sw      x8,   8 * 4(sp)
    sw      x9,   9 * 4(sp)
    sw      x10, 10 * 4(sp)
    sw      x11, 11 * 4(sp)
    sw      x12, 12 * 4(sp)
    sw      x13, 13 * 4(sp)
    sw      x14, 14 * 4(sp)
    sw      x15, 15 * 4(sp)
    sw      x16, 16 * 4(sp)
    sw      x17, 17 * 4(sp)
    sw      x18, 18 * 4(sp)
    sw      x19, 19 * 4(sp)
    sw      x20, 20 * 4(sp)
    sw      x21, 21 * 4(sp)
    sw      x22, 22 * 4(sp)
    sw      x23, 23 * 4(sp)
    sw      x24, 24 * 4(sp)
    sw      x25, 25 * 4(sp)
    sw      x26, 26 * 4(sp)
    sw      x27, 27 * 4(sp)
    sw      x28, 28 * 4(sp)
    sw      x29, 29 * 4(sp)
    sw      x30, 30 * 4(sp)
    sw      x31, 31 * 4(sp)
    ret


## Load User Registers
__load_uregs:
    lw      x5,   5 * 4(sp)
    lw      x6,   6 * 4(sp)
    lw      x7,   7 * 4(sp)
    lw      x8,   8 * 4(sp)
    lw      x9,   9 * 4(sp)
    lw      x10, 10 * 4(sp)
    lw      x11, 11 * 4(sp)
    lw      x12, 12 * 4(sp)
    lw      x13, 13 * 4(sp)
    lw      x14, 14 * 4(sp)
    lw      x15, 15 * 4(sp)
    lw      x16, 16 * 4(sp)
    lw      x17, 17 * 4(sp)
    lw      x18, 18 * 4(sp)
    lw      x19, 19 * 4(sp)
    lw      x20, 20 * 4(sp)
    lw      x21, 21 * 4(sp)
    lw      x22, 22 * 4(sp)
    lw      x23, 23 * 4(sp)
    lw      x24, 24 * 4(sp)
    lw      x25, 25 * 4(sp)
    lw      x26, 26 * 4(sp)
    lw      x27, 27 * 4(sp)
    lw      x28, 28 * 4(sp)
    lw      x29, 29 * 4(sp)
    lw      x30, 30 * 4(sp)
    lw      x31, 31 * 4(sp)
    addi    sp, sp, 32 * 4
    ret



# ============================================================================ #
# TTY IRQ Handler and Syscalls
# ============================================================================ #

    .ktext
### TTY Interrupt Handler
# Echos any ready-read characters and appends them to the receive buffer.
# Transmits the next byte, if any, if the device is ready to receive.
# If, when receiving, either buffer is full, the IRQ will not wait and will
#   merely drop the byte from that buffer. This is not ideal, but is fine for
#   this simple implementation.
###
__tty_irq:
    csrrw   sp, mscratch, sp
    addi    sp, sp, -32 * 4
    sw      ra, 1 * 4(sp)
    jal     __save_uregs

    # Load `tty.stat`
    li      s0, TTY_ADDR
    lw      s1, TTY_STAT(s0)

    # Check `tty.stat.rbit`
____tty_check_rbit:
    andi    t0, s1, TTY_RBIT
    beqz    t0, ____tty_check_wbit

    # rbit set, so read, echo (tran append), then recv append
    lw      s3, TTY_READ(s0)
    la      a0, __tty_tranbuf
    la      a1, __tty_tran_size
    mv      a2, s3
    jal     __tty_append
    la      a0, __tty_recvbuf
    la      a1, __tty_recv_size
    mv      a2, s3
    jal     __tty_append

    # Check `tty.stat.wbit`
____tty_check_wbit:
    andi    t0, s1, TTY_WBIT
    beqz    t0, ____tty_irq_end

    # wbit set, so lower write irq, then try tran consume
    lw      x0, TTY_WRIT(s0)
    la      a0, __tty_tranbuf
    la      a1, __tty_tran_size
    jal     __tty_consume
    bltz    a0, ____tty_irq_end # Nothing consumed, skip the write
    sw      a0, TTY_WRIT(s0)    # Transmit byte

____tty_irq_end:
    jal     __load_uregs
    lw      ra, 1 * 4(sp)
    addi    sp, sp, 32 * 4
    csrrw   sp, mscratch, sp
    mret


### TTY PUT Syscall
# Appends the given byte to the transmission buffer and returns 0,
#   or returns -1 if the transmission buffer is full.
# If an append action is taken, will attempt to transmit the next byte to
#   as the IRQ may have already been lowered.
###
__tty_put:
    # User registers alreayd saved

    # Append byte to transmission buffer
    mv      a2, a0      # Argument byte in a0
    la      a0, __tty_tranbuf
    la      a1, __tty_tran_size
    jal     __tty_append
    sw      x10, 10 * 4(sp)   # Store result of append in user `a0`

    # Check `tty.stat.wbit` to see if we should write immediately
    li      s0, TTY_ADDR
    lw      t0, TTY_STAT(s0)
    andi    t0, t0, TTY_WBIT
    beqz    t0, ____tty_put_end

    # Transmit one byte from the buffer
    la      a0, __tty_tranbuf
    la      a1, __tty_tran_size
    jal     __tty_consume
    bltz    a0, ____tty_irq_end # Nothing consumed, skip the write
    sw      a0, TTY_WRIT(s0)    # Transmit byte

____tty_put_end:
    # Increment mepc
    csrr    t0, mepc
    addi    t0, t0, 4
    csrw    mepc, t0

    # Restore state, but load overwrite result in `a0` first
    jal     __load_uregs
    lw      ra, 1 * 4(sp)
    addi    sp, sp, 32 * 4
    csrrw   sp, mscratch, sp
    mret


### TTY GET Syscall
# Consumes and returns a byte from the receive buffer,
#  or returns -1 if the receive buffer is empty.
###
__tty_get:
    # User registers alreayd saved

    # Consume byte from recieve buffer
    la      a0, __tty_recvbuf
    la      a1, __tty_recv_size
    jal     __tty_consume

    # Increment mepc
    csrr    t0, mepc
    addi    t0, t0, 4
    csrw    mepc, t0

    # Restore state, but load overwrite result in `a0` first
    sw      x10, 10 * 4(sp)
    jal     __load_uregs
    lw      ra, 1 * 4(sp)
    addi    sp, sp, 32 * 4
    csrrw   sp, mscratch, sp
    mret


### TTY FLUSH Syscall
# Flushes the transmission buffer, only returning control to the user once
#   buffer has been emptied and all bytes have reached the device.
###
__tty_flush:
    # User registers alreayd saved

    # Setup repeatedly-used values
    li      s0, TTY_ADDR
    la      s1, __tty_tranbuf
    la      s2, __tty_tran_size

____tty_flush_loop:
    # Check to see if we can transmit / if our byte has been received
    lw      t0, TTY_STAT(s0)
    andi    t0, t0, TTY_WBIT
    beqz    t0, ____tty_flush_loop

    # wbit set, so lower write irq, then try tran consume
    lw      x0, TTY_WRIT(s0)
    mv      a0, s1
    mv      a1, s2
    jal     __tty_consume
    bltz    a0, ____tty_flush_end   # Nothing consumed, end the flush
    sw      a0, TTY_WRIT(s0)        # Transmit byte
    j       ____tty_flush_loop      # Wait to send next byte

____tty_flush_end:
    # Increment mepc
    csrr    t0, mepc
    addi    t0, t0, 4
    csrw    mepc, t0

    # Restore state, but load overwrite result in `a0` first
    sw      x10, 10 * 4(sp)
    jal     __load_uregs
    lw      ra, 1 * 4(sp)
    addi    sp, sp, 32 * 4
    csrrw   sp, mscratch, sp
    mret


### TTY Append
# Arguments:
# - a0 <- address of buffer
# - a1 <- address of {size,front} pair
# - a2 <- byte to append
# Returns:
# - a0 <- 0 on successful append, -1 if buffer is full
###
__tty_append:
    # Check size to ensure there's room to append
    lw      t0, 0(a1)   # Get buffer content size
    li      t1, TTY_BUFSIZE - 1
    bgt     t0, t1, ____tty_append_fail     # No room to append, fail

    # Append argument byte
    lw      t2, 4(a1)   # `front` is stored just after `size`
    add     t2, t2, t0  # Get `back` index
    and     t2, t2, t1  # Wrap `back` index around
    add     a0, a0, t2  # `a0` contains buffer address
    sb      a2, 0(a0)   # Append at `buf[(front + size) % bufsize]`

    # Increment size
    addi    t0, t0, 1
    sw      t0, 0(a1)

____tty_append_end:
    li      a0, 0   # Zero signals success
    ret

____tty_append_fail:
    li      a0, -1
    ret


### TTY Consume
# Arguments:
# - a0 <- address of buffer
# - a1 <- address of {size,front} pair
# Returns:
# - a0 <- consumed byte on success, -1 if buffer is empty
###
__tty_consume:
    # Check size to ensure there's room to append
    lw      t0, 0(a1)   # Get buffer content size
    beqz    t0, ____tty_consume_fail    # Nothing to consume, fail

    # Grab byte
    lw      t1, 4(a1)   # `front` is stored just after `size`
    add     t2, t1, a0
    lbu     a0, 0(t2)   # `buff[front]`

    # Decrement size and increment front
    addi    t0, t0, -1
    sw      t0, 0(a1)
    addi    t1, t1, 1
    li      t2, TTY_BUFSIZE - 1
    and     t1, t1, t2  # Wrap `front` index around
    sw      t1, 4(a1)

____tty_consume_end:
    ret

____tty_consume_fail:
    li      a0, -1
    ret
