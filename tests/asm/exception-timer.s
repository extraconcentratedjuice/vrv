##
# Copyright (c) 1990-2023 James R. Larus.
# Copyright (c) 2023 LupLab.
#
# SPDX-License-Identifier: BSD-3-Clause
##

# This is the exception handler code that the processor runs when
# an exception occurs. It only prints some information about the
# exception, but can server as a model of how to write a handler.

# Exception vector elements are repesented by:
#   [0:1]:  A single byte with the following representation values:
#       0x01:        Print " {mtval}"
#       0x02:        Print " {mtval2}"
#       0x03:        Both of above
#       0x04:        Print " at {mepc}"
#       0x08:        Print " code {a7}"
#       0x40:        Any of the above and do not increment `mepc`
#       0x80:        Any of the above and exit the program
#   [1:2+]: A string containing the message

    .kdata
__m_exc:    .string "  Exception"
__m_int:    .string "  Interrupt"

__m_mcause: .string "\n\tMCAUSE: "
__m_mepc:   .string "\n\tMEPC:   "
__m_mtval:  .string "\n\tMTVAL:  "
__m_mtval2: .string "\n\tMTVAL2: "


__e0:   .string " [Misaligned instruction address]"
__e1:   .string " [Instruction access fault]"
__e2:   .string " [Illegal instruction]"
__e3:   .string " [Breakpoint]"
__e4:   .string " [Misaligned load address]"
__e5:   .string " [Load access fault]"
__e6:   .string " [Misaligned store address]"
__e7:   .string " [Store access fault]"
__e8:   .string " [User-mode ecall]"
__e11:  .string " [Machine-mode ecall]"

__i3:   .string " [Software]"
__i7:   .string " [Timer]"
__i11:  .string " [External]"

__evec: .word __e0, __e1, __e2, __e3, __e4, __e5, __e6, __e7, __e8, 0, 0, __e11
__ivec: .word 0, 0, 0, __i3, 0, 0, 0, __i7, 0, 0, 0, __i11


# Constant symbols
    .equ    PRINT_HEX   1
    .equ    PRINT_CHR   3
    .equ    PRINT_STR   4
    .equ    EXIT        20

    .equ    NEWLN_CHR   '\n'
    .equ    SPACE_CHR   ' '

    .equ    MPIE_BIT    0x80

    .equ    TIME_DELTA  0x100
    .equ    TIMER_BIT   0x80


# Exception vector processor:
#   Interrupts the exception code and values in the exception vector to print
#   an informative string about the ecountered exception.
#
    .ktext
__exception_handler:
    # Save `ra` to allow calling of kernel functions
    addi    sp, sp, -4
    sw      ra, (sp)

    # Save temp registers
    jal     __save_arg_regs

    # Determine if this is an exception or an interrupt
    csrr    a3, mcause      # a3 <- mcause
    la      a4, __evec      # a4 <- exception vector
    la      a0, __m_exc     # a0 <- exception message
    xor     a6, a6, a6      # a6 <- 0 (cause is not an interrupt)
    li      a5, 0x80000000  # a5 <- 0x80000000

    # Cause is in an interrupt
    and     a5, a3, a5      # a5 <- mcause & 0x80000000
    beqz    a5, ____not_interrupt
    addi    a6, a6, 1       # a6 <- 1 (cause is an interrupt)
    la      a4, __ivec      # a4 <- interrupt vector
    la      a0, __m_int     # a0 <- interrupt messsage
    xor     a3, a3, a5      # Clear high bit in mcause (for indexing)

    # - Clear interrupt bit in `mip`
    li      a5, 1
    sll     a5, a5, a3
    csrc    mip, a5
____not_interrupt:

    # Print header message
    li      a7, PRINT_STR
    ecall                   # Syscall: print string

    # Get vector entry for this exception/interrupt
    slli    a3, a3, 2       # a3 <- mcause * 4
    add     a4, a4, a3      # a4 <- index into vector
    lw      a4, (a4)        # a4 <- entry from vector

    # Print exception string
    mv      a0, a4
    ecall

    # Print mcause
    la      a0, __m_mcause
    ecall
    csrr    a0, mcause
    li      a7, PRINT_HEX
    ecall

    # Print mepc
    la      a0, __m_mepc
    li      a7, PRINT_STR
    ecall
    csrr    a0, mepc
    li      a7, PRINT_HEX
    ecall

    # Print mtval
    la      a0, __m_mtval
    li      a7, PRINT_STR
    ecall
    csrr    a0, mtval
    li      a7, PRINT_HEX
    ecall

    # Print mtval2
    la      a0, __m_mtval2
    li      a7, PRINT_STR
    ecall
    csrr    a0, mtval2
    li      a7, PRINT_HEX
    ecall
    li      a0, NEWLN_CHR
    li      a7, PRINT_CHR
    ecall

    # Skip exit if exception is a timer interrupt
    bnez    a6, ____timer_interrupt

    # Exit with code -1
    li      a0, -1
    li      a7, EXIT
    ecall

____timer_interrupt:
    # Interrupt is a timer interrupt
    # Increment mtimecmp
    csrr    a7, mtimecmp
    li      a5, TIME_DELTA
    add     a5, a5, a7
    csrw    mtimecmp, a5

    # Increment mtimecmph, if necessary
    bgt     a5, a7, ____not_mtimecmph_inc
    csrr    a6, mtimecmph
    addi    a6, a6, 1
    csrw   mtimecmph, a6
____not_mtimecmph_inc:

    # - Clear timer interrupt bit excplitily now that mtimcmp incremented
    li      a5, TIMER_BIT
    csrc    mip, a5

    # Restore arg registers
    jal     __restore_arg_regs

    # Restore `ra`
    lw      ra, (sp)
    addi    sp, sp, 4

    # Return control to user-mode
    mret


# Register state management
#   Procedures to save and restore arg register states for use by kernel.
#
    .ktext
__save_arg_regs:
    addi    sp, sp, -8 * 4
    sw      a0, 0 * 4(sp)
    sw      a1, 1 * 4(sp)
    sw      a2, 2 * 4(sp)
    sw      a3, 3 * 4(sp)
    sw      a4, 4 * 4(sp)
    sw      a5, 5 * 4(sp)
    sw      a6, 6 * 4(sp)
    sw      a7, 7 * 4(sp)
    ret
__restore_arg_regs:
    lw      a0, 0 * 4(sp)
    lw      a1, 1 * 4(sp)
    lw      a2, 2 * 4(sp)
    lw      a3, 3 * 4(sp)
    lw      a4, 4 * 4(sp)
    lw      a5, 5 * 4(sp)
    lw      a6, 6 * 4(sp)
    lw      a7, 7 * 4(sp)
    addi    sp, sp, 8 * 4
    ret


# Standard startup code:
#   Initialize CSRs that control trap handling and machine state.
#
    .ktext
    .globl  __start
__start:
    # Initialize `mtvec` machine trap vector with our exception handler address
    la      a0, __exception_handler
    csrw    mtvec, a0

    # Enable global interrupts when in user-mode (`mie <- mpie` upon `mret`)
    li      a0, MPIE_BIT
    csrs    mstatus, a0
    li      a0, TIMER_BIT
    csrs    mie, a0

    # Initialize `mepc` with bootloader address so we can enter user-mode there
    la      a0, __user_bootstrap
    csrw    mepc, a0

    # Set mtimecmp
    li      a0, TIME_DELTA
    csrw    mtimecmp, a0
    csrw    mtimecmph, zero

    # Enter bootloader in user-mode
    mret


# Bootloader:
#   Invoke the routine `main` with arguments: `main(argc, argv, envp)`
#
    .text
__user_bootstrap:
    # The stack is initialized with the arguments for `main`. Arg registers
    #   are preloaded with their values: `a0<-argc`, `a1<-argv`, and `a2<-envp`

    # Jumpt to `main` function in user program
    jal     main

    # Exit
    li      a7, EXIT
    ecall
