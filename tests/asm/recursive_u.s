.equ ITER_COUNT (1024 * 1024) - 512

    .text
    .globl main
main:
    addi    sp, sp, -4
    sw      ra, 0(sp)
    li      a0, ITER_COUNT
    jal     rec
    lw      ra, 0(sp)
    addi    sp, sp, 4
    li      a0, 0
    ret

rec:
    addi    sp, sp, -4
    sw      ra, 0(sp)
    addi    a0, a0, -1
    blez    a0, rec_end
    jal     rec
rec_end:
    lw      ra, 0(sp)
    addi    sp, sp, 4
    ret
