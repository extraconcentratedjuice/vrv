/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include <QString>

#include "format.h"

/*
 * Local helpers
 */
enum class BaseFormat {
    Base2,      /* Binary */
    Base10,     /* Decimal */
    Base16,     /* Hexadecimal */
};

static struct {
    int base;
    QString prefix;
    int width;
    QLatin1Char pad;
} baseProp[] = {
    { 2,    "0b",   32, QLatin1Char('0') },
    { 10,   "",     10, QLatin1Char(' ') },
    { 16,   "0x",   8,  QLatin1Char('0') },
};

static BaseFormat baseToFormat(int base)
{
    switch (base) {
        case 2:
            return BaseFormat::Base2;
        case 10:
            return BaseFormat::Base10;
        case 16:
        default:
            return BaseFormat::Base16;
    }
}

/*
 * Base conversion functions
 */
QString format(uint val, int base, bool prefix, int width)
{
    auto prop = baseProp[static_cast<int>(baseToFormat(base))];
    return QStringLiteral("%1%2")
        .arg(prefix ? prop.prefix : "")
        .arg(val, width ? width : prop.width, prop.base, prop.pad);
}

QString formatAddress(uint addr)
{
    return format(addr, 16, false);
}

/*
 * HTML-oriented formatting functions
 */
QString formatByteHtml(uchar byte, int base)
{
    return format(byte, base, false, 2).replace(' ', "&nbsp;");
}

QString formatWordHtml(uint word, int base)
{
    return format(word, base, false).replace(' ', "&nbsp;");
}

QString formatCharHtml(char inputChar)
{
    if (QChar::isPrint(inputChar))
        return QString(inputChar).toHtmlEscaped().replace(' ', "&nbsp;");
    else
        return QString('.');
}

QString formatSegLabelHtml(QString segName, uint lowAddr, uint highAddr)
{
    return QString("<div class=\"segment\">")
        % QString("<p style=\"text-align:center; font-weight: bold;\">")
        % segName
        % QString(" [") % formatAddress(lowAddr)
        % QString("]..[") % formatAddress(highAddr)
        % QString("]</p></div>");
}

QString genNbspHtml(int n)
{
    QString str;
    for (int i = 0; i < n; i++)
        str += "&nbsp;";
    return str;
}
