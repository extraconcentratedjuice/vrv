/*
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef REG_MODEL_H
#define REG_MODEL_H

#include <QList>
#include <QString>

#include "format.h"

#include "../emu/csr.h"
#include "../emu/run.h"

/* Base virtual class for all types of registers */
class RegModel
{
public:
    virtual int length(void) = 0;

    virtual QString number(int i) = 0;
    virtual QString name(int i) = 0;
    virtual QString value(int i) = 0;

    virtual QString modelName() = 0;

    virtual QString toPlainText() {
        QString str = modelName();
        for (auto i = 0; i < length(); i++)
            str += "\n" % number(i) % " " % name(i) % " " % value(i);
        return str;
    }

protected:
    int base = 16;  /* Default base */
};

/* Register model for general-purpose registers */
class GPRModel : public RegModel
{
public:
    GPRModel() : RegModel() {
        /* Remove duplicate `fp` which is the same as s0 (x8) */
        gprName.removeAt(8);
    }

    int length(void) {
        return gprName.length();
    }

    QString number(int i) {
        return QStringLiteral("x%1").arg(i);
    }

    QString name(int i) {
        return QString(gprName[i]);
    }

    QString value(int i) {
        if (base == 10)
            return QString::number(R[i]);
        else
            return format(R[i], base, false);
    }

    QString modelName() {
        return "General-purpose Registers";
    }

private:
    QList<QString> gprName = {
#define GPR_MAP(name, val) \
    name,
#include "../emu/gpr_map.h"
#undef GPR_MAP
    };
};

/* Register model for floating-point registers */
class FPRModel : public RegModel
{
public:
    FPRModel() : RegModel() { }

    int length(void) {
        return fprName.length();
    }

    QString number(int i) {
        return QStringLiteral("f%1").arg(i);
    }

    QString name(int i) {
        return QString(fprName[i]);
    }

    QString value(int i) {
        float fpval = FR[i];
        int *ifpval = (int*)&fpval;

        if (base == 10)
            return QString::number(fpval);
        else
            return format(*ifpval, 16, false);
    }

    QString modelName() {
        return "Floating-point Registers";
    }

private:
    QList<QString> fprName = {
#define FPR_MAP(name, val) \
    name,
#include "../emu/fpr_map.h"
#undef FPR_MAP
    };
};

/* Register model for CSRs */
class CSRModel : public RegModel
{
public:
    CSRModel() : RegModel() { }

    int length(void) {
        return csrList.length();
    }

    QString number(int i) {
        csr_s &csr = csrList[i];
        return QStringLiteral("0x%1")
            .arg(csr.encoding, 3, 16, QLatin1Char('0'));
    }

    QString name(int i) {
        csr_s &csr = csrList[i];
        return QString(csr.name);
    }

    QString value(int i) {
        csr_s &csr = csrList[i];
        int val = (C[csr.index] >> csr.shift) & csr.mask;

        return format(val, base, false);
    }

    QString modelName() {
        return "Control and Status Registers";
    }

private:
    struct csr_s {
        QString name;
        uint index;
        uint encoding;
        uint mask;
        uint shift;
    };

    QList<csr_s> csrList = {
#define CSR_MAP(name, index, number, mask, shift) \
    { name, index, number, mask, shift },
#include "../emu/csr_map.h"
#undef CSR_MAP
    };
};

#endif /* REG_MODEL_H */
