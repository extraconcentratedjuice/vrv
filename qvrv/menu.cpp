/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include <QStringBuilder>
#define QT_USE_FAST_CONCATENATION
#include <QMessageBox>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QFontDialog>
#include <QColorDialog>
#include <QProcess>
#include <QScrollBar>
#include <QSignalMapper>

#include "datawidget.h"
#include "vrvwin.h"
#include "ui_savelogfile.h"
#include "ui_breakpoint.h"
#include "format.h"

#include "../emu/emu.h"
#include "../emu/frontend.h"
#include "../emu/reg.h"
#include "../emu/run.h"
#include "../emu/symbol.h"
#include "../emu/utils.h"

/*
 * File menu
 */
void VrvWindow::file_LoadFile()
{
    fileLoad_d->show();
}

void VrvWindow::file_SaveLogFile()
{
    static QDialog *saveLogFileDialog;
    static Ui::SaveLogFileDialog *slf;

    if (saveLogFileDialog == NULL) {
        saveLogFileDialog = new QDialog();

        slf = new Ui::SaveLogFileDialog();
        slf->setupUi(saveLogFileDialog);

        QFileDialog* fb = new QFileDialog(this, "Save to Log File", "",
                                          "All Files (*);;Text Files (*.txt)");

        /* If user clicks on "Browse" to select a file, open the QFileDialog */
        connect(slf->saveFileBrowser, &QToolButton::clicked,
                fb, &QFileDialog::exec);
        /* Set edit line automatically when QFileDialog returns */
        connect(fb, &QFileDialog::fileSelected,
                slf->saveFileEdit, &QLineEdit::setText);
    }

    if (saveLogFileDialog->exec() == QDialog::Accepted) {
        QString fileName = slf->saveFileEdit->text();

        if (fileName.isEmpty())
            return;

        QFile file(fileName);
        file.open(QIODevice::WriteOnly | QIODevice::Truncate);
        QTextStream fileStream(&file);

        if (slf->gprSave->isChecked())
            fileStream << gprModel.toPlainText() << "\n\n";

        if (slf->fprSave->isChecked())
            fileStream << fprModel.toPlainText() << "\n\n";

        if (slf->csrSave->isChecked())
            fileStream << csrModel.toPlainText() << "\n\n";

        if (slf->textSave->isChecked())
            fileStream << ui->textWidget->toPlainText() << "\n\n";

        if (slf->dataSave->isChecked())
            fileStream << ui->dataWidget->toPlainText() << "\n\n";

        if (slf->consoleSave->isChecked())
            fileStream << VRVConsole->toPlainText();

        file.close();
    }
}

void VrvWindow::fileLoad_accepted()
{
    execReset();
}

/*
 * Execution menu
 */
void VrvWindow::execReset()
{
    // Signal that any current execution should not continue
    force_break = true;
    waitingForConsoleInput = false;

    // Destroy the world before initializing again
    destroy_world();

    VRVConsole->Clear();
    if (!InitializeWorld()) {
        updateStatus(StatusError);
        setExecActionState(ExecNo);
        return;
    }

    if (fileLoad_d->programFilenames().size()) {
        // Only initialize stack if there are program files
        initStack();

        for (const auto &filename : fileLoad_d->programFilenames()) {
            bool file_opened;
            if (!read_program_file(filename.toLocal8Bit().data(), &file_opened) && file_opened) {
                // Reinitialize world to clear user-program memory
                fe->write_log(1, "Flushing program memory...\n");
                destroy_world();
                InitializeWorld();

                updateStatus(StatusError);
                setExecActionState(ExecNo);
                return;
            }
        }
    }

    // Check for undefined symbols
    char *undefs = undefined_symbol_string();
    if (undefs != NULL) {
        fe->error("The following symbols are undefined:\n%s", undefs);

        updateStatus(StatusError);
        setExecActionState(ExecNo);
        return;
    }

    updateStatus(StatusIdle);
    setExecActionState(ExecRun);
    emit updatedWorld(true);
}

void VrvWindow::execRun()
{
    updateStatus(StatusRunning);
    setExecActionState(ExecPause);

    force_break = false;
    while (!force_break && programStatus == StatusRunning) {
        executeProgram(QVRV_RUN_STEPS);
        App->processEvents();   // Check for events in midst of long computation
    }
    UpdateDataDisplay();
}

void VrvWindow::execPause()
{
    updateStatus(StatusPaused);
    setExecActionState(ExecRun);

    force_break = true;
}

void VrvWindow::execSingleStep()
{
    updateStatus(StatusSinglestep);

    force_break = false;
    executeProgram(1);
    UpdateDataDisplay();
}

void VrvWindow::consoleHasLine()
{
    setWaitingForConsoleInput(false);
}

void VrvWindow::initStack()
{
    // Parse argv into space-separated and/or double-quoted strings
    QList<QString> argv_list;
    if (fileLoad_d->programFilenames().size())
        argv_list.push_back(fileLoad_d->programFilenames()[0].toLocal8Bit().data());
    else
        argv_list.push_back("");
    int tok_start = 0;
    bool skip_whitespace = true;
    bool in_quotes = false;
    const QString &argString = fileLoad_d->programArguments();
    for (int i = tok_start; i < argString.size(); ++i) {
        if (skip_whitespace) {
            skip_whitespace = false;
            tok_start = i;
            while (i < argString.size() && argString[i].isSpace())
                tok_start = ++i;
            if (i >= argString.size())
                break;
        }

        QChar c1 = argString[i];
        QChar c2 = i < argString.size() - 1 ? argString[i + 1] : '\0';

        if (c1 == '"') {
            if (in_quotes) {
                argv_list.push_back(argString.mid(tok_start, i - tok_start));
                skip_whitespace = true;
            } else {
                if (tok_start < i) {
                    argv_list.push_back(argString.mid(tok_start, i - tok_start));
                }
                tok_start = i + 1;
            }
            in_quotes = !in_quotes;

        } else if (c2 == '\0' || (!in_quotes && c2.isSpace())) {
            argv_list.push_back(argString.mid(tok_start, i + 1 - tok_start));
            skip_whitespace = true;
        }
    }

    // Convert to C array
    int argc = argv_list.size();
    char **argv = (char **)xmalloc((argc + 1) * sizeof(char *));
    for (int i = 0; i < argc; ++i) {
        argv[i] = xstrdup(argv_list[i].toLocal8Bit().data());
    }
    argv[argc] = NULL;

    // Initialize
    initialize_run_stack(argc, argv);
}

void VrvWindow::updateStatus(ProgStatus status)
{
    programStatus = status;
    switch (programStatus) {
        case StatusIdle:
            Window->statusBar()->showMessage("");
            break;

        case StatusError:
            Window->statusBar()->showMessage("Error");
            break;

        case StatusStopped:
            Window->statusBar()->showMessage("Stopped");
            break;

        case StatusPaused:
            Window->statusBar()->showMessage("Paused");
            break;

        case StatusRunning:
            Window->statusBar()->showMessage("Running");
            break;

        case StatusSinglestep:
            Window->statusBar()->showMessage("Single Step");
            break;

        default:
            Window->statusBar()->showMessage("?? Unknown status ??");
            break;
    }
}

void VrvWindow::setExecActionState(ExecActionState state)
{
    switch (state) {
        case ExecNo:
            ui->actionExecRun->setEnabled(false);
            ui->actionExecPause->setEnabled(false);
            ui->actionExecSingleStep->setEnabled(false);
            break;

        case ExecRun:
            ui->actionExecRun->setEnabled(true);
            ui->actionExecPause->setEnabled(false);
            ui->actionExecSingleStep->setEnabled(true);
            break;

        case ExecPause:
            ui->actionExecRun->setEnabled(false);
            ui->actionExecPause->setEnabled(true);
            ui->actionExecSingleStep->setEnabled(false);
            break;
    }
}

void VrvWindow::setWaitingForConsoleInput(bool waiting)
{
    if (waitingForConsoleInput == waiting)
        return;

    if ((waitingForConsoleInput = waiting)) {
        // Update satus message
        Window->statusBar()->showMessage(
            Window->statusBar()->currentMessage()
            % " (Waiting For Input...)");

        ui->consoleDockWidget->show();
        ui->consoleDockWidget->raise();
        ui->consoleDockWidget->activateWindow();
        VRVConsole->setFocus();

        // Disable execution actions
        setExecActionState(ExecNo);

    } else {
        // Reset status message
        updateStatus(programStatus);
        // Enable actions and re-run the waiting `ecall`
        if (programStatus == StatusRunning) {
            setExecActionState(ExecPause);
            execRun();

        } else {
            setExecActionState(ExecRun);
            execSingleStep();
        }
    }
}

void VrvWindow::executeProgram(int steps)
{
    runres_e res = exec_program(steps);
    ui->textWidget->highlightInstruction(PC);

    switch (res) {
        case RUN_STOPPED:
            updateStatus(StatusStopped);
            setExecActionState(ExecNo);
            break;

        case RUN_CONTINUABLE:
            /* Allow single-stepping programs to continue.
             * Multi-step running programs should continue running. */
            if (programStatus == StatusSinglestep)
                setExecActionState(ExecRun);
            break;

        case RUN_BREAKPOINT:
        {
            static QDialog *breakpointDialog;
            static Ui::BreakpointDialog *bpd;

            if (bpd == NULL) {
                breakpointDialog = new QDialog(this);

                bpd = new Ui::BreakpointDialog();
                bpd->setupUi(breakpointDialog);

                connect(bpd->continueButton, &QPushButton::clicked,
                        this, &VrvWindow::continueBreakpoint);
                connect(bpd->singleStepButton, &QPushButton::clicked,
                        this, &VrvWindow::singleStepBreakpoint);
                connect(bpd->abortButton, &QPushButton::clicked,
                        this, &VrvWindow::abortBreakpoint);
            }
            bpd->label->setText(QString("Execution stopped at breakpoint at ")
                                % QString("0x") % formatAddress(PC));
            breakpointDialog->open();
            updateStatus(StatusPaused);
            setExecActionState(ExecRun);
            break;
        }

        case RUN_WAIT_FOR_INPUT:
            setWaitingForConsoleInput(true);
            break;
    }
}

void VrvWindow::continueBreakpoint()
{
    execRun();
}

void VrvWindow::singleStepBreakpoint()
{
    execSingleStep();
}

void VrvWindow::abortBreakpoint()
{
    updateStatus(StatusIdle);
    setExecActionState(ExecRun);
}

/*
 * Text segment menu
 */
void VrvWindow::textShowUser(bool isChecked)
{
    ui->textWidget->setSegmentVisibility(TextWidget::UserTextSegment, isChecked);
}

void VrvWindow::textShowKernel(bool isChecked)
{
    ui->textWidget->setSegmentVisibility(TextWidget::KernelTextSegment, isChecked);
}

void VrvWindow::textShowComments(bool isChecked)
{
    ui->textWidget->setCommentVisibility(isChecked);
}

void VrvWindow::textShowEncoding(bool isChecked)
{
    ui->textWidget->setEncodingVisibility(isChecked);
}

/*
 * Data segment menu
 */
void VrvWindow::dataShowUser(bool isChecked)
{
    ui->dataWidget->setSegmentVisibility(DataWidget::UserDataSegment, isChecked);
}

void VrvWindow::dataShowStack(bool isChecked)
{
    ui->dataWidget->setSegmentVisibility(DataWidget::UserStackSegment, isChecked);
}

void VrvWindow::dataShowKernel(bool isChecked)
{
    ui->dataWidget->setSegmentVisibility(DataWidget::KernelDataSegment, isChecked);
}

void VrvWindow::dataGrouped(bool isChecked)
{
    ui->dataWidget->setGroupedByWords(isChecked);
}

void VrvWindow::dataBase(QAction *action)
{
    if (action) {
        int base = action->data().toInt();
        ui->dataWidget->setBase(base);
    }
}

/*
 * Help
 */
void VrvWindow::about()
{
    QMessageBox aboutMsg;

    QString text =
        "Qt frontend for VRV (Virtual RISC-V)<ul>"
        "<li>Copyright (c) 2023 <a href='https://luplab.cs.ucdavis.edu/'>LupLab</a></li>"
        "<li><a href='https://www.gnu.org/licenses/agpl-3.0.en.html'>GNU Affero General Public License version 3</a></li>"
        "<li><a href='https://gitlab.com/luplab/vrv'>Website</a></li>";

    QString details =
        "VRV was initially forked from Spim "
        "(https://spimsimulator.sourceforge.net/), a now unmaintained but "
        "formerly popular MIPS32 simulator copyrighted by James Larus and "
        "distributed under a BSD license.";

    aboutMsg.setIcon(QMessageBox::Information);
    aboutMsg.setWindowTitle("About Qvrv");
    aboutMsg.setText(text);
    aboutMsg.setDetailedText(details);
    aboutMsg.addButton(QMessageBox::Ok);
    aboutMsg.exec();
}
