/*
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include <QScrollBar>
#include <QString>
#include <QVBoxLayout>

#include "datawidget.h"
#include "format.h"

#include "../emu/memaccess.h"
#include "../emu/memloader.h"
#include "../emu/memseg.h"
#include "../emu/utils.h"

/*
 * Public methods
 */
DataWidget::DataWidget()
    : text(new QPlainTextEdit()),
    dataSegments(UserDataSegment | UserStackSegment),
    dataBase(16),
    dataGrouped(false)
{
    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(text);
    setLayout(layout);

    text->setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
    text->setContextMenuPolicy(Qt::NoContextMenu);
    text->setReadOnly(true);
    text->viewport()->setCursor(Qt::ArrowCursor);
}

bool DataWidget::segmentVisibility(Segment segment)
{
    return !!(dataSegments & segment);
}

void DataWidget::setSegmentVisibility(Segment segment, bool visible)
{
    if (visible && !(dataSegments & segment)) {
        dataSegments |= segment;
        displaySegments(true);
    } else if (!visible && (dataSegments & segment)) {
        dataSegments &= ~segment;
        displaySegments(true);
    }
}

int DataWidget::base()
{
    return dataBase;
}

void DataWidget::setBase(int base)
{
    if (dataBase != base) {
        /* Force update display if base has changed */
        dataBase = base;
        displaySegments(true);
    }
}

bool DataWidget::groupedByWords()
{
    return dataGrouped;
}

void DataWidget::setGroupedByWords(bool grouped)
{
    if (dataGrouped != grouped) {
        dataGrouped = grouped;
        displaySegments(true);
    }
}

QString DataWidget::toPlainText() {
    return text->toPlainText();
}

/*
 * Slots
 */
void DataWidget::refreshView(bool reset)
{
    /* No force update, we'll let the emulator tell us if some segments were
     * modified */
    displaySegments(false);

    /* Scroll back to the top upon reset */
    if (reset)
        text->verticalScrollBar()->setValue(text->verticalScrollBar()->minimum());
}

/*
 * Private methods
 */
void DataWidget::displaySegments(bool force)
{
    bool update = false;

    /* Check if some data segments have been modified */
    if (!force) {
        for (auto i = 0; i < MEMORY_SEG_COUNT; ++i) {
            if (!is_text_seg((seg_idx_e)i) && M[i].modified) {
                update = true;
                M[i].modified = false;
            }
        }
    }

    /* Update memory contents if forced or if it was modified */
    if (force || update) {
        /* Save scrollbar's slider position */
        int scrollPosition = text->verticalScrollBar()->value();

        /* Update segments */
        QString contents = QString("<div class=\"data-segments\">")
            % formatUserDataSegment()
            % formatUserStackSegment()
            % formatKernelDataSegment()
            % QString("</div>");

        /* Update view */
        text->clear();
        text->appendHtml(contents);

        /* Restore scrollbar's slider position */
        text->verticalScrollBar()->setSliderPosition(scrollPosition);
    }
}

QString DataWidget::formatKernelDataSegment()
{
    if (dataSegments & KernelDataSegment)
        return formatSegment("Kernel data segment", K_DATA_SEG);
    else
        return QString();
}

QString DataWidget::formatUserDataSegment()
{
    if (dataSegments & UserDataSegment)
        return formatSegment("User data segment", DATA_SEG);
    else
        return QString();
}

QString DataWidget::formatUserStackSegment()
{
    if (dataSegments & UserStackSegment)
        return formatSegment("User stack segment", STACK_SEG);
    else
        return QString();
}

QString DataWidget::formatSegment(QString title, uint segmentIndex)
{
    uint lowAddress = M[segmentIndex].lower_bound,
         highAddress = M[segmentIndex].upper_bound;

    return formatSegLabelHtml(title, lowAddress, highAddress)
        % formatSegmentMemory(lowAddress, highAddress);
}

QString DataWidget::formatSegmentMemory(uint from, uint to)
{
    QString contents;

    Q_ASSERT(from % BP_WORD == 0);

    while (from < to) {
        auto addrEnd = from;

        /* Figure out number of empty consecutive lines if any */
        while (addrEnd < to && read_word(addrEnd, EMU_ACCESS) == 0)
            addrEnd += BP_WORD;
        addrEnd = ALIGN_DOWN(addrEnd, memLineLen);

        /* Compact addresses for at least four empty lines */
        if (addrEnd - from >= memLineLen * 4) {
            contents += QString("[") % formatAddress(from)
                % QString("]..[") % formatAddress(addrEnd)
                % QString("]") % genNbspHtml(2) % QString("00000000<br>");
            from = addrEnd;
        }

        /* Print next line if we haven't reached the end yet */
        if (from < to) {
            addrEnd = MIN(to, ALIGN_UP(from + memLineLen, memLineLen));
            contents += formatMemoryLineNum(from, addrEnd);
            from = addrEnd;
        }
    }

    return contents;
}

QString DataWidget::formatMemoryLineNum(uint from, uint to)
{
    QString line;

    Q_ASSERT(from < to && (from % BP_WORD == 0));

    /* Address range */
    line += QString("[") % formatAddress(from) % QString("]");
    line += genNbspHtml(2);

    /* Numerical memory content */
    uint addr = from;
    while (addr < to) {
        line += genNbspHtml(2);

        if (dataGrouped) {
            uint val = read_word(addr, EMU_ACCESS);
            line += formatWordHtml(val, dataBase);
            addr += BP_WORD;
        } else {
            uchar val = read_byte(addr, EMU_ACCESS);
            line += formatByteHtml(val, dataBase);
            addr += 1;
        }
    }

    /* ASCII memory content */
    line += formatMemoryLineChr(from, addr);

    line += QString("<br>");
    return line;
}

QString DataWidget::formatMemoryLineChr(uint from, uint to)
{
    QString line = genNbspHtml(4);

    Q_ASSERT(from < to && (from % BP_WORD == 0));

    /* If @from isn't aligned to a line, fill out missing bytes with spaces */
    if (from % memLineLen != 0) {
        int missingBytes = memLineLen - (to - from);
        line += genNbspHtml(missingBytes);
    }

    /* Now translate each valid byte to ASCII when possible */
    for (uint addr = from; addr < to; addr++) {
        uint8_t val = read_byte(addr, EMU_ACCESS);
        line += formatCharHtml(val);
    }

    return line;
}
