/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include <QRegularExpression>
#include <QScrollBar>
#include <QString>
#include <QVBoxLayout>

#include "format.h"
#include "textwidget.h"

#include "../emu/reg.h"
#include "../emu/emu.h"
#include "../emu/inst.h"
#include "../emu/memloader.h"
#include "../emu/sstream.h"

/*
 * Public methods
 */
TextWidget::TextWidget()
    : text(new TextEdit(this)),
    textSegments(UserTextSegment),
    showEncoding(false),
    showComment(true)
{
    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(text);
    setLayout(layout);
}

bool TextWidget::segmentVisibility(Segment segment)
{
    return !!(textSegments & segment);
}

void TextWidget::setSegmentVisibility(Segment segment, bool visible)
{
    if (visible && !(textSegments & segment)) {
        textSegments |= segment;
        displaySegments(true);
    } else if (!visible && (textSegments & segment)) {
        textSegments &= ~segment;
        displaySegments(true);
    }
}

bool TextWidget::encodingVisibility()
{
    return showEncoding;
}

void TextWidget::setEncodingVisibility(bool visible)
{
    if (visible != showEncoding) {
        /* Force update display if property has changed */
        showEncoding = visible;
        displaySegments(true);
    }
}

bool TextWidget::commentVisibility()
{
    return showComment;
}

void TextWidget::setCommentVisibility(bool visible)
{
    if (visible != showComment) {
        /* Force update display if property has changed */
        showComment = visible;
        displaySegments(true);
    }
}

void TextWidget::highlightInstruction(uint pcAddr)
{
    /* Position a cursor over the target PC address */
    QRegularExpression regexpAddr("\\[" + formatAddress(pcAddr) + "\\] ");
    QTextCursor cursor = text->document()->find(regexpAddr);

    if (!cursor.isNull()) {
        /* No text needs to actually be selected */
        cursor.clearSelection();

        /* Create selection over the line */
        QTextEdit::ExtraSelection selection;
        selection.format.setBackground(QBrush(Qt::cyan));
        selection.format.setForeground(QBrush(Qt::black));
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = cursor;

        QList<QTextEdit::ExtraSelection> selections;
        selections.append(selection);

        text->setExtraSelections(selections);

        /* Set our cursor as the main text cursor and make it visible */
        text->setTextCursor(cursor);
        text->ensureCursorVisible();
    }
}

QString TextWidget::toPlainText()
{
    return text->toPlainText();
}

/*
 * Slots
 */
void TextWidget::refreshView(bool reset)
{
    /* No force update, we'll let the emulator tell us if some segments were
     * modified */
    displaySegments(false);

    /* Scroll back to the top upon reset */
    if (reset)
        text->verticalScrollBar()->setValue(text->verticalScrollBar()->minimum());
}

/*
 * Private methods
 */
void TextWidget::displaySegments(bool force)
{
    bool update = false;

    /* Check if some text segments have been modified */
    if (!force) {
        for (auto i = 0; i < MEMORY_SEG_COUNT; ++i) {
            if (is_text_seg((seg_idx_e)i) && M[i].modified) {
                update = true;
                M[i].modified = false;
            }
        }
    }

    /* Update memory contents if forced or if it was modified */
    if (force || update) {
        /* Update segments */
        QString contents = QString("<div class=\"text-segments\">")
            % formatUserTextSegment()
            % formatKernelTextSegment()
            % QString("</div>");

        /* Update view */
        text->clear();
        text->appendHtml(contents);

        /* Show instruction at PC */
        highlightInstruction(PC);
    }
}

QString TextWidget::formatUserTextSegment()
{
    if (textSegments & UserTextSegment)
        return formatSegment("User text segment", TEXT_SEG);
    else
        return QString();
}

QString TextWidget::formatKernelTextSegment()
{
    if (textSegments & KernelTextSegment)
        return formatSegment("Kernel text segment", K_TEXT_SEG);
    else
        return QString();
}

QString TextWidget::formatSegment(QString title, uint segmentIndex)
{
    uint lowAddress = M[segmentIndex].lower_bound,
         locAddress = M[segmentIndex].loc_counter,
         highAddress = M[segmentIndex].upper_bound;

    return formatSegLabelHtml(title, lowAddress, highAddress)
        % formatSegmentInstructions(lowAddress, locAddress, highAddress);
}

QString TextWidget::formatSegmentInstructions(uint from, uint to, uint high)
{
    QString contents;
    sstream ss;

    Q_ASSERT(from % BP_WORD == 0);

    ss_init(&ss);

    for (uint a = from; a < to; a += BP_WORD) {
        inst_s *inst = read_inst(a, EMU_ACCESS);

        if (!inst)
            continue;

        format_an_inst(&ss, inst, a);

        char *pc = ss_to_string(&ss);
        if (*pc == '*')
            ++pc;
        char *binInst = pc + 16;
        char *disassembly = binInst + 12;
        char *comment = NULL;

        pc += 3;
        pc[8] = '\0';
        binInst += 2;
        binInst[8] = '\0';
        comment = strstr(disassembly, ";");
        if (comment != NULL)
        {
            char* s;
            for (s = comment - 1; *s == ' '; s--)
                ;
            *(s + 1) = '\0';
        }

        if (inst_is_db_breakpoint(inst))
            contents += QString(text->markBreakpoint);
        else
            contents += genNbspHtml(1);

        contents += QString("[") % QString(pc) % QString("] ")
            % (showEncoding ? QString(binInst) : QString(""))
            % genNbspHtml(2) % QString("<b>") % QString(disassembly) % QString("</b>")
            % genNbspHtml(25 - strlen(disassembly))
            % (comment != NULL && showComment
               ? QString("<i>") % QString(comment).toHtmlEscaped() % QString("</i>")
               : QString(""))
            % QString("<br>");

        ss_clear(&ss);
    }

    if (to < high) {
        contents += genNbspHtml(1) % QString("[") % formatAddress(to)
            % QString("]..[") % formatAddress(high)
            % QString("]") % genNbspHtml(2) % QString("00000000<br>");
    }

    ss_destroy(&ss);

    return contents;
}
