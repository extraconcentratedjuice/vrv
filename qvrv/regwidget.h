/*
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef CSR_WIDGET_H
#define CSR_WIDGET_H

#include <QTableWidget>
#include <QWidget>

#include "regmodel.h"

class RegWidget : public QWidget
{
    Q_OBJECT

public:
    RegWidget();
    void setRegModel(RegModel *model);

public slots:
    void refreshView(bool reset);

private:
    QTableWidget *table;
    RegModel *model;
};

#endif /* CSR_WIDGET_H */
