QT += widgets

CONFIG += c++17

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QT_USE_QSTRINGBUILDER

SOURCES += \
    console.cpp \
    datawidget.cpp \
    fileloaddialog.cpp \
    format.cpp \
    main.cpp \
    menu.cpp \
    regwidget.cpp \
    state.cpp \
    textwidget.cpp \
    vrvwin.cpp

HEADERS += \
    console.h \
    datawidget.h \
    fileloaddialog.h \
    format.h \
    regmodel.h \
    regwidget.h \
    state.h \
    textedit.h \
    textwidget.h \
    vrvwin.h

FORMS += \
    breakpoint.ui \
    savelogfile.ui \
    vrvwin.ui

# Include VRV EMU files as a target
# - VRV `emu` library
vrvLib.target = ../emu/vrv.a
vrvLib.depends = ../emu/*.[ch]
vrvLib.commands = make -C ../emu/
PRE_TARGETDEPS += ../emu/vrv.a
LIBS += ../emu/vrv.a
QMAKE_EXTRA_TARGETS += vrvLib
# - VRV `emu` clean
vrvClean.commands = make -C ../emu distclean
distclean.depends = vrvClean
QMAKE_EXTRA_TARGETS += distclean vrvClean # Even though `distclean` is a standard target, need to add again as extra

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    windows_images.qrc

DISTFILES += \
    NewIcon.icns \
    README \
    macinfo.plist \
    qtspim.rc \
    windows_images/ClearRegs.png \
    windows_images/ClearRegsBig.png \
    windows_images/Help.png \
    windows_images/HelpBig.png \
    windows_images/LoadFile.png \
    windows_images/LoadFileBig.png \
    windows_images/NewIcon.ico \
    windows_images/Pause.png \
    windows_images/PauseBig.png \
    windows_images/Reinit.png \
    windows_images/ReinitBig.png \
    windows_images/ReloadFile.png \
    windows_images/ReloadFileBig.png \
    windows_images/Run.png \
    windows_images/RunBig.png \
    windows_images/SaveLog.png \
    windows_images/SaveLogBig.png \
    windows_images/SingleStep.png \
    windows_images/SingleStepBig.png \
    windows_images/Stop.png \
    windows_images/StopBig.png \
    windows_images/qtspim.png \
