#include <QAction>
#include <QCloseEvent>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QGroupBox>
#include <QPushButton>
#include <QResource>
#include <QSpacerItem>
#include <QToolButton>
#include <QVBoxLayout>

#include "fileloaddialog.h"
#include "state.h"

#include "../emu/frontend.h"

/*
 * Button object names
 */

FileLoadDialog::FileLoadDialog(QWidget *parent, QSettings *settings) :
    QDialog(parent, Qt::Tool),
    _settings(settings),
    _useDefaultSystemFile(true)
{
    // Read starting values from settings
    readSettings();

    // Set up skeleton
    this->setLayout(({
        QVBoxLayout *layout = new QVBoxLayout();

        layout->addWidget(({
            // System file group
            QGroupBox *group = new QGroupBox("System File");
            group->setLayout(({
                QVBoxLayout *layout = new QVBoxLayout();

                // Use default system file line
                layout->addLayout(({
                    QHBoxLayout *layout = new QHBoxLayout();

                    // Checkbox
                    _systemFileCheckbox = new QCheckBox("Use Default System File");
                    layout->addWidget(_systemFileCheckbox);
                    QObject::connect(_systemFileCheckbox, SIGNAL(stateChanged(int)), this, SLOT(modifiedInt(int)));

                    layout;
                }));

                // System file dialog
                layout->addLayout(({
                    QHBoxLayout *layout = new QHBoxLayout();

                    // Line title label
                    layout->addWidget(({
                        new QLabel("System File: ");
                    }));

                    // Filename input box
                    layout->addWidget(({
                        _systemFileLoadGroup = new QGroupBox();
                        QHBoxLayout *layout = new QHBoxLayout(_systemFileLoadGroup);

                        // Filename value label
                        _systemFileLoadValue = new QLineEdit();
                        layout->addWidget(_systemFileLoadValue);

                        // File dialog button
                        layout->addWidget(({
                            QToolButton *button = new QToolButton();
                            button->setText("...");
                            QObject::connect(button, SIGNAL(clicked()), this, SLOT(systemFileDialogOpen()));
                            button;
                        }));

                        _systemFileLoadGroup;
                    }));

                    layout;
                }));

                // Vertical spring
                layout->addStretch();

                layout;
            }));
            group;
        }));

        // Program filenames
        layout->addWidget(({
            QGroupBox *group = new QGroupBox("Program Files");
            QHBoxLayout *layout = new QHBoxLayout(group);

            layout->addWidget(({
                _programFilenameList = new QListWidget();
                QObject::connect(_programFilenameList, SIGNAL(itemSelectionChanged()), this, SLOT(modifiedVoid()));
                _programFilenameList;
            }));


            layout->addLayout(({
                QVBoxLayout *layout = new QVBoxLayout();

                layout->addWidget(({
                    QPushButton *button = new QPushButton("Add");
                    QObject::connect(button, SIGNAL(clicked(bool)), this, SLOT(programFilenameAdd(bool)));
                    button;
                }));

                layout->addWidget(({
                    QPushButton *button = new QPushButton("Move To Top");
                    _programFilenameSelectionButtons.append(button);
                    QObject::connect(button, SIGNAL(clicked(bool)), this, SLOT(programFilenameTop(bool)));
                    button;
                }));

                layout->addWidget(({
                    QPushButton *button = new QPushButton("Remove");
                    _programFilenameSelectionButtons.append(button);
                    QObject::connect(button, SIGNAL(clicked(bool)), this, SLOT(programFilenameRemove(bool)));
                    button;
                }));

                layout->addStretch();

                layout->addWidget(({
                    QPushButton *button = new QPushButton("Clear");
                    QObject::connect(button, SIGNAL(clicked(bool)), this, SLOT(programFilenameClear(bool)));
                    button;
                }));

                layout;
            }));

            group;
        }));

        // Program arguments
        layout->addWidget(({
            QGroupBox *group = new QGroupBox("Program Arguments");
            QHBoxLayout *layout = new QHBoxLayout(group);
            layout->addWidget(
                _programArgumentsLineEdit = new QLineEdit()
            );
            group;
        }));

        // Dialog buttons
        layout->addWidget(({
            _dialogButtonBox = new QDialogButtonBox(
                QDialogButtonBox::Ok | QDialogButtonBox::Reset | QDialogButtonBox::Cancel);
            QObject::connect(_dialogButtonBox, SIGNAL(clicked(QAbstractButton *)), this, SLOT(dialogButtonBoxClicked(QAbstractButton *)));
            _dialogButtonBox;
        }));

        layout;
    }));
}

void FileLoadDialog::showEvent(QShowEvent *event)
{
    renderValues(true);

    QDialog::showEvent(event);
}

void FileLoadDialog::dialogButtonBoxClicked(QAbstractButton *button)
{
    switch (_dialogButtonBox->buttonRole(button)) {
        case QDialogButtonBox::AcceptRole:
        {
            // Save current dialog values in permanent state
            _useDefaultSystemFile = _systemFileCheckbox->isChecked();
            _systemFilename = _systemFileLoadValue->text();
            _programFilenames.clear();
            for (int i = 0; i < _programFilenameList->count(); ++i) {
                _programFilenames.append(_programFilenameList->item(i)->text());
            }
            _programArguments = _programArgumentsLineEdit->text();

            // Write values to settings
            writeSettings();

            // Accept and close
            accept();
            break;
        }

        case QDialogButtonBox::ResetRole:
        {
            _systemFileCheckbox->setChecked(true);
            _systemFileLoadValue->setText("");
            _programArgumentsLineEdit->setText("");

            // Re-render and keep open
            renderValues(false);
            break;
        }

        default:
            // Reject and close
            reject();
    }
}

void FileLoadDialog::renderValues(bool resetToSavedValues)
{
    if (resetToSavedValues) {
        _systemFileLoadValue->setText(_systemFilename);
        _systemFileCheckbox->setChecked(_useDefaultSystemFile);

        _programFilenameList->clear();
        for (const auto &filename : _programFilenames) {
            _programFilenameList->addItem(filename);
        }

        _programArgumentsLineEdit->setText(_programArguments);
    }

    _systemFileLoadGroup->setDisabled(_systemFileCheckbox->isChecked());
    bool noProgramFilenameSelected = _programFilenameList->selectedItems().empty();
    for (auto button : _programFilenameSelectionButtons) {
        button->setDisabled(noProgramFilenameSelected);
    }
}

const QStringList &FileLoadDialog::programFilenames() const
{
    return _programFilenames;
}

bool FileLoadDialog::useDefaultSystemFile() const
{
    return _useDefaultSystemFile;
}

const QString &FileLoadDialog::systemFilename() const
{
    return _systemFilename;
}

const QString &FileLoadDialog::programArguments() const
{
    return _programArguments;
}

void FileLoadDialog::modifiedVoid()
{
    renderValues(false);
}
void FileLoadDialog::modifiedBool(bool)
{
    renderValues(false);
}
void FileLoadDialog::modifiedInt(int)
{
    renderValues(false);
}

void FileLoadDialog::systemFileDialogOpen()
{
    QString filename = QFileDialog::getOpenFileName(this,
        "Open System File",
        st.fileDialogDir,
        "Assembly (*.a *.s *.asm);;Text files (*.txt)");
    this->raise();
    if (!filename.size())
        return;
    st.fileDialogDir = QFileInfo(filename).absolutePath();

    if (filename == _systemFileLoadValue->text())
        return;

    _systemFileLoadValue->setText(filename);
}

void FileLoadDialog::programFilenameAdd(bool)
{
    QStringList filenames = QFileDialog::getOpenFileNames(this,
        "Open Program File(s)",
        st.fileDialogDir,
        "Assembly (*.a *.s *.asm);;Text files (*.txt)");
    this->raise();
    if (!filenames.size())
        return;
    st.fileDialogDir = QFileInfo(filenames[0]).absolutePath();

    for (QString filename : filenames) {
        _programFilenameList->addItem(filename);
    }
}

void FileLoadDialog::programFilenameTop(bool)
{
    int row = _programFilenameList->currentRow();
    if (row < 0)
        return;

    QListWidgetItem *item = _programFilenameList->takeItem(row);
    _programFilenameList->insertItem(0, item);
    _programFilenameList->setCurrentRow(0);
}

void FileLoadDialog::programFilenameRemove(bool)
{
    int row = _programFilenameList->currentRow();
    if (row < 0)
        return;

    QListWidgetItem *item = _programFilenameList->takeItem(row);
    delete item;
}

void FileLoadDialog::programFilenameClear(bool)
{
    _programFilenameList->clear();
}

void FileLoadDialog::readSettings()
{
    if (!_settings)
        return;

    _settings->beginGroup("FileLoad");
    _programFilenames= _settings->value("programFilenames", QStringList()).toStringList();
    _programArguments = _settings->value("programArguments", "").toString();
    _systemFilename = _settings->value("systemFilename", "").toString();
    _useDefaultSystemFile = _settings->value("useDefaultSystemFile", true).toBool();
    _settings->endGroup();
}

void FileLoadDialog::writeSettings() const
{
    if (!_settings)
        return;

    _settings->beginGroup("FileLoad");
    _settings->setValue("programFilenames", _programFilenames);
    _settings->setValue("programArguments", _programArguments);
    _settings->setValue("systemFilename", _systemFilename);
    _settings->setValue("useDefaultSystemFile", _useDefaultSystemFile);
    _settings->endGroup();
}
