/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include <QApplication>
#include <QMessageBox>
#include <QScreen>
#include <QStandardPaths>

#include "state.h"
#include "vrvwin.h"
#include "ui_vrvwin.h"

#include "../emu/frontend.h"
#include "../emu/emu.h"
#include "../emu/param.h"
#include "../emu/symbol.h"

/*
 * Exported variables
 */

qvrv_state_s st;

void VrvWindow::setDataDisplayBase(int base)
{
    switch (base) {
        case 2: ui->actionDataBin->setChecked(true); break;
        case 10: ui->actionDataDec->setChecked(true); break;
        case 16: ui->actionDataHex->setChecked(true); break;
        default: setDataDisplayBase(16);
    }
}

void VrvWindow::setVerbosityLevel(int verbosity)
{
    switch (verbosity) {
        case 0: ui->actionVerbosityQuiet->setChecked(true); break;
        case 1: ui->actionVerbosityNormal->setChecked(true); break;
        case 2: ui->actionVerbosityVerbose->setChecked(true); break;
        default: setVerbosityLevel(1);
    }
}

/*
 * Restore program settings and window positions
 */
void VrvWindow::readSettings()
{
    settings.beginGroup("MainWin");
    restoreGeometry(settings.value("Geometry").toByteArray());
    restoreState(settings.value("WindowState").toByteArray());
    settings.endGroup();

    settings.beginGroup("FileDialog");
    st.fileDialogDir = settings.value("fileDialogDir", QStandardPaths::HomeLocation).toString();
    settings.endGroup();

    settings.beginGroup("TextWin");
    ui->actionTextUser->setChecked(settings.value("User", ui->textWidget->segmentVisibility(TextWidget::UserTextSegment)).toBool());
    ui->actionTextKernel->setChecked(settings.value("Kernel", ui->textWidget->segmentVisibility(TextWidget::KernelTextSegment)).toBool());
    ui->actionTextComments->setChecked(settings.value("Comments", ui->textWidget->commentVisibility()).toBool());
    ui->actionTextEncoding->setChecked(settings.value("Encoding", ui->textWidget->encodingVisibility()).toBool());
    settings.endGroup();

    settings.beginGroup("DataView");
    ui->actionDataUser->setChecked(settings.value("User", ui->dataWidget->segmentVisibility(DataWidget::UserDataSegment)).toBool());
    ui->actionDataStack->setChecked(settings.value("Stack", ui->dataWidget->segmentVisibility(DataWidget::UserStackSegment)).toBool());
    ui->actionDataKernel->setChecked(settings.value("Kernel", ui->dataWidget->segmentVisibility(DataWidget::KernelDataSegment)).toBool());
    ui->actionDataGrouped->setChecked(settings.value("Grouped", ui->dataWidget->groupedByWords()).toBool());
    setDataDisplayBase(settings.value("Base", ui->dataWidget->base()).toInt());
    settings.endGroup();

    settings.beginGroup("Emu");
    ui->actionBreakExceptions->setChecked(settings.value("BreakExceptions", false).toBool());
    ui->actionExceptionsMisalignedLoads->setChecked(settings.value("ExceptionsMisalignedLoads", true).toBool());
    ui->actionExceptionsMisalignedStores->setChecked(settings.value("ExceptionsMisalignedStores", true).toBool());
    ui->actionDeviceTTY->setChecked(settings.value("DeviceTTY", false).toBool());
    setVerbosityLevel(settings.value("Verbosity", 1).toInt());
    settings.endGroup();
}

void VrvWindow::writeSettings()
{
    settings.beginGroup("MainWin");
    settings.setValue("Geometry", saveGeometry());
    settings.setValue("WindowState", saveState());
    settings.endGroup();

    settings.beginGroup("FileDialog");
    settings.setValue("fileDialogDir", st.fileDialogDir);
    settings.endGroup();

    settings.beginGroup("TextWin");
    settings.setValue("User", ui->actionTextUser->isChecked());
    settings.setValue("Kernel", ui->actionTextKernel->isChecked());
    settings.setValue("Comments", ui->actionTextComments->isChecked());
    settings.setValue("Encoding", ui->actionTextEncoding->isChecked());
    settings.endGroup();

    settings.beginGroup("DataView");
    settings.setValue("User", ui->actionDataUser->isChecked());
    settings.setValue("Stack", ui->actionDataStack->isChecked());
    settings.setValue("Kernel", ui->actionDataKernel->isChecked());
    settings.setValue("Grouped", ui->actionDataGrouped->isChecked());
    settings.setValue("Base", ui->dataWidget->base());
    settings.endGroup();

    settings.beginGroup("Emu");
    settings.setValue("BreakExceptions", ctl.break_on_exceptions);
    settings.setValue("ExceptionsMisalignedLoads", !!(ctl.misaligned_exceptions & MEMOP_LOAD));
    settings.setValue("ExceptionsMisalignedStores", !!(ctl.misaligned_exceptions & MEMOP_STORE));
    settings.setValue("DeviceTTY", conf.mapped_tty);
    settings.setValue("Verbosity", ctl.verbose_lvl);
    settings.endGroup();

    settings.sync();
}
