/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#include "vrvwin.h"

#include "../emu/frontend.h"
#include "../emu/param.h"

#define WAIT_LOOP_STOP 0
#define WAIT_LOOP_CONT 1

Console::Console(QWidget *parent)
    : QPlainTextEdit(parent)
{
    setUndoRedoEnabled(false);
    appendPlainText(QString(""));

    setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
}

void Console::WriteOutput(QString out)
{
    activateWindow();
    moveCursor(QTextCursor::End);
    insertPlainText(out);
    ensureCursorVisible();
}

char Console::ReadChar()
{
    activateWindow();

    if (!InputAvailable())
        return '\0';

    char firstChar = inputBuffer.front().toLatin1();
    inputBuffer.remove(0, 1);
    if (firstChar == '\n')
        --bufferLineCount;
    return firstChar;
}

bool Console::ReadLine(char *buff, size_t size)
{
    if (bufferLineCount <= 0)
        return false;

    if (buff && size) {
        size_t i = 0;
        while (i < size - 1) {
            if ((buff[i++] = ReadChar()) == '\n')
                break;
        }
        buff[i] = '\0';
    }
    return true;
}

bool Console::InputAvailable()
{
    return inputBuffer.length() > 0;
}

void Console::Clear()
{
    setPlainText("");
    inputBuffer = QString("");
    bufferLineCount = 0;
}

void Console::keyPressEvent(QKeyEvent* /*e*/)
{
    // Ignore
}

void Console::keyReleaseEvent(QKeyEvent* e)
{
    QString key = e->text();

    if (key != "") {
        for (QChar qc : key) {
            if (qc == QChar('\r'))
                qc = QChar('\n');

            inputBuffer.append(qc);
            if (!conf.mapped_tty) {
                // Do not echo input when using mem mapped IO
                WriteOutput(key);
            }

            // Signal to GUI that a line is available, and increment line count
            if (qc == QChar('\n')) {
                ++bufferLineCount;
                emit inputReady();
            }
        }
    }
}
