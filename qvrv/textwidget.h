/*
 * Copyright (c) 1990-2023 James R. Larus.
 * Copyright (c) 2023 LupLab.
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */
#ifndef TEXT_WIDGET_H
#define TEXT_WIDGET_H

#include <QObject>
#include <QPlainTextEdit>
#include <QString>
#include <QWidget>

#include "textedit.h"

class TextWidget : public QWidget
{
    Q_OBJECT

public:
    TextWidget();

    enum Segment {
        KernelTextSegment   = 0x1,
        UserTextSegment     = 0x2,

        AllTextSegments     = 0x3
    };
    Q_DECLARE_FLAGS(Segments, Segment);

    bool segmentVisibility(Segment segment);
    void setSegmentVisibility(Segment segment, bool visible);

    bool encodingVisibility();
    void setEncodingVisibility(bool visible);

    bool commentVisibility();
    void setCommentVisibility(bool visible);

    void highlightInstruction(uint pcAddr);

    QString toPlainText();

public slots:
    void refreshView(bool reset);

private:
    TextEdit *text;
    Segments textSegments;

    bool showEncoding;
    bool showComment;

    void displaySegments(bool force);

    QString formatKernelTextSegment();
    QString formatUserTextSegment();

    QString formatSegment(QString title, uint segmentIndex);
    QString formatSegmentInstructions(uint from, uint to, uint high);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(TextWidget::Segments)

#endif /* TEXT_WIDGET_H */
