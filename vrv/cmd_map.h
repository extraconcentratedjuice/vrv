/*
 * Copyright (c) 2023 LupLab
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

/* Key-value pairs for all commands used in the VRV prompt.
 * -- Must be sorted before use -- */

{ "b",                  BREAK_CMD },
{ "break",              BREAK_CMD },
{ "breakpoint",         BREAK_CMD },

{ "d",                  DELETE_CMD },
{ "del",                DELETE_CMD },
{ "delete",             DELETE_CMD },

{ "h",                  HELP_CMD },
{ "help",               HELP_CMD },

{ "l",                  LOAD_CMD },
{ "load",               LOAD_CMD },
{ "read",               LOAD_CMD },

{ "p",                  PRINT_CMD },
{ "print",              PRINT_CMD },
{ "printall",           PRINT_CMD },

{ "pb",                 PRINT_BKPTS_CMD },
{ "bkpts",              PRINT_BKPTS_CMD },
{ "printbkpts",         PRINT_BKPTS_CMD },
{ "printbreaks",        PRINT_BKPTS_CMD },
{ "printbreakpoints",   PRINT_BKPTS_CMD },

{ "pr",                 PRINT_REGS_CMD },
{ "regs",               PRINT_REGS_CMD },
{ "registers",          PRINT_REGS_CMD },
{ "printregs",          PRINT_REGS_CMD },
{ "printregisters",     PRINT_REGS_CMD },

{ "ps",                 PRINT_SYMBOLS_CMD },
{ "syms",               PRINT_SYMBOLS_CMD },
{ "symbols",            PRINT_SYMBOLS_CMD },
{ "printsyms",          PRINT_SYMBOLS_CMD },
{ "printsymbols",       PRINT_SYMBOLS_CMD },

{ "r",                  RUN_CMD },
{ "run",                RUN_CMD },
{ "c",                  RUN_CMD },
{ "cont",               RUN_CMD },
{ "continue",           RUN_CMD },
{ "start",              RUN_CMD },

{ "q",                  QUIT_CMD },
{ "quit",               QUIT_CMD },
{ "exit",               QUIT_CMD },

{ "set",                SET_CMD },
{ "opt",                SET_CMD },
{ "option",             SET_CMD },

{ "s",                  STEP_CMD },
{ "step",               STEP_CMD },
